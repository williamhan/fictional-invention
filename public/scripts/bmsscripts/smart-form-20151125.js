	$(function(){

			$("#smart-form").steps({
				bodyTag: "fieldset",
				headerTag: "h2",
				bodyTag: "fieldset",
				transitionEffect: "slideLeft",
				enableFinishButton: true,
				titleTemplate: "#title#",
				labels: {
					finish: "Submit Form",
					next: "Apply Now",
					previous: "Go Back",
					loading: "Loading..."
				},
				onStepChanging: function (event, currentIndex, newIndex){
					if (currentIndex > newIndex){return true; }
					var form = $(this);
					if (currentIndex < newIndex){}
					return form.valid();
				},
				onStepChanged: function (event, currentIndex, priorIndex){
				},
				onFinishing: function (event, currentIndex){
					var form = $(this);
					form.validate().settings.ignore = ":disabled";
					return form.valid();
				},
				onFinished: function (event, currentIndex){
					var form = $(this);
					//var apptype = $("#app_type").val();
					$(form).ajaxSubmit({
				         beforeSend: function() {
							  $( "#header-left" ).replaceWith( "<div class='col-sm-7' id='header-left'><div class='logo'><img src='img/company3.png'></div><div class='verticle-line'><h1>Thank You For Applying!</h1></div><h2>We have received your job application. If you wish to expidite the hiring process, please call us at:<br><br>Students - 800-887-8619<br>Veterans - 866-219-6080<br>CDL-A Holders - 800-338-3634<br><br>Monday - Friday<br>7:00am - 5PM (MST)<br><br>Otherwise, A Hiring Specialist Will Be In Touch With You Shortly.</h2></div><!-- Begin INDEED conversion code --> <script type='text/javascript'> /* <![CDATA[ */ var indeed_conversion_id = '9175775594800170'; var indeed_conversion_label = ''; /* ]]> */ </script> <script type='text/javascript' src='//conv.indeed.com/pagead/conversion.js'> </script> <noscript> <img height=1 width=1 border=0 src='//conv.indeed.com/pagead/conv/9175775594800170/?script=0'> </noscript> <!-- End INDEED conversion code -->" );
							  $('#join-us-form').fadeOut( "slow", function() {
								    // Animation complete.
							  });
							  $("html, body").animate({ scrollTop: 0 }, "slow");
							  return true;
						}

					});
					//alert("Thank you!\n\nWe have received your Job Application\nTo Speed up Your Application Please Call Us!\nStudents - 800-887-8619\nVeterans - 866-219-6080\nCDL-A Holders - 800-338-3634\n\nMonday - Friday\n7:00am - 5PM (MST)");

					//window.location.reload(true);
					//var apptype = $("#app_type").val();
					//$( document ).ajaxComplete();
					document.getElementById("smart-form").reset();

				}
			}).validate({
				errorClass: "state-error",
				validClass: "state-success",
				errorElement: "em",
				onkeyup: false,
				onclick: false,
				rules: {
					driver_fname: {
						required: true
					},
					driver_lname: {
						required: true
					},
					driver_email: {
						required: true,
						email: true
					},
					work_phone: {
						required: true,
						number: true,
						phoneUS: true
					},
					home_phone: {
						required: false,
						number: true
					},
					driver_address: {
						required: true
					},
					driver_zip:{
						required: true,
						zipcodeUS: true
					},
					driver_city:{
						required: true
					},
					driver_state:{
						required: true
					},
					app_type:{
						required: true
					},
					veteran_status:{
						required: true
					},
					cdl_holder:{
						required: true
					},
					agree_to_terms:{
						required: true
					},
					Disclosure_and_Everify_terms:{
						required: true
					},
					FMCSA_terms:{
						required:true,
					},
					PSP_terms:{
						required:false,
					},
					disclosure_name:{
						required:true,
					},
					disclosure_ssn:{
						required:true,
					},
					disclosure_dlNumber:{
						required:true,
					},
					driverlicense_state:{
						required:true,
					},
					disclosure_dlexp:{
						required:true,
					}
				},
				messages: {
					driver_fname: {
						required: "Please enter First Name"
					},
					driver_lname: {
						required: "Please enter Last Name"
					},
					driver_email: {
						required: 'Please enter your email',
						email: 'You must enter a VALID email'
					},
					work_phone: {
						required: 'Please enter your telephone',
						number: 'Please enter numbers only'
					},
					home_phone: {
						required: 'Please enter your telephone',
						number: 'Please enter numbers only'
					},
					driver_address: {
						required: "Please enter your address"
					},
					driver_zip:{
						required: 'Please enter your zip code'
					},
					driver_city:{
						required: 'Please enter your city'
					},
					driver_state:{
						required: 'Please select your state'
					},
					app_type:{
						required: 'Please select your driving experience'
					},
					veteran_status:{
						required: 'Please select veteran status'
					},
					pardot_score:{
						required: 'Please enter referral code'
					},
					agree_to_terms:{
						required: '    Please agree to terms and conditions'
					},
					Disclosure_and_Everify_terms:{
						required: '    Please agree to Disclosure and EVerify Terms'
					},
					FMCSA_terms:{
						required: '    Please agree to FMCSA Terms'
					},
					PSP_terms:{
						required: '    Please agree to PSP Terms'
					},
				},
				highlight: function(element, errorClass, validClass) {
					$(element).closest('.field').addClass(errorClass).removeClass(validClass);
				},
				unhighlight: function(element, errorClass, validClass) {
					$(element).closest('.field').removeClass(errorClass).addClass(validClass);
				},
				errorPlacement: function(error, element) {
					if (element.is(":radio") || element.is(":checkbox")) {
						element.closest('.option-group').after(error);
					} else {
						error.insertAfter(element.parent());
					}
				}

			});

			/* Reload Captcha
			----------------------------------------------- */
			function reloadCaptcha(){ $("#captchax").attr("src","php/captcha/captcha.php?r=" + Math.random()); }
			$('.captcode').click(function(e){
				e.preventDefault();
				reloadCaptcha();
			});

			/* Project datepicker range
			----------------------------------------------- */
			$("#disclosure_dlexp").datepicker({
				maxDate: null,
				changeMonth: true,
				changeYear: true,
				yearRange: "-0:+50", // next fifty years
				numberOfMonths: 1,
				prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
				nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
			});

			$("#driver_birthdate").datepicker({
				maxDate: "-21y",
				changeMonth: true,
				changeYear: true,
				yearRange: "-100:+0", // last hundred years
				numberOfMonths: 1,
				prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
				nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
			});

	});


	$(document).ready(function(){
		$("input[name*='cdl_holder']").change(function(){
			   var cdlHold = $(this).val();
			    if(cdlHold == "N") {
			        $("#app_type").val("SC");
			        $("#app_type").hide();
			        $("#PSP").hide();
		            $("#EVERIFY").show();
					$("#FMCSA").show();

			    }
			    else{
			        $("#app_type").val("SR");
			        $("#app_type").show();
			        $("#PSP").show();
		            $("#EVERIFY").show();
					$("#FMCSA").show();


			    }

			});
		});

	$(function(){
	  	$('.smartfm-ctrl').formShowHide();
	});

	$(function(){
	$('ul[role="tablist"]').hide();
	});

//	$(document).ready(function(){
//	$(function(){
//	$('a[href="#finish"]').attr('href', 'http://www.google.com/');
//	});
//	});
