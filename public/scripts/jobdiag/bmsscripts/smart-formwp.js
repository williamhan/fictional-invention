	$(function(){
			
			$("#smart-form").steps({
				bodyTag: "fieldset",
				headerTag: "h2",
				bodyTag: "fieldset",
				transitionEffect: "slideLeft",
				titleTemplate: "#title#",
				labels: {
					finish: "Submit Form",
					next: "Apply Now",
					previous: "Go Back",
					loading: "Loading..." 
				},
				onStepChanging: function (event, currentIndex, newIndex){
					if (currentIndex > newIndex){return true; }
					var form = $(this);
					if (currentIndex < newIndex){}
					return form.valid();
				},
				onStepChanged: function (event, currentIndex, priorIndex){
				},
				onFinishing: function (event, currentIndex){
					var form = $(this);
					form.validate().settings.ignore = ":disabled";
					return form.valid();
				},
				onFinished: function (event, currentIndex){
					var form = $(this);
					$(form).ajaxSubmit({
							target:'.result',			   
							beforeSubmit:function(){ 
							},
							error:function(){
							},
							 success:function(){
								 $('#smart-form').resetForm();
									$('.field').removeClass("state-error, state-success");
									if( $('.alert-error').length == 0){
										$('#smart-form').resetForm();
										reloadCaptcha();
									}
							 }
					  });					
				}
			}).validate({
				errorClass: "state-error",
				validClass: "state-success",
				errorElement: "em",
				onkeyup: false,
				onclick: false,
				rules: {
					driver_fname: {
						required: true
					},
					driver_lname: {
						required: true
					},					
					driver_email: {
						required: true,
						email: true
					},
					work_phone: {
						required: true,
						number: true,
						phoneUS: true
					},
					home_phone: {
						required: false,
						number: true
					},
					driver_address: {
						required: true
					},
					driver_zip:{
						required: true,
						zipcodeUS: true
					},
					driver_city:{
						required: true
					},
					driver_state:{
						required: true
					},
					app_type:{
						required: true
					},
					veteran_status:{
						required: true
					},
					pardot_score:{
						required: false
					},
					agree_to_terms:{
						required: true
					},
					Disclosure_and_Everify_terms:{
						required: true
					},					
					FMCSA_terms:{
						required:true,
					},					
					PSP_terms:{
						required:false,
					},					
					disclosure_name:{
						required:true,
					},					
					disclosure_ssn:{
						required:true,
					},					
					disclosure_dlNumber:{
						required:true,
					},					
					driverlicense_state:{
						required:true,
					},					
					disclosure_dlexp:{
						required:true,
					}					
				},
				messages: {
					driver_fname: {
						required: "Please enter firstname"
					},
					driver_lname: {
						required: "Please enter lastname"
					},
					driver_email: {
						required: 'Please enter your email',
						email: 'You must enter a VALID email'
					},
					work_phone: {
						required: 'Please enter your telephone',
						number: 'Please enter numbers only'
					},
					home_phone: {
						required: 'Please enter your telephone',
						number: 'Please enter numbers only'
					},					
					driver_address: {
						required: "Please enter your address"
					},
					driver_zip:{
						required: 'Please enter your zip code'
					},
					driver_city:{
						required: 'Please enter your city'
					},
					driver_state:{
						required: 'Please select your state'
					},
					app_type:{
						required: 'Please select your driving experience'
					},
					veteran_status:{
						required: 'Please select veteran status'
					},
					pardot_score:{
						required: 'Please enter referral code'
					},
					agree_to_terms:{
						required: '    Please agree to terms and conditions'
					},
					Disclosure_and_Everify_terms:{
						required: '    Please agree to Disclosure and EVerify Terms'
					},
					FMCSA_terms:{
						required: '    Please agree to FMCSA Terms'
					},
					PSP_terms:{
						required: '    Please agree to PSP Terms'
					},					
				},
				highlight: function(element, errorClass, validClass) {
					$(element).closest('.field').addClass(errorClass).removeClass(validClass);
				},
				unhighlight: function(element, errorClass, validClass) {
					$(element).closest('.field').removeClass(errorClass).addClass(validClass);
				},
				errorPlacement: function(error, element) {
					if (element.is(":radio") || element.is(":checkbox")) {
						element.closest('.option-group').after(error);
					} else {
						error.insertAfter(element.parent());
					}
				}
			
			});
			
			/* Reload Captcha
			----------------------------------------------- */	
			function reloadCaptcha(){ $("#captchax").attr("src","php/captcha/captcha.php?r=" + Math.random()); }
			$('.captcode').click(function(e){
				e.preventDefault();
				reloadCaptcha();
			});			
			
			/* Project datepicker range
			----------------------------------------------- */			
			$("#disclosure_dlexp").datepicker({
				defaultDate: "+1w",
				changeMonth: false,
				numberOfMonths: 1,
				prevText: '<i class="fa fa-chevron-left"></i>',
				nextText: '<i class="fa fa-chevron-right"></i>',
				onClose: function( selectedDate ) {
					$( "#end_date" ).datepicker( "option", "minDate", selectedDate );
				}
			});
			
			$("#driver_birthdate").datepicker({
				defaultDate: "+1w",
				changeMonth: false,
				numberOfMonths: 1,
				prevText: '<i class="fa fa-chevron-left"></i>',
				nextText: '<i class="fa fa-chevron-right"></i>',			
				onClose: function( selectedDate ) {
					$( "#start_date" ).datepicker( "option", "maxDate", selectedDate );
				}
			});
					
	}); 