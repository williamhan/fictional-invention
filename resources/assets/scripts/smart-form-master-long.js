$(function(){

    $("#smart-form").steps({
        bodyTag: "fieldset",
        headerTag: "h2",
        bodyTag: "fieldset",
        transitionEffect: "slideLeft",
        enableFinishButton: true,
        titleTemplate: "#title#",
        labels: {
            finish: "Submit Application",
            next: "Continue",
            previous: "Go Back",
            loading: "Loading..."
        },
        onStepChanging: function (event, currentIndex, newIndex)
        {

            var howheardpngo = $("#how_heard").val();

            var apptypepngo = $("#app_type").val();

            var perengostepone = "; (function(c,a,p,t,u,r,e){if(!c[u]){c.GlobalSnowplowNamespace = c.GlobalSnowplowNamespace||[]; c.GlobalSnowplowNamespace.push(u); c[u] = function(){(c[u].q = c[u].q||[]).push(arguments)}; c[u].q = c[u].q||[]; r = a.createElement(p); e = a.getElementsByTagName(p)[0]; r.async = 1; r.src = t; e.parentNode.insertBefore(r,e) } } (window,document,'script','//d1fc8wv8zag5ca.cloudfront.net/2.6.1/sp.js','analytics_perengo')); window.analytics_perengo('newTracker', 'crengland-tracker', 'analytics.perengo.com', {appId: 'crengland', cookieDomain:'.drivecre.com'}); window.analytics_perengo('trackPageView');";

            var perengosteptwosc = "; (function(c,a,p,t,u,r,e){if(!c[u]){c.GlobalSnowplowNamespace = c.GlobalSnowplowNamespace||[]; c.GlobalSnowplowNamespace.push(u); c[u] = function(){(c[u].q = c[u].q||[]).push(arguments)}; c[u].q = c[u].q||[]; r = a.createElement(p); e = a.getElementsByTagName(p)[0]; r.async = 1;r.src = t; e.parentNode.insertBefore(r,e) } } (window,document,'script','//d1fc8wv8zag5ca.cloudfront.net/2.6.1/sp.js','analytics_perengo')); window.analytics_perengo('newTracker', 'crengland-tracker', 'analytics.perengo.com', {appId: 'crengland', cookieDomain:'.drivecre.com'}); window.analytics_perengo('trackPageView'); window.analytics_perengo('trackStructEvent','conversion','personal-details-complete','student','1','10.0');";

            var perengosteptwoexp = "; (function(c,a,p,t,u,r,e){if(!c[u]){c.GlobalSnowplowNamespace = c.GlobalSnowplowNamespace||[]; c.GlobalSnowplowNamespace.push(u); c[u] = function(){(c[u].q = c[u].q||[]).push(arguments)}; c[u].q = c[u].q||[]; r = a.createElement(p); e = a.getElementsByTagName(p)[0]; r.async = 1;r.src = t; e.parentNode.insertBefore(r,e) } } (window,document,'script','//d1fc8wv8zag5ca.cloudfront.net/2.6.1/sp.js','analytics_perengo')); window.analytics_perengo('newTracker', 'crengland-tracker', 'analytics.perengo.com', {appId: 'crengland', cookieDomain:'.drivecre.com'}); window.analytics_perengo('trackPageView'); window.analytics_perengo('trackStructEvent','conversion','personal-details-complete','experienced','1','10.0');";

            var perengostepthree = "; (function(c,a,p,t,u,r,e){if(!c[u]){c.GlobalSnowplowNamespace = c.GlobalSnowplowNamespace||[]; c.GlobalSnowplowNamespace.push(u); c[u] = function(){(c[u].q = c[u].q||[]).push(arguments)}; c[u].q = c[u].q||[]; r = a.createElement(p); e = a.getElementsByTagName(p)[0]; r.async = 1; r.src = t; e.parentNode.insertBefore(r,e) } } (window,document,'script','//d1fc8wv8zag5ca.cloudfront.net/2.6.1/sp.js','analytics_perengo')); window.analytics_perengo('newTracker', 'crengland-tracker', 'analytics.perengo.com', {appId: 'crengland', cookieDomain:'.drivecre.com'}); window.analytics_perengo('trackPageView'); window.analytics_perengo('trackStructEvent','conversion','questionnaire-complete','driver','1','10.0');";

            var perengostepfoursc = "; (function(c,a,p,t,u,r,e){if(!c[u]){c.GlobalSnowplowNamespace = c.GlobalSnowplowNamespace||[]; c.GlobalSnowplowNamespace.push(u); c[u] = function(){(c[u].q = c[u].q||[]).push(arguments)}; c[u].q = c[u].q||[]; r = a.createElement(p); e = a.getElementsByTagName(p)[0]; r.async = 1; r.src = t; e.parentNode.insertBefore(r,e) } } (window,document,'script','//d1fc8wv8zag5ca.cloudfront.net/2.6.1/sp.js','analytics_perengo')); window.analytics_perengo('newTracker', 'crengland-tracker', 'analytics.perengo.com', {appId: 'crengland', cookieDomain:'.drivecre.com'}); window.analytics_perengo('trackPageView'); window.analytics_perengo('trackStructEvent','conversion','application-complete','student','1','10.0');";

            var perengostepfourexp = "; (function(c,a,p,t,u,r,e){if(!c[u]){c.GlobalSnowplowNamespace = c.GlobalSnowplowNamespace||[]; c.GlobalSnowplowNamespace.push(u); c[u] = function(){(c[u].q = c[u].q||[]).push(arguments)}; c[u].q = c[u].q||[]; r = a.createElement(p); e = a.getElementsByTagName(p)[0]; r.async = 1; r.src = t; e.parentNode.insertBefore(r,e) } } (window,document,'script','//d1fc8wv8zag5ca.cloudfront.net/2.6.1/sp.js','analytics_perengo')); window.analytics_perengo('newTracker', 'crengland-tracker', 'analytics.perengo.com', {appId: 'crengland', cookieDomain:'.drivecre.com'}); window.analytics_perengo('trackPageView'); window.analytics_perengo('trackStructEvent','conversion','application-complete','experienced','1','10.0');";

//             if(howheardpngo.match("^PERENGO"))
//             {
                if(newIndex === 1)
                {
	                if(apptypepngo == "SC")
	                {
                        var PNGOSTEPTWO = document.createElement('script');
                        PNGOSTEPTWO.type = "text/javascript";
                        PNGOSTEPTWO.text = perengosteptwosc;
                        $("head").append(PNGOSTEPTWO);
                    }

                    else
	                {
                        var PNGOSTEPTWO = document.createElement('script');
                        PNGOSTEPTWO.type = "text/javascript";
                        PNGOSTEPTWO.text = perengosteptwoexp;
                        $("head").append(PNGOSTEPTWO);
                    }


                }


                if (newIndex === 4)
				{
					var PNGOSTEPTHREE = document.createElement('script');
                    PNGOSTEPTHREE.type = "text/javascript";
                    PNGOSTEPTHREE.text = perengostepthree;
                    $("head").append(PNGOSTEPTHREE);
				}





            if (currentIndex > newIndex){return true; }
            var form = $(this);
            if (currentIndex < newIndex){}
            return form.valid();
        },
        onStepChanged: function (event, currentIndex, priorIndex){
        },
        onFinishing: function (event, currentIndex){
            var form = $(this);
            form.validate().settings.ignore = ":disabled";
            return form.valid();
        },
        onFinished: function (event, currentIndex){
            var form = $(this);
            //var apptype = $("#app_type").val();
            $(form).ajaxSubmit({
                beforeSend: function() {
                    var isappsr = $("#app_type").val();

                    if (isappsr == "SR")
                    {
                        $( "#header-left" ).replaceWith( "<div class='col-sm-7' id='header-left'><div class='logo'><img src='img/company3.png'></div><div class='verticle-line'><h1>We received your application!</h1></div><h2>Important Next Step: You will receive an email shortly from C.R. England (no-reply@crengland.com) with instructions to verify your email address. Please click the “Verify Now” button to finalize the application process.<br><br>If you have any questions at all, please call:<br><br>800-887-8619<br><br>Monday - Friday<br>8:00am-5:00pm MST</h2></div>" );
                    }
                    else if (isappsr == "D")
                    {
                        $( "#header-left" ).replaceWith( "<div class='col-sm-7' id='header-left'><div class='logo'><img src='img/company3.png'></div><div class='verticle-line'><h1>We received your application!</h1></div><h2>Important Next Step: You will receive an email shortly from C.R. England (no-reply@crengland.com) with instructions to verify your email address. Please click the “Verify Now” button to finalize the application process.<br><br>If you have any questions at all, please call:<br><br>800-887-8619<br><br>Monday - Friday<br>8:00am-5:00pm MST</h2></div>" );
                    }
                    else
                    {
                        $( "#header-left" ).replaceWith( "<div class='col-sm-7' id='header-left'><div class='logo'><img src='img/company3.png'></div><div class='verticle-line'><h1>We received your application!</h1></div><h2>Important Next Step: You will receive an email shortly from C.R. England (no-reply@crengland.com) with instructions to verify your email address. Please click the “Verify Now” button to finalize the application process.<br><br>If you have any questions at all, please call:<br><br>800-887-8619<br><br>Monday - Friday<br>8:00am-5:00pm MST</h2></div>" );
                    }

                    ga('send', 'pageview', '/non-cdl/thank-you');
                    ga('send', 'event', 'PageLoad', 'ThankYou', 'Non-CDL 1.0');

                    window._vis_opt_queue = window._vis_opt_queue || [];
                    window._vis_opt_queue.push(function() {_vis_opt_goal_conversion(201);});



                    function addScript(code)
                    {
                        var JS= document.createElement('script');
                        JS.text= code;
                        $("head").append(JS);
                    }

                    function addSrcScript(source)
                    {
                        var JSS= document.createElement('script');
                        JSS.type = "text/javascript";
                        JSS.src = source;
                        $("head").append(JSS);
                    }

                    function addNoScriptImg(imgsrc, hgt, wdth, brdr)
                    {
                        var NS = document.createElement('noscript');
                        var NSI = document.createElement('img');
                        NSI.height = hgt;
                        NSI.width = wdth;
                        NSI.border = brdr;
                        NSI.src = imgsrc;
                        $(NS).append(NSI);
                        $("head").append(NS);
                    }

                    function addNoScriptIframe(imgsrcd, hgtd, wdthd, brdrd, styld)
                    {
                        var NSD = document.createElement("noscript");
                        var NSID = document.createElement("iframe");
                        NSID.height = hgtd;
                        NSID.width = wdthd;
                        NSID.border = brdrd;
                        NSID.style = styld;
                        NSID.src = imgsrcd;
                        $(NSD).append(NSID);
                        $("head").append(NSD);
                    }

                    function addNoScriptImgStyle(imgsrcs, hgts, wdths, brdrs, styls)
                    {
                        var NSs = document.createElement('noscript');
                        var NSIs = document.createElement('img');
                        NSIs.height = hgts;
                        NSIs.width = wdths;
                        NSIs.border = brdrs;
                        NSIs.style = styls;
                        NSIs.src = imgsrcs;
                        $(NSs).append(NSIs);
                        $("head").append(NSs);
                    }

                    function addImg(imgsrci, hgti, wdthi, brdri)
                    {
                        var IMG= document.createElement('img');
                        IMG.setAttribute( 'height', hgti );
                        IMG.setAttribute( 'width', wdthi );
                        IMG.setAttribute( 'border', brdri );
                        IMG.setAttribute( 'src', imgsrci );
                        $("head").append(IMG);
                    }

                    function addIframe(iframesrcif, idif, stylif)
                    {
                        var IFRM= document.createElement('iframe');
                        IFRM.setAttribute( 'src', iframesrcif );
                        IFRM.setAttribute( 'id', idif );
                        IFRM.setAttribute( 'style', stylif );
                        $("head").append(IFRM);
                    }


                    function sizmek() {

                        var ebRand = Math.random()+'';
                        var ebRandCalc = ebRand * 1000000;

                        var thirdTag = document.createElement("script");
                        thirdTag.src = "HTTP://bs.serving-sys.com/Serving/ActivityServer.bs?cn=as&amp;ActivityID=1005203&amp;rnd=&#39" + ebRandCalc;
                        $("head").append(thirdTag);
                    }




                    var howheard = $("#how_heard").val();
                    var recordSource = $("#record_source").val();
                    var geminiVars = ["3100", "3101", "3102", "3103", "3104", "3105", "3106", "3107"];
                    var alltruckingvars = ["5085","5086"];

                    var hearstvars = ["5088"];

                    var upwardvars = ["5317"];

                    var zipalertcode = "(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': 	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); 	})(window,document,'script','dataLayer','GTM-P6P7Z4');";

                    var geminicode = "(function(w, d, t, r, u) {w[u] = w[u] || [];w[u].push({'projectId': '1000541572863', 'properties': {'pixelId': '39736'} }); var s = d.createElement(t);s.src = r;s.async = true;s.onload = s.onreadystatechange = function() { var y, rs = this.readyState,c = w[u];if (rs && rs != 'complete' && rs != 'loaded') {return} try { y = YAHOO.ywa.I13N.fireBeacon; w[u] = []; w[u].push = function(p) { y([p]) }; y(c) } catch (e) {} }; var scr = d.getElementsByTagName(t)[0],par = scr.parentNode; par.insertBefore(s, scr)})(window, document, 'script', 'https://s.yimg.com/wi/ytc.js', 'dotq');";

                    var googlecode = "/* <![CDATA[ */ var google_conversion_id = 1038203223; var google_conversion_language = 'en_US'; var google_conversion_format = '3'; var google_conversion_color = 'ffffff'; var google_conversion_label = 'CgZRCL3ZgwEQ1_KG7wM'; var google_remarketing_only = false; /* ]]> */";

                    var randallreicode = "(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','GTM-WKF65B');";

                    var kendrafbcode = "!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod? n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n; n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0; t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window, document,'script','//connect.facebook.net/en_US/fbevents.js'); fbq('init', '283321262099572');";

		            var perengostepone = "; (function(c,a,p,t,u,r,e){if(!c[u]){c.GlobalSnowplowNamespace = c.GlobalSnowplowNamespace||[]; c.GlobalSnowplowNamespace.push(u); c[u] = function(){(c[u].q = c[u].q||[]).push(arguments)}; c[u].q = c[u].q||[]; r = a.createElement(p); e = a.getElementsByTagName(p)[0]; r.async = 1; r.src = t; e.parentNode.insertBefore(r,e) } } (window,document,'script','//d1fc8wv8zag5ca.cloudfront.net/2.6.1/sp.js','analytics_perengo')); window.analytics_perengo('newTracker', 'crengland-tracker', 'analytics.perengo.com', {appId: 'crengland', cookieDomain:'.drivecre.com'}); window.analytics_perengo('trackPageView');";

                    var jobdiagsrc = "//www.jobdiagnosis.com/pixel_featured.php?sponsored=crengland";

                    var jobdiadid = "myIframe";


                    var indeedconv = "//conv.indeed.com/pagead/conversion.js";
                    var careerbuildersrc = "//click.appcast.io/pixels/generic3-2394.js?ent=98";
                    var indeedimgsrc = "//conv.indeed.com/pagead/conv/9175775594800170/?script=0";
                    var alltruckimgconvcdl = "//www.clickmeter.com/conversion.aspx?id=55BA17FD8A554B768E0A1C80A3C7512E&val=0.00&param=empty&com=0.00&comperc=0.00";
                    var alltruckimgconvsc = "//www.clickmeter.com/conversion.aspx?id=1641733CA6F34EEFA7996E30518D7A16&val=0.00&param=empty&com=0.00&comperc=0.00";
                    var googleconv = "//www.googleadservices.com/pagead/conversion.js";
                    var zipgtm = "//www.googletagmanager.com/ns.html?id=GTM-P6P7Z4";
                    var zipimage = "//track.ziprecruiter.com/conversion?board=coliseum_cr_england_cpc";
                    var jobtwocareersimg = "//www.jobs2careers.com/conversion2.php?p=1303";
                    var jobtwocareersimgva = "//www.jobs2careers.com/conversion2.php?p=1284";
                    var randallgtm = "//www.googletagmanager.com/ns.html?id=GTM-WKF65B";
                    var randallsrc = "//click.appcast.io/pixels/generic3-2155.js?ent=44";
                    var bairdsrc = "//click.appcast.io/pixels/bayard3-2967.js?ent=33";
                    var bairdorgsrc = "//click.appcast.io/pixels/bayard3-3001.js?ent=33";
                    var clickmetersrc = "//www.clickmeter.com/conversion.aspx?id=F3B1249DCDCE4AE1884F699FDECC0B3B&val=0.00&param=empty&com=0.00&comperc=0.00";
                    var sizmeksrc = "HTTP://bs.serving-sys.com/Serving/ActivityServer.bs?cn=as&amp;ActivityID=1005203&amp;ns=1";
                    var cdljobnowsrc = "https://t.cdljobnow.com/conversion/pixel.png";
                    var kendrafbsrc = "https://www.facebook.com/tr?id=283321262099572&ev=PageView&noscript=1";
                    var leadfivesrc = "//clk.l5srv.net/job_search/api/convert.srv?aid=MTU5";
                    var leadfivesrctwo = "//clk.l5srv.net/job_search/api/convert.srv?aid=MTg4";
                    var midamericacareers = "//click.appcast.io/pixels/generic3-3551.js?ent=41";
                    var kendrafbstyle = "display:none";
                    var gtmstyle = "display:none;visibility:hidden";
                    var zeros = "0";
                    var ones = "1";

                    var perengostepfoursc = "; (function(c,a,p,t,u,r,e){if(!c[u]){c.GlobalSnowplowNamespace = c.GlobalSnowplowNamespace||[]; c.GlobalSnowplowNamespace.push(u); c[u] = function(){(c[u].q = c[u].q||[]).push(arguments)}; c[u].q = c[u].q||[]; r = a.createElement(p); e = a.getElementsByTagName(p)[0]; r.async = 1; r.src = t; e.parentNode.insertBefore(r,e) } } (window,document,'script','//d1fc8wv8zag5ca.cloudfront.net/2.6.1/sp.js','analytics_perengo')); window.analytics_perengo('newTracker', 'crengland-tracker', 'analytics.perengo.com', {appId: 'crengland', cookieDomain:'.drivecre.com'}); window.analytics_perengo('trackPageView'); window.analytics_perengo('trackStructEvent','conversion','application-complete','student','1','10.0');";

		            var perengostepfourexp = "; (function(c,a,p,t,u,r,e){if(!c[u]){c.GlobalSnowplowNamespace = c.GlobalSnowplowNamespace||[]; c.GlobalSnowplowNamespace.push(u); c[u] = function(){(c[u].q = c[u].q||[]).push(arguments)}; c[u].q = c[u].q||[]; r = a.createElement(p); e = a.getElementsByTagName(p)[0]; r.async = 1; r.src = t; e.parentNode.insertBefore(r,e) } } (window,document,'script','//d1fc8wv8zag5ca.cloudfront.net/2.6.1/sp.js','analytics_perengo')); window.analytics_perengo('newTracker', 'crengland-tracker', 'analytics.perengo.com', {appId: 'crengland', cookieDomain:'.drivecre.com'}); window.analytics_perengo('trackPageView'); window.analytics_perengo('trackStructEvent','conversion','application-complete','experienced','1','10.0');";



                    addSrcScript(careerbuildersrc);

                    addSrcScript(bairdsrc);

                    addSrcScript(bairdorgsrc);

                    var PNGOSTEPONE= document.createElement('script');
                        PNGOSTEPONE.type = "text/javascript";
                        PNGOSTEPONE.text = perengostepone;
                        $("head").append(PNGOSTEPONE);

                    var clickmeter = document.createElement("script");
                    clickmeter.type = "text/javascript";
                    clickmeter.id = "cmconvscript";
                    clickmeter.src = "//s3.amazonaws.com/scripts-clickmeter-com/js/conversion.js";
                    $("body").append(clickmeter);

                    addNoScriptImg(clickmetersrc, zeros, zeros, zeros);

/*
                    if (howheard.match("^PERENGO"))
                    {
*/
	                    if(isappsr == "SC")
		                {
	                        var PNGOSTEPFOUR = document.createElement('script');
	                        PNGOSTEPFOUR.type = "text/javascript";
	                        PNGOSTEPFOUR.text = perengostepfoursc;
	                        $("head").append(PNGOSTEPFOUR);
	                    }

	                    else
		                {
	                        var PNGOSTEPFOUR = document.createElement('script');
	                        PNGOSTEPFOUR.type = "text/javascript";
	                        PNGOSTEPFOUR.text = perengostepfourexp;
	                        $("head").append(PNGOSTEPFOUR);
	                    }
//                     }


                    if (howheard.match("^INDEED"))
                    {
                        addSrcScript(indeedconv);

                        addNoScriptImg(indeedimgsrc, ones, ones, zeros);
                    }

                    if (howheard.match("^MIDAMERICAN"))
                    {
                        addSrcScript(midamericacareers);
                    }

                    if (howheard.match("^FACEBOOK_KVH"))
                    {
                        addScript(kendrafbcode);

                        addNoScriptImgStyle(kendrafbsrc, ones, ones, zeros, kendrafbstyle);
                    }


                    if (howheard.match("^JOBDIAGNOSIS"))
                    {
                        addIframe(jobdiagsrc, jobdiadid, kendrafbstyle);
                    }

                    if (howheard.match("^BAY_LEAD5"))
                    {
                        addImg(leadfivesrc, zeros, zeros, zeros);

                        addImg(leadfivesrctwo, zeros, zeros, zeros);
                    }

                    if (geminiVars.indexOf(recordSource) === -1)
                    {
                        //Do Nothing
                    }
                    else
                    {
                        addScript(geminicode);
                    }
                    if (alltruckingvars.indexOf(recordSource) === -1)
                    {
                        // Do Nothing
                    }
                    else
                    {
                        var cdlHold = $("input[id^='cdl_holder']").val();
                        if (cdlHold == "Y")
                        {
                            addImg(alltruckimgconvcdl, ones, ones, zeros);
                        }
                        else
                        {
                            addImg(alltruckimgconvsc, ones, ones, zeros);
                        }
                    }



                    if (upwardvars.indexOf(recordSource) === -1)
                    {
                        //Do Nothing
                    }
                    else
                    {
                        var upward = document.createElement("img");
                        upward.height = "0";
                        upward.width = "0";
                        upward.src = "http://l5srv.net/AdServer/convert.ads?aid=MjEw";
                        $("head").append(upward);
                    }

                    if (howheard.match("^JOBS2CAREERS"))
                    {
                        if (howheard.match("^JOBS2CAREERS_VA"))
                        {
                            addNoScriptImg(jobtwocareersimgva, ones, ones, zeros);
                        }
                        else
                        {
                            addNoScriptImg(jobtwocareersimg, ones, ones, zeros);
                        }
                    }

                    if (howheard.match("^BAY_CRAIGS")) {
                        sizmek();
                        addNoScriptImg(sizmeksrc, ones, ones, zeros);

                    }

                    if (howheard.match("^BAY_CDL_NOW"))
                    {
                        addImg(cdljobnowsrc, ones, ones, zeros);
                    }



                    if (howheard.match("^BING_"))
                    {
                        window.uetq = window.uetq || [];
                        window.uetq.push({ 'ec': 'Bing App', 'ea': 'Bing App', 'el': 'Bing App', 'ev': 1 });
                    }







                    if (howheard.match("^GOOGLE"))
                    {
                        addScript(googlecode);
                        var googlens = document.createElement("noscript");
                        var googlediv = document.createElement("div");
                        googlediv.style = "display:inline";
                        var googlensi = document.createElement("img");
                        googlensi.height = "1";
                        googlensi.width = "1";
                        googlensi.style = "border-style:none;";
                        googlensi.alt = "border-style:none;";
                        googlensi.src = "//www.googleadservices.com/pagead/conversion/1038203223/?label=CgZRCL3ZgwEQ1_KG7wM&amp;guid=ON&amp;script=0";
                        $(googlens).append(googlediv);
                        $(googlediv).append(googlensi);
                        $("head").append(googlens);
                        addSrcScript(googleconv);
                    }
                    if (howheard.match("^RANDALLMEDIA"))
                    {
                        addNoScriptImgStyle(randallgtm, zeros, zeros, zeros, gtmstyle);
                        addScript(randallreicode);
                    }
                    if (howheard.match("^RANDALL_MEDIA"))
                    {
                        addSrcScript(randallsrc);
                    }
                    if (howheard.match("^ZIPALERTS"))
                    {
                        addNoScriptIframe(zipgtm, zeros, zeros, zeros, gtmstyle);
                        addScript(zipalertcode);
                        addImg(zipimage, ones, ones, zeros);
                    }
                    $('#join-us-form').fadeOut( "slow", function() {
                    });
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    return true;
                }
            });

            document.getElementById("smart-form").reset();

        }
    }).validate({
        errorClass: "state-error",
        validClass: "state-success",
        errorElement: "em",
        onkeyup: false,
        onclick: false,
        rules: {
            driver_fname:{required: true, letters: true},
            driver_lname:{required: true, letters: true},
            driver_email: {required: true, email: true},
            work_phone: {required: true, number: true, phoneUS: true},
            home_phone: {required: false, number: true, phoneUS: true},
            driver_address:{required: true},
            driver_zip:{required: true, zipcodeUS: true},
            driver_city:{required: true, letters: true},
            driver_state:{required: true},
            app_type:{required: true},
            veteran_status:{required: true},
            honorable_discharge:{required: "#veteran_status_y:checked"},
            cdl_holder:{required: true},
            agree_to_terms:{required: true},
            pass_drug_test:{
                required: true
            },
            number_accidents_last_five:{required: true, number: true},
            number_tickets:{required: true, number: true},
            number_duis:{required: true, number: true},
            company_driver:{required: true},
            company_driver_start:{required: "#company_driver_y:checked"},
            company_driver_end:{required: "#company_driver_y:checked"},
            moving_violations:{required: true},
            'moving_violations_date[0]':{required: "#moving_violations_y:checked"},
            'violation_type[0]':{required: "#moving_violations_y:checked"},
            'violation_city[0]':{required: "#moving_violations_y:checked", letters: "#moving_violations_y:checked"},
            'violation_county[0]':{required: "#moving_violations_y:checked", letters: "#moving_violations_y:checked"},
            'violation_state[0]':{required: "#moving_violations_y:checked"},
            'violation_details[0]':{required: "#moving_violations_y:checked"},
            'fines_and_fees[0]':{required: "#moving_violations_y:checked"},
            accidents:{required: true},
            'accident_date[0]':{required: "#accidents_y:checked"},
            'accident_ticket[0]':{required: "#accidents_y:checked"},
            'accident_injuries[0]':{required: "#accidents_y:checked"},
            'accident_fatalities[0]':{required: "#accidents_y:checked"},
            'accident_preventable[0]':{required: "#accidents_y:checked"},
            'accident_fault[0]':{required: "#accidents_y:checked"},
            'accident_city[0]':{required: "#accidents_y:checked", letters: "#accidents_y:checked"},
            'accident_county[0]':{required: "#accidents_y:checked", letters: "#accidents_y:checked"},
            'accident_state[0]':{required: "#accidents_y:checked"},
            'accident_damage_amount[0]':{required: "#accidents_y:checked", number: "#accidents_y:checked"},
            'accident_details[0]':{required: "#accidents_y:checked"},
            dui_dwi:{required: true},
            'dui_dwi_date[0]':{required: "#dui_dwi_y:checked"},
            'dui_dwi_city[0]':{required: "#dui_dwi_y:checked", letters: "#dui_dwi_y:checked"},
            'dui_dwi_county[0]':{required: "#dui_dwi_y:checked", letters: "#dui_dwi_y:checked"},
            'dui_dwi_state[0]':{required: "#dui_dwi_y:checked"},
            'dui_dwi_details[0]':{required: "#dui_dwi_y:checked"},
            suspendedrevoked:{required: true},
            'suspendedrevoked_start_date[0]':{required: "#suspendedrevoked_y:checked"},
            'suspendedrevoked_end_date[0]':{required: "#suspendedrevoked_y:checked"},
            'suspendedrevoked_city[0]':{required: "#suspendedrevoked_y:checked", letters: "#suspendedrevoked_y:checked"},
            'suspendedrevoked_county[0]':{required: "#suspendedrevoked_y:checked", letters: "#suspendedrevoked_y:checked"},
            'suspendedrevoked_state[0]':{required: "#suspendedrevoked_y:checked"},
            'license_reinstated[0]':{required: "#suspendedrevoked_y:checked"},
            drugtest:{required: true},
            drugtest_date:{required: "#drugtest_y:checked"},
            drugtest_details:{required: false},
            felony:{required: true},
            'felony_date[0]':{required: "#felony_y:checked"},
            'felony_convicted[0]':{required: "#felony_y:checked"},
            'felony_incarcerated[0]':{required: "#felony_y:checked"},
            'felony_city[0]':{required: "#felony_y:checked", letters: "#felony_y:checked"},
            'felony_county[0]':{required: "#felony_y:checked", letters: "#felony_y:checked"},
            'felony_state[0]':{required: "#felony_y:checked"},
            'felony_details[0]':{required: "#felony_y:checked"},
            misdemeanor:{required: true},
            'misdemeanor_date[0]':{required: "#misdemeanor_y:checked"},
            'misdemeanor_convicted[0]':{required: "#misdemeanor_y:checked"},
            'misdemeanor_city[0]':{required: "#misdemeanor_y:checked", letters: "#misdemeanor_y:checked"},
            'misdemeanor_county[0]':{required: "#misdemeanor_y:checked", letters: "#misdemeanor_y:checked"},
            'misdemeanor_state[0]':{required: "#misdemeanor_y:checked"},
            'misdemeanor_details[0]':{required: "#misdemeanor_y:checked"},
            schoolgrad:{required: true},
            school_grad_name:{required: "#school_y:checked"},
            school_grad_date:{required: "#school_y:checked"},
            school_grad_city:{required: "#school_y:checked", letters: "#school_y:checked"},
            school_grad_state:{required: "#school_y:checked"},
            school_grad_zip:{required: "#school_y:checked",	zipcodeUS: "#school_y:checked"},
            'work_history_start[0]':{required: true},
            'work_history_end[0]':{required: false},
            'work_history_company[0]':{required: true},
            'work_history_title[0]':{required: true, letters: true},
            'work_history_address[0]':{required: true},
            'work_history_city[0]':{required: true, letters: true},
            'work_history_state[0]':{required: true},
            'work_history_zip[0]':{required: true,	zipcodeUS: true},
            'work_history_phone[0]':{required: true,	phoneUS: true},
            'work_history_reason[0]':{required: false},
            workhiscurrent_y:{required: false},
            'work_history_contact[0]':{required: true},
            Disclosure_and_Everify_terms:{required: true},
            FMCSA_terms:{required: true},
            PSP_terms:{required:"#cdl_holder_y:checked"},
            signature_data:{required: true},
            signature_data:{required: true},
            disclosure_name:{required: true, letters: true},
            disclosure_ssn:{required: true, digits: true, maxlength: 9, minlength: 9},
            disclosure_dlNumber:{required: true},
            driverlicense_state:{required: true},
            driver_birthdate:{required: true},
            disclosure_dlexp:{required: true}
        },
        messages: {
            driver_fname: {	required: "Please enter First Name", letters: 'Please enter letters only'},
            driver_lname: {	required: "Please enter Last Name", letters: 'Please enter letters only'},
            driver_email: {	required: 'Please enter your email',	email: 'You must enter a VALID email'},
            work_phone: {	required: 'Please enter your telephone',	number: 'Please enter numbers only'},
            home_phone: {	required: 'Please enter your telephone',	number: 'Please enter numbers only'},
            driver_address: {	required: "Please enter your address"},
            driver_zip:{	required: 'Please enter your zip code'},
            driver_city:{	required: 'Please enter your city', letters: 'Please enter letters only'},
            driver_state:{	required: 'Please select your state'},
            app_type:{	required: 'Please select your driving experience'},
            veteran_status:{	required: 'Please select veteran status'},
            agree_to_terms:{	required: '    Please agree to terms and conditions'},
            disclosure_name: {	required: "Please enter Full Name", letters: 'Please enter letters only'},
            Disclosure_and_Everify_terms:{	required: '    Please agree to Disclosure and EVerify Terms'},
            FMCSA_terms:{	required: '    Please agree to FMCSA Terms'},
            PSP_terms:{	required: '    Please agree to PSP Terms'},
        },
        highlight: function(element, errorClass, validClass) {
            $(element).closest('.field').addClass(errorClass).removeClass(validClass);
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).closest('.field').removeClass(errorClass).addClass(validClass);
        },
        errorPlacement: function(error, element) {
            if (element.is(":radio") || element.is(":checkbox")) {
                element.closest('.option-group').after(error);
            } else {
                error.insertAfter(element.parent());
            }
        }

    });

    /* Reload Captcha
     ----------------------------------------------- */
    function reloadCaptcha(){ $("#captchax").attr("src","php/captcha/captcha.php?r=" + Math.random()); }
    $('.captcode').click(function(e){
        e.preventDefault();
        reloadCaptcha();
    });

    /* Project datepicker range
     ----------------------------------------------- */
    $("#disclosure_dlexp").datepicker({
        dateFormat: 'mm/dd/yy',
        maxDate: null,
        changeMonth: true,
        changeYear: true,
        yearRange: "-0:+50", // next fifty years
        numberOfMonths: 1,
        prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
        nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
    });


    $(".otherdlexp").datepicker({
        dateFormat: 'mm/dd/yy',
        minDate: "-50y",
        changeMonth: true,
        changeYear: true,
        yearRange: "-50:+0", // next fifty years
        numberOfMonths: 1,
        prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
        nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
    });

    $("#driver_birthdate").datepicker({
        dateFormat: "mm/dd/yy",
        maxDate: "-21y",
        changeMonth: true,
        changeYear: true,
        yearRange: '-100:-21', // last hundred years
        numberOfMonths: 1,
        prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
        nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
    });


    $("#company_driver_start").datepicker({
        dateFormat: 'mm/dd/yy',
        maxDate: "-1",
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0", // last hundred years
        numberOfMonths: 1,
        prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
        nextText: '<span class="glyphicon glyphicon-chevron-right"></span>',
        onClose: function( selectedDate ) {
            $( "#company_driver_end" ).datepicker( "option", "minDate", selectedDate );
        }
    });

    $("#company_driver_end").datepicker({
        dateFormat: 'mm/dd/yy',
        maxDate: "-1",
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0", // last hundred years
        numberOfMonths: 1,
        prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
        nextText: '<span class="glyphicon glyphicon-chevron-right"></span>',
        onClose: function( selectedDate ) {
            $( "#company_driver_start" ).datepicker( "option", "maxDate", selectedDate );
        }
    });

    $(".moving_violations_date").datepicker({
        dateFormat: 'mm/dd/yy',
        minDate: "-5y",
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0", // last hundred years
        numberOfMonths: 1,
        prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
        nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
    });


    $(".accidents_date").datepicker({
        dateFormat: 'mm/dd/yy',
        minDate: "-5y",
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0", // last hundred years
        numberOfMonths: 1,
        prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
        nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
    });

    $(".dui_dwi_date").datepicker({
        dateFormat: 'mm/dd/yy',
        minDate: "-21y",
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0", // last hundred years
        numberOfMonths: 1,
        prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
        nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
    });

    $(".suspendedrevoked_start_date").datepicker({
        dateFormat: 'mm/dd/yy',
        minDate: "-21y",
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0", // last hundred years
        numberOfMonths: 1,
        prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
        nextText: '<span class="glyphicon glyphicon-chevron-right"></span>',
        onClose: function( selectedDate ) {
            $( ".suspendedrevoked_end_date" ).datepicker( "option", "minDate", selectedDate );
        }
    });

    $(".suspendedrevoked_end_date").datepicker({
        dateFormat: 'mm/dd/yy',
        //maxDate: "-21y",
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0", // last hundred years
        numberOfMonths: 1,
        prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
        nextText: '<span class="glyphicon glyphicon-chevron-right"></span>',
        onClose: function( selectedDate ) {
            $( ".suspendedrevoked_start_date" ).datepicker( "option", "maxDate", selectedDate );
        }
    });

    $("#illegal_drugs_date").datepicker({
        dateFormat: 'mm/dd/yy',
        minDate: "-1y",
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0", // last hundred years
        numberOfMonths: 1,
        prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
        nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
    });

    $("#drugtest_date").datepicker({
        dateFormat: 'mm/dd/yy',
        minDate: "-100y",
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0", // last hundred years
        numberOfMonths: 1,
        prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
        nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
    });

    $(".felony_date").datepicker({
        dateFormat: 'mm/dd/yy',
        minDate: "-100y",
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0", // last hundred years
        numberOfMonths: 1,
        prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
        nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
    });

    $(".incarceration_release_date").datepicker({
        dateFormat: 'mm/dd/yy',
        minDate: "-100y",
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0", // last hundred years
        numberOfMonths: 1,
        prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
        nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
    });

    $(".misdemeanor_date").datepicker({
        dateFormat: 'mm/dd/yy',
        minDate: "-100y",
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0", // last hundred years
        numberOfMonths: 1,
        prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
        nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
    });

    $("#school_grad_date").datepicker({
        dateFormat: 'mm/dd/yy',
        minDate: "-5y",
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0", // last hundred years
        numberOfMonths: 1,
        prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
        nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
    });

    $(".work_history_start").datepicker({
        dateFormat: 'mm/dd/yy',
        minDate: "-30y",
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0", // last hundred years
        numberOfMonths: 1,
        prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
        nextText: '<span class="glyphicon glyphicon-chevron-right"></span>',
        onClose: function( selectedDate ) {
            $( ".work_history_end" ).datepicker( "option", "minDate", selectedDate );
        }
    });


    $(".work_history_end").datepicker({
        dateFormat: 'mm/dd/yy',
        minDate: "-3y",
        changeMonth: true,
        changeYear: true,
        //showButtonPanel: true,
        currentText: "Current",
        yearRange: "-100:+0", // last hundred years
        numberOfMonths: 1,
        prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
        nextText: '<span class="glyphicon glyphicon-chevron-right"></span>',
        onClose: function( selectedDate ) {
            $( ".work_history_start" ).datepicker( "option", "maxDate", selectedDate );
        }
    });

});


$(document).ready(function(){
    $("input[id^='cdl_holder']").change(function(){
        var cdlHold = $(this).val();
        if(cdlHold == "N") {

            $('#app_type').children().remove();
            $("#app_type").append('<option value="SC"></option>');

            $("#app_type").hide();
            $("#PSP").hide();
            $("#EVERIFY").show();
            $("#FMCSA").show();

        }
        else if (cdlHold == "Y") {

            $('#app_type').children().remove();
            $("#app_type").append('<option value="">select your CDL driving experience</option>');
            $("#app_type").append('<option value="SR">I have less than 3 months experience</option>');
            $("#app_type").append('<option value="T">I have more than 3 months but less than 6 months experience</option>');
            $("#app_type").append('<option value="D">I have at least 6 months experience</option>');

            $("#app_type").show();
            $("#PSP").show();
            $("#EVERIFY").show();
            $("#FMCSA").show();

        }

    });
});

$(function(){
    $('.smartfm-ctrl').formShowHide();
});

$(function(){
    $('ul[role="tablist"]').hide();
});

$(document).ready(function(){
    $("#driver_birthdate").mask('99/99/9999', {placeholder:'_'});
    $("#disclosure_dlexp").mask('99/99/9999', {placeholder:'_'});
});

$(function() {

    /* Simple Cloning
     ------------------------------------------------- */
    $('#simple-clone').cloneya({
        serializeID: true,
        serializeIndex: true
    });

    /* Group Cloning
     ------------------------------------------------- */
    $('#clone-group-fields').cloneya({
        serializeID: true,
        serializeIndex: true
    });

    /* Group Cloning
     ------------------------------------------------- */
    $('#clone-group-fields2').cloneya({
        serializeID: true,
        serializeIndex: true
    });
    /* Group Cloning
     ------------------------------------------------- */
    $('#clone-group-fields3').cloneya({
        serializeID: true,
        serializeIndex: true
    });
    /* Group Cloning
     ------------------------------------------------- */
    $('#clone-group-fields4').cloneya({
        serializeID: true,
        serializeIndex: true
    });


    /* MIN MAX Cloning
     ------------------------------------------------- */
    $('#clone-min-max').cloneya({
        maximum: 3,
        minimum: 2
    });



    /* MOVING VIOLATIONS CLONING
     ----------------------------------------------*/
    $('#clone-animate').cloneya({
        maximum: 50,
        serializeID: true,
        serializeIndex: true
    }).on('before_clone.cloneya', function(event, toClone){
        toClone.find('.moving_violations_date').each(function(){
            $(this).datepicker('destroy');
        });
    }).on('after_clone.cloneya', function (event, toClone, newclone) {
        toClone.find('.moving_violations_date').each(function(){
            $(this).datepicker({
                dateFormat: 'mm/dd/yy',
                minDate: "-5y",
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+0", // last hundred years
                numberOfMonths: 1,
                prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
                nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
            });
        });
    }).on('after_append.cloneya', function (event, toClone, newclone) {
        newclone.find('.moving_violations_date').each(function(){
            $(this).datepicker({
                dateFormat: 'mm/dd/yy',
                minDate: "-5y",
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+0", // last hundred years
                numberOfMonths: 1,
                prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
                nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
            });
        });
    });

    /* ACCIDENTS CLONING
     ------------------------------------------------- */
    $('#clone-animate2').cloneya({
        maximum: 50,
        serializeID: true,
        serializeIndex: true
    }).on('before_clone.cloneya', function(event, toClone){
        toClone.find('.accidents_date').each(function(){
            $(this).datepicker('destroy');
        });
        toClone.find('.accdamageamount').each(function(){
            $(this).maskMoney('destroy');
        });
    }).on('after_clone.cloneya', function (event, toClone, newclone) {
        toClone.find('.accidents_date').each(function(){
            $(this).datepicker({
                dateFormat: 'mm/dd/yy',
                minDate: "-5y",
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+0", // last hundred years
                numberOfMonths: 1,
                prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
                nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
            });
        });
        toClone.find('.accdamageamount').each(function(){
            $(this).maskMoney({allowNegative: true, thousands:',', decimal:'.', affixesStay: false});
        });
    }).on('after_append.cloneya', function (event, toClone, newclone) {
        newclone.find('.accidents_date').each(function(){
            $(this).datepicker({
                dateFormat: 'mm/dd/yy',
                minDate: "-5y",
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+0", // last hundred years
                numberOfMonths: 1,
                prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
                nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
            });
        });
        newclone.find('.accdamageamount').each(function(){
            $(this).maskMoney({allowNegative: true, thousands:',', decimal:'.', affixesStay: false});
        });
    });

    /* DUI CLONING
     ------------------------------------------------- */
    $('#clone-animate3').cloneya({
        maximum: 50,
        serializeID: true,
        serializeIndex: true
    }).on('before_clone.cloneya', function(event, toClone){
        toClone.find('.dui_dwi_date').each(function(){
            $(this).datepicker('destroy');
        });
    }).on('after_clone.cloneya', function (event, toClone, newclone) {
        toClone.find('.dui_dwi_date').each(function(){
            $(this).datepicker({
                dateFormat: 'mm/dd/yy',
                minDate: "-50y",
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+0", // last hundred years
                numberOfMonths: 1,
                prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
                nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
            });
        });
    }).on('after_append.cloneya', function (event, toClone, newclone) {
        newclone.find('.dui_dwi_date').each(function(){
            $(this).datepicker({
                dateFormat: 'mm/dd/yy',
                minDate: "-50y",
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+0", // last hundred years
                numberOfMonths: 1,
                prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
                nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
            });
        });
    });

    /* SUSPENDED REVOKED CLONING
     ------------------------------------------------- */
    $('#clone-animate4').cloneya({
        maximum: 50,
        serializeID: true,
        serializeIndex: true
    }).on('before_clone.cloneya', function(event, toClone){
        toClone.find('.suspendedrevoked_start_date').each(function(){
            $(this).datepicker('destroy');
        });
        toClone.find('.suspendedrevoked_end_date').each(function(){
            $(this).datepicker('destroy');
        });
    }).on('after_clone.cloneya', function (event, toClone, newclone) {
        toClone.find('.suspendedrevoked_start_date').each(function(){
            $(this).datepicker({
                dateFormat: 'mm/dd/yy',
                minDate: "-5y",
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+0", // last hundred years
                numberOfMonths: 1,
                prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
                nextText: '<span class="glyphicon glyphicon-chevron-right"></span>',
                onClose: function( selectedDate ) {
                    $( ".suspendedrevoked_end_date" ).datepicker( "option", "minDate", selectedDate );
                }
            });
        });
        toClone.find('.suspendedrevoked_end_date').each(function(){
            $(this).datepicker({
                dateFormat: 'mm/dd/yy',
                minDate: "-5y",
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+0", // last hundred years
                numberOfMonths: 1,
                prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
                nextText: '<span class="glyphicon glyphicon-chevron-right"></span>',
                onClose: function( selectedDate ) {
                    $( ".suspendedrevoked_start_date" ).datepicker( "option", "maxDate", selectedDate );
                }
            });
        });
    }).on('after_append.cloneya', function (event, toClone, newclone) {
        newclone.find('.suspendedrevoked_start_date').each(function(){
            $(this).datepicker({
                dateFormat: 'mm/dd/yy',
                minDate: "-5y",
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+0", // last hundred years
                numberOfMonths: 1,
                prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
                nextText: '<span class="glyphicon glyphicon-chevron-right"></span>',
                onClose: function( selectedDate ) {
                    $( ".suspendedrevoked_end_date" ).datepicker( "option", "minDate", selectedDate );
                }
            });
        });
        newclone.find('.suspendedrevoked_end_date').each(function(){
            $(this).datepicker({
                dateFormat: 'mm/dd/yy',
                minDate: "-5y",
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+0", // last hundred years
                numberOfMonths: 1,
                prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
                nextText: '<span class="glyphicon glyphicon-chevron-right"></span>',
                onClose: function( selectedDate ) {
                    $( ".suspendedrevoked_start_date" ).datepicker( "option", "maxDate", selectedDate );
                }
            });
        });
    });

    /* FELONY CLONING
     ------------------------------------------------- */
    $('#clone-animate5').cloneya({
        maximum: 50,
        serializeIndex: true,
        serializeID: true

    }).on('before_clone.cloneya', function(event, toClone){
        toClone.find('.felony_date').each(function(){
            $(this).datepicker('destroy');
        });
        toClone.find('.smartfm-ctrl1').each(function(){
            $(this).formShowHide('destroy');
        });
        toClone.find('.incarceration_release_date').each(function(){
            $(this).datepicker('destroy');
        });
    }).on('after_clone.cloneya', function (event, toClone, newclone) {
        toClone.find('.felony_date').each(function(){
            $(this).datepicker({
                dateFormat: 'mm/dd/yy',
                minDate: "-100y",
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+0", // last hundred years
                numberOfMonths: 1,
                prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
                nextText: '<span class="glyphicon glyphicon-chevron-right"></span>',
                onClose: function( selectedDate ) {
                    $( ".incarceration_release_date" ).datepicker( "option", "minDate", selectedDate );
                }
            });
        });
        toClone.find('.smartfm-ctrl1').each(function(){
            $(this).formShowHide();
        });
        toClone.find('.incarceration_release_date').each(function(){
            $(this).datepicker({
                dateFormat: 'mm/dd/yy',
                minDate: "-100y",
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+0", // last hundred years
                numberOfMonths: 1,
                prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
                nextText: '<span class="glyphicon glyphicon-chevron-right"></span>',
                onClose: function( selectedDate ) {
                    $( ".felony_date" ).datepicker( "option", "maxDate", selectedDate );
                }
            });
        });
    }).on('after_append.cloneya', function (event, toClone, newclone) {
        newclone.find('.felony_date').each(function(){
            $(this).datepicker({
                dateFormat: 'mm/dd/yy',
                minDate: "-100y",
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+0", // last hundred years
                numberOfMonths: 1,
                prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
                nextText: '<span class="glyphicon glyphicon-chevron-right"></span>',
                onClose: function( selectedDate ) {
                    $( ".incarceration_release_date" ).datepicker( "option", "minDate", selectedDate );
                }
            });
        });
        newclone.find('.smartfm-ctrl1').each(function(){
            $(this).formShowHide();
        });
        newclone.find('.incarceration_release_date').each(function(){
            $(this).datepicker({
                dateFormat: 'mm/dd/yy',
                minDate: "-100y",
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+0", // last hundred years
                numberOfMonths: 1,
                prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
                nextText: '<span class="glyphicon glyphicon-chevron-right"></span>',
                onClose: function( selectedDate ) {
                    $( ".felony_date" ).datepicker( "option", "maxDate", selectedDate );
                }
            });
        });
    });

    /* MISDEMEANOR CLONING
     ------------------------------------------------- */
    $('#clone-animate6').cloneya({
        maximum: 50,
        serializeID: true,
        serializeIndex: true
    }).on('before_clone.cloneya', function(event, toClone){
        toClone.find('.misdemeanor_date').each(function(){
            $(this).datepicker('destroy');
        });
    }).on('after_clone.cloneya', function (event, toClone, newclone) {
        toClone.find('.misdemeanor_date').each(function(){
            $(this).datepicker({
                dateFormat: 'mm/dd/yy',
                minDate: "-100y",
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+0", // last hundred years
                numberOfMonths: 1,
                prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
                nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
            });
        });
    }).on('after_append.cloneya', function (event, toClone, newclone) {
        newclone.find('.misdemeanor_date').each(function(){
            $(this).datepicker({
                dateFormat: 'mm/dd/yy',
                minDate: "-100y",
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+0", // last hundred years
                numberOfMonths: 1,
                prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
                nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
            });
        });
    });

    /* OTHER DL CLONING
     ------------------------------------------------- */
    $('#clone-animate8').cloneya({
        maximum: 50,
        serializeID: true,
        serializeIndex: true
    }).on('before_clone.cloneya', function(event, toClone){
        toClone.find('.otherdlexp').each(function(){
            $(this).datepicker('destroy');
        });
    }).on('after_clone.cloneya', function (event, toClone, newclone) {
        toClone.find('.otherdlexp').each(function(){
            $(this).datepicker({
                dateFormat: 'mm/dd/yy',
                minDate: "-50y",
                changeMonth: true,
                changeYear: true,
                yearRange: "-50:+0", // last fifty years
                numberOfMonths: 1,
                prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
                nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
            });
        });
    }).on('after_append.cloneya', function (event, toClone, newclone) {
        newclone.find('.otherdlexp').each(function(){
            $(this).datepicker({
                dateFormat: 'mm/dd/yy',
                minDate: "-50y",
                changeMonth: true,
                changeYear: true,
                yearRange: "-50:+0", // last fifty years
                numberOfMonths: 1,
                prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
                nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
            });
        });
    });


    /* Animated Cloning with custom events
     ------------------------------------------------- */
    $('#clone-animate7').cloneya({
        maximum: 50,
        //preserveChildCount: true,
        serializeID: true,
        serializeIndex: true
    }).on('before_clone.cloneya', function(event, toClone){
        toClone.find('.work_history_start').each(function(){
            $(this).datepicker('destroy');
        });
        toClone.find('.work_history_end').each(function(){
            $(this).datepicker('destroy');
        });
    }).on('after_clone.cloneya', function (event, toClone, newclone) {
        toClone.find('.work_history_start').each(function(){
            $(this).datepicker({
                dateFormat: 'mm/dd/yy',
                minDate: "-30y",
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+0", // last hundred years
                numberOfMonths: 1,
                prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
                nextText: '<span class="glyphicon glyphicon-chevron-right"></span>',
                onClose: function( selectedDate ) {
                    $( ".work_history_end" ).datepicker( "option", "minDate", selectedDate );
                }
            });
        });
        toClone.find('.work_history_end').each(function(){
            $(this).datepicker({
                dateFormat: 'mm/dd/yy',
                minDate: "-30y",
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+0", // last hundred years
                numberOfMonths: 1,
                prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
                nextText: '<span class="glyphicon glyphicon-chevron-right"></span>',
                onClose: function( selectedDate ) {
                    $( ".work_history_start" ).datepicker( "option", "maxDate", selectedDate );
                }
            });
        });
    }).on('after_append.cloneya', function (event, toClone, newclone) {
        newclone.find('.work_history_start').each(function(){
            $(this).datepicker({
                dateFormat: 'mm/dd/yy',
                minDate: "-30y",
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+0", // last hundred years
                numberOfMonths: 1,
                prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
                nextText: '<span class="glyphicon glyphicon-chevron-right"></span>',
                onClose: function( selectedDate ) {
                    $( ".work_history_end" ).datepicker( "option", "minDate", selectedDate );
                }
            });
        });
        newclone.find('.work_history_end').each(function(){
            $(this).datepicker({
                dateFormat: 'mm/dd/yy',
                minDate: "-30y",
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+0", // last hundred years
                numberOfMonths: 1,
                prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
                nextText: '<span class="glyphicon glyphicon-chevron-right"></span>',
                onClose: function( selectedDate ) {
                    $( ".work_history_start" ).datepicker( "option", "maxDate", selectedDate );
                }
            });
        });
    });
});




$(document).ready(function(){



// 	var howheardpngo = $("#how_heard").val();

    var perengostepone = "; (function(c,a,p,t,u,r,e){if(!c[u]){c.GlobalSnowplowNamespace = c.GlobalSnowplowNamespace||[]; c.GlobalSnowplowNamespace.push(u); c[u] = function(){(c[u].q = c[u].q||[]).push(arguments)}; c[u].q = c[u].q||[]; r = a.createElement(p); e = a.getElementsByTagName(p)[0]; r.async = 1; r.src = t; e.parentNode.insertBefore(r,e) } } (window,document,'script','//d1fc8wv8zag5ca.cloudfront.net/2.6.1/sp.js','analytics_perengo')); window.analytics_perengo('newTracker', 'crengland-tracker', 'analytics.perengo.com', {appId: 'crengland', cookieDomain:'.drivecre.com'}); window.analytics_perengo('trackPageView');";

/*
	if (howheardpngo.match("^PERENGO"))
	{
*/
		var PNGOSTEPONE = document.createElement('script');
        PNGOSTEPONE.type = "text/javascript";
        PNGOSTEPONE.text = perengostepone;
        $("head").append(PNGOSTEPONE);
// 	}



});


// $(document).ready(function(){
//     var myw = $("#smart-form").width();
//     $sigDiv = $("#signature").jSignature({width: myw, height: 88,'UndoButton':true});
// });


$(document).ready(function(){
    $("#disclosure_name").focus(function(){
        $("#signaturedata:empty").text(function(){
            $("#signature_data_error").text("This field is required.");
        });
    });
});


$(document).ready(function(){
    $("#signature").bind('change', function(e) {
        var datapair = $("#signature").jSignature("getData", "image");
        $("#signaturedata").val(datapair[1]);
        $("#signature_data_error").hide();
    });
});



