<h2></h2>
                        <fieldset>
                            <div class="section align-center">Have you ever been employed as a company driver <br>or independent contractor for C.R. England?
                                <div id="companyDriver" class="option-group field">
                                    <label class="option">
                                        <input type="radio" id="company_driver_y" name="company_driver" class="smartfm-ctrl" value="Y"  data-show-id="company_driver_dates">
                                        <span class="radio"></span> Yes
                                    </label>
                                    <label class="option">
                                        <input type="radio" id="company_driver_n" name="company_driver" class="smartfm-ctrl" value="N"  data-show-id="">
                                        <span class="radio"></span> No
                                    </label>
                                </div>
                                <div class="spacer-b30"></div>
                            </div>
                            <div class="section align-center hiddenbox formShowHide_reset" id="company_driver_dates">
                                <div class="frm-row">Please Enter Your Start And Ending Dates Of Employment
                                    <div class="section colm colm6">
                                        <label class="field prepend-icon">
                                            <input type="text" name="company_driver_start" id="company_driver_start" class="gui-input" placeholder="Start Date" readonly="readonly">
                                            <span class="field-icon"><i class="glyphicon glyphicon-calendar"></i></span>
                                        </label>
                                    </div>
                                    <div class="section colm colm6">
                                        <label class="field prepend-icon">
                                            <input type="text" name="company_driver_end" id="company_driver_end" class="gui-input" placeholder="End Date" readonly="readonly">
                                            <span class="field-icon"><i class="glyphicon glyphicon-calendar"></i></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="section align-center">Have you attended a truck driving school in the last five years?
                                <div id="school_grad" class="option-group field">
                                    <label class="option">
                                        <input type="radio" id="school_y" name="schoolgrad" class="smartfm-ctrl" value="Y"  data-show-id="other_school1">
                                        <span class="radio"></span> Yes
                                    </label>
                                    <label class="option">
                                        <input type="radio" id="school_n" name="schoolgrad" class="smartfm-ctrl" value="N"  data-show-id="">
                                        <span class="radio"></span> No
                                    </label>
                                </div>
                                <div class="spacer-b30"></div>
                            </div>
                            <div class="section align-center hiddenbox" id="other_school1">
                                <div class="frm-row">
                                    <div class="section colm colm6">
                                        <label class="field prepend-icon">
                                            <input type="text" name="school_grad_name" id="school_grad_name" class="gui-input" placeholder="School Name">
                                            <span class="field-icon"><i class="glyphicon glyphicon-book"></i></span>
                                        </label>
                                    </div>
                                    <div class="section colm colm6">
                                        <label class="field prepend-icon">
                                            <input type="text" name="school_grad_date" id="school_grad_date" class="gui-input" placeholder="Graduation Date" readonly="readonly">
                                            <span class="field-icon"><i class="glyphicon glyphicon-calendar"></i></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="frm-row">
                                    <div class="section colm colm4">
                                        <label class="field prepend-icon">
                                            <input type="text" name="school_grad_city" id="school_grad_city" class="gui-input" placeholder="City">
                                            <span class="field-icon"><i class="glyphicon glyphicon-globe"></i></span>
                                        </label>
                                    </div>
                                    <div class="section colm colm4">
                                        <label for="school_grad_state" class="field select prepend-icon">
                                            <select id="school_grad_state" name="school_grad_state">
                                                <option value="">State:</option>
                                                <option value="AL">Alabama</option>
                                                <option value="AK">Alaska</option>
                                                <option value="AZ">Arizona</option>
                                                <option value="AR">Arkansas</option>
                                                <option value="CA">California</option>
                                                <option value="CO">Colorado</option>
                                                <option value="CT">Connecticut</option>
                                                <option value="DE">Delaware</option>
                                                <option value="DC">District Of Columbia</option>
                                                <option value="FL">Florida</option>
                                                <option value="GA">Georgia</option>
                                                <option value="HI">Hawaii</option>
                                                <option value="ID">Idaho</option>
                                                <option value="IL">Illinois</option>
                                                <option value="IN">Indiana</option>
                                                <option value="IA">Iowa</option>
                                                <option value="KS">Kansas</option>
                                                <option value="KY">Kentucky</option>
                                                <option value="LA">Louisiana</option>
                                                <option value="ME">Maine</option>
                                                <option value="MD">Maryland</option>
                                                <option value="MA">Massachusetts</option>
                                                <option value="MI">Michigan</option>
                                                <option value="MN">Minnesota</option>
                                                <option value="MS">Mississippi</option>
                                                <option value="MO">Missouri</option>
                                                <option value="MT">Montana</option>
                                                <option value="NE">Nebraska</option>
                                                <option value="NV">Nevada</option>
                                                <option value="NH">New Hampshire</option>
                                                <option value="NJ">New Jersey</option>
                                                <option value="NM">New Mexico</option>
                                                <option value="NY">New York</option>
                                                <option value="NC">North Carolina</option>
                                                <option value="ND">North Dakota</option>
                                                <option value="OH">Ohio</option>
                                                <option value="OK">Oklahoma</option>
                                                <option value="OR">Oregon</option>
                                                <option value="PA">Pennsylvania</option>
                                                <option value="RI">Rhode Island</option>
                                                <option value="SC">South Carolina</option>
                                                <option value="SD">South Dakota</option>
                                                <option value="TN">Tennessee</option>
                                                <option value="TX">Texas</option>
                                                <option value="UT">Utah</option>
                                                <option value="VT">Vermont</option>
                                                <option value="VA">Virginia</option>
                                                <option value="WA">Washington</option>
                                                <option value="WV">West Virginia</option>
                                                <option value="WI">Wisconsin</option>
                                                <option value="WY">Wyoming</option>
                                            </select>
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                    <div class="section colm colm4">
                                        <label class="field prepend-icon">
                                            <input type="text" name="school_grad_zip" id="school_grad_zip" class="gui-input" placeholder="Zip">
                                            <span class="field-icon"><i class="glyphicon glyphicon-map-marker"></i></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="section align-center">Have you ever failed or refused a pre-employment drug screen?
                                <div id="drug_test" class="option-group field">
                                    <label class="option">
                                        <input type="radio" id="drugtest_y" name="drugtest" class="smartfm-ctrl" value="Y"  data-show-id="pre_emp_drugtest">
                                        <span class="radio"></span> Yes
                                    </label>
                                    <label class="option">
                                        <input type="radio" id="drugtest_n" name="drugtest" class="smartfm-ctrl" value="N"  data-show-id="">
                                        <span class="radio"></span> No
                                    </label>
                                </div>
                                <div class="spacer-b30"></div>
                            </div>
                            <div class="section align-center hiddenbox" id="pre_emp_drugtest">
                                <div class="frm-row">
                                    <div class="section colm colm6">
                                        <label class="field prepend-icon">
                                            <input type="text" name="drugtest_date" id="drugtest_date" class="gui-input" placeholder="When?" readonly="readonly">
                                            <span class="field-icon"><i class="glyphicon glyphicon-calendar"></i></span>
                                        </label>
                                    </div>
                                    <div class="section colm colm6">
                                        <label class="field prepend-icon">
                                            <input type="text" class="gui-input" id="drugtest_details" name="drugtest_details" placeholder="Explain. (optional)">
                                            <span class="field-icon"><i class="glyphicon glyphicon-pencil"></i></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="section align-center">Have you ever been charged with a misdemeanor?
                                <div id="misdemeanor_charged" class="option-group field">
                                    <label class="option">
                                        <input type="radio" id="misdemeanor_y" name="misdemeanor" class="smartfm-ctrl" value="Y"  data-show-id="misdemeanor">
                                        <span class="radio"></span> Yes
                                    </label>
                                    <label class="option">
                                        <input type="radio" id="misdemeanor_n" name="misdemeanor" class="smartfm-ctrl" value="N"  data-show-id="">
                                        <span class="radio"></span> No
                                    </label>
                                </div>
                                <div class="spacer-b30"></div>
                            </div>
                            <div class="section align-center hiddenbox" id="misdemeanor">
                                <div id="clone-animate6">
                                    <div class="toclone">
                                        <div class="section colm colm12">
                                            <label class="field prepend-icon">
                                                <input type="text" name="misdemeanor_date[0]" id="misdate" class="gui-input misdemeanor_date" placeholder="Date Charged" readonly="readonly">
                                                <span class="field-icon"><i class="glyphicon glyphicon-calendar"></i></span>
                                            </label>
                                        </div>
                                        <div class="section colm colm12">Were You Convicted?
                                            <div class="option-group field">
                                                <label class="option">
                                                    <input type="radio" id="misconvicted_y" name="misdemeanor_convicted[0]" class="smartfm-ctrl" value="Y">
                                                    <span class="radio"></span> Yes
                                                </label>
                                                <label class="option">
                                                    <input type="radio" id="misconvicted_n" name="misdemeanor_convicted[0]" class="smartfm-ctrl" value="N">
                                                    <span class="radio"></span> No
                                                </label>
                                            </div>
                                        </div>
                                        <div class="frm-row">
                                            <div class="section colm colm4">
                                                <label class="field prepend-icon">
                                                    <input type="text" name="misdemeanor_city[0]" id="miscity" class="gui-input" placeholder="City">
                                                    <span class="field-icon"><i class="glyphicon glyphicon-globe"></i></span>
                                                </label>
                                            </div>
                                            <div class="section colm colm4">
                                                <label class="field prepend-icon">
                                                    <input type="text" name="misdemeanor_county[0]" id="miscounty" class="gui-input" placeholder="County">
                                                    <span class="field-icon"><i class="glyphicon glyphicon-map-marker"></i></span>
                                                </label>
                                            </div>
                                            <div class="section colm colm4">
                                                <label class="field select prepend-icon">
                                                    <select id="misstate" name="misdemeanor_state[0]">
                                                        <option value="">State:</option>
                                                        <option value="AL">Alabama</option>
                                                        <option value="AK">Alaska</option>
                                                        <option value="AZ">Arizona</option>
                                                        <option value="AR">Arkansas</option>
                                                        <option value="CA">California</option>
                                                        <option value="CO">Colorado</option>
                                                        <option value="CT">Connecticut</option>
                                                        <option value="DE">Delaware</option>
                                                        <option value="DC">District Of Columbia</option>
                                                        <option value="FL">Florida</option>
                                                        <option value="GA">Georgia</option>
                                                        <option value="HI">Hawaii</option>
                                                        <option value="ID">Idaho</option>
                                                        <option value="IL">Illinois</option>
                                                        <option value="IN">Indiana</option>
                                                        <option value="IA">Iowa</option>
                                                        <option value="KS">Kansas</option>
                                                        <option value="KY">Kentucky</option>
                                                        <option value="LA">Louisiana</option>
                                                        <option value="ME">Maine</option>
                                                        <option value="MD">Maryland</option>
                                                        <option value="MA">Massachusetts</option>
                                                        <option value="MI">Michigan</option>
                                                        <option value="MN">Minnesota</option>
                                                        <option value="MS">Mississippi</option>
                                                        <option value="MO">Missouri</option>
                                                        <option value="MT">Montana</option>
                                                        <option value="NE">Nebraska</option>
                                                        <option value="NV">Nevada</option>
                                                        <option value="NH">New Hampshire</option>
                                                        <option value="NJ">New Jersey</option>
                                                        <option value="NM">New Mexico</option>
                                                        <option value="NY">New York</option>
                                                        <option value="NC">North Carolina</option>
                                                        <option value="ND">North Dakota</option>
                                                        <option value="OH">Ohio</option>
                                                        <option value="OK">Oklahoma</option>
                                                        <option value="OR">Oregon</option>
                                                        <option value="PA">Pennsylvania</option>
                                                        <option value="RI">Rhode Island</option>
                                                        <option value="SC">South Carolina</option>
                                                        <option value="SD">South Dakota</option>
                                                        <option value="TN">Tennessee</option>
                                                        <option value="TX">Texas</option>
                                                        <option value="UT">Utah</option>
                                                        <option value="VT">Vermont</option>
                                                        <option value="VA">Virginia</option>
                                                        <option value="WA">Washington</option>
                                                        <option value="WV">West Virginia</option>
                                                        <option value="WI">Wisconsin</option>
                                                        <option value="WY">Wyoming</option>
                                                    </select>
                                                    <i class="arrow double"></i>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="section colm colm12">
                                            <label class="field prepend-icon">
                                                <input type="text" class="gui-input" id="misdetails" name="misdemeanor_details[0]" placeholder="In your own words, describe the misdemeanor...">
                                                <span class="field-icon"><i class="glyphicon glyphicon-pencil"></i></span>
                                            </label>
                                        </div>
                                        <a href="#" class="clone button btn-primary"><i class="glyphicon glyphicon-plus"></i></a>
                                        <a href="#" class="delete button"><i class="glyphicon glyphicon-minus"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="section align-center">Have you ever been charged with a felony?
                                <div id="felony_charged" class="option-group field">
                                    <label class="option">
                                        <input type="radio" id="felony_y" name="felony" class="smartfm-ctrl" value="Y"  data-show-id="felony">
                                        <span class="radio"></span> Yes
                                    </label>
                                    <label class="option">
                                        <input type="radio" id="felony_n" name="felony" class="smartfm-ctrl" value="N"  data-show-id="">
                                        <span class="radio"></span> No
                                    </label>
                                </div>
                                <div class="spacer-b30"></div>
                            </div>
                            <div class="section align-center hiddenbox" id="felony">
                                <div id="clone-animate5">
                                    <div class="toclone" id="felonyclone">
                                        <div class="section colm colm12">
                                            <label class="field prepend-icon">
                                                <input type="text" name="felony_date[0]" id="feldate" class="gui-input felony_date" placeholder="Date Charged" readonly="readonly">
                                                <span class="field-icon"><i class="glyphicon glyphicon-calendar"></i></span>
                                            </label>
                                        </div>
                                        <div class="frm-row">
                                            <div class="section colm colm6">Were You Convicted?
                                                <div class="option-group field">
                                                    <label class="option">
                                                        <input type="radio" id="felconvicted_y" name="felony_convicted[0]" class="smartfm-ctrl" value="Y">
                                                        <span class="radio"></span> Yes
                                                    </label>
                                                    <label class="option">
                                                        <input type="radio" id="felconvicted_n" name="felony_convicted[0]" class="smartfm-ctrl" value="N">
                                                        <span class="radio"></span> No
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="section colm colm6">Were You Incarcerated?
                                                <div class="option-group field">
                                                    <label class="option">
                                                        <input type="radio" id="felincarcerated_y" name="felony_incarcerated[0]" class="smartfm-ctrl" value="Y" >
                                                        <span class="radio"></span> Yes
                                                    </label>
                                                    <label class="option">
                                                        <input type="radio" id="felincarcerated_n" name="felony_incarcerated[0]" class="smartfm-ctrl" value="N" >
                                                        <span class="radio"></span> No
                                                    </label>
                                                </div>


                                            </div>
                                        </div>
                                        <div class="frm-row">
                                            <div class="section colm colm4">
                                                <label class="field prepend-icon">
                                                    <input type="text" name="felony_city[0]" id="felcity" class="gui-input" placeholder="City">
                                                    <span class="field-icon"><i class="glyphicon glyphicon-globe"></i></span>
                                                </label>
                                            </div>
                                            <div class="section colm colm4">
                                                <label class="field prepend-icon">
                                                    <input type="text" name="felony_county[0]" id="felcounty" class="gui-input" placeholder="County">
                                                    <span class="field-icon"><i class="glyphicon glyphicon-map-marker"></i></span>
                                                </label>
                                            </div>
                                            <div class="section colm colm4">
                                                <label class="field select prepend-icon">
                                                    <select id="felstate" name="felony_state[0]">
                                                        <option value="">State:</option>
                                                        <option value="AL">Alabama</option>
                                                        <option value="AK">Alaska</option>
                                                        <option value="AZ">Arizona</option>
                                                        <option value="AR">Arkansas</option>
                                                        <option value="CA">California</option>
                                                        <option value="CO">Colorado</option>
                                                        <option value="CT">Connecticut</option>
                                                        <option value="DE">Delaware</option>
                                                        <option value="DC">District Of Columbia</option>
                                                        <option value="FL">Florida</option>
                                                        <option value="GA">Georgia</option>
                                                        <option value="HI">Hawaii</option>
                                                        <option value="ID">Idaho</option>
                                                        <option value="IL">Illinois</option>
                                                        <option value="IN">Indiana</option>
                                                        <option value="IA">Iowa</option>
                                                        <option value="KS">Kansas</option>
                                                        <option value="KY">Kentucky</option>
                                                        <option value="LA">Louisiana</option>
                                                        <option value="ME">Maine</option>
                                                        <option value="MD">Maryland</option>
                                                        <option value="MA">Massachusetts</option>
                                                        <option value="MI">Michigan</option>
                                                        <option value="MN">Minnesota</option>
                                                        <option value="MS">Mississippi</option>
                                                        <option value="MO">Missouri</option>
                                                        <option value="MT">Montana</option>
                                                        <option value="NE">Nebraska</option>
                                                        <option value="NV">Nevada</option>
                                                        <option value="NH">New Hampshire</option>
                                                        <option value="NJ">New Jersey</option>
                                                        <option value="NM">New Mexico</option>
                                                        <option value="NY">New York</option>
                                                        <option value="NC">North Carolina</option>
                                                        <option value="ND">North Dakota</option>
                                                        <option value="OH">Ohio</option>
                                                        <option value="OK">Oklahoma</option>
                                                        <option value="OR">Oregon</option>
                                                        <option value="PA">Pennsylvania</option>
                                                        <option value="RI">Rhode Island</option>
                                                        <option value="SC">South Carolina</option>
                                                        <option value="SD">South Dakota</option>
                                                        <option value="TN">Tennessee</option>
                                                        <option value="TX">Texas</option>
                                                        <option value="UT">Utah</option>
                                                        <option value="VT">Vermont</option>
                                                        <option value="VA">Virginia</option>
                                                        <option value="WA">Washington</option>
                                                        <option value="WV">West Virginia</option>
                                                        <option value="WI">Wisconsin</option>
                                                        <option value="WY">Wyoming</option>
                                                    </select>
                                                    <i class="arrow double"></i>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="section colm colm12">
                                            <label class="field prepend-icon">
                                                <input type="text" class="gui-input" id="feldetails" name="felony_details[0]" placeholder="In your own words, describe the felony...">
                                                <span class="field-icon"><i class="glyphicon glyphicon-pencil"></i></span>
                                            </label>
                                        </div>
                                        <a href="#" class="clone button btn-primary"><i class="glyphicon glyphicon-plus"></i></a>
                                        <a href="#" class="delete button"><i class="glyphicon glyphicon-minus"></i></a>
                                    </div>
                                </div>
                            </div>
                        </fieldset>