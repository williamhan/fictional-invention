<h2 id="contactInfo">Contact Info</h2>

                        <fieldset id="first">

                            <div class="spacer-t20 spacer-b20"></div>

                            <div class="section">
                                <label class="field prepend-icon">
                                    <input type="hidden" name="start_time" id="start_time" class="gui-input" value="{{date('Y-m-d H:i:s')}}">
                                    <input type="hidden" name="driver_guid" id="driver_guid" class="gui-input" value={{App\AppFunc::getGUID()}}>
                                    <input type="email" name="driver_email" id="driver_email" class="gui-input" placeholder="Email Address">
                                    <span class="field-icon">
										<span class="glyphicon glyphicon-envelope"></span>
									</span>
                                </label>
                            </div>
                            <div class="frm-row">
                                <div class="section colm colm6">
                                    <label class="field prepend-icon">
                                        <input type="text" name="driver_fname" id="driver_fname" class="gui-input" placeholder="First Name">
                                        <input type="hidden" name="record_source" id="record_source" class="gui-input" value={{Request::get('rs')}}>
                                        <input type="hidden" name="how_heard" id="how_heard" class="gui-input" value={{Request::get('id')}}>
                                        <input type="hidden" name="app_version" id="app_version" class="gui-input" value="Long">
                                        <input type="hidden" name="var_track" id="var_track" class="gui-input" value="{{Request::path()}}">
                                        <span class="field-icon">
										<span class="glyphicon glyphicon-user"></span>
									</span>
                                    </label>
                                </div>
                                <div class="section colm colm6">
                                    <label class="field prepend-icon">
                                        <input type="text" name="driver_lname" id="driver_lname" class="gui-input" placeholder="Last Name">
                                        <span class="field-icon">
										<span class="glyphicon glyphicon-user"></span>
									</span>
                                    </label>
                                </div>
                            </div>
                            <div class="align-center">
                                <div class="frm-row">
                                    <div class="section colm colm6">
                                        <label class="field prepend-icon">
                                            <input type="tel" name="work_phone" id="work_phone" class="gui-input" placeholder="Cell Phone">
                                            <span class="field-icon">
											<span class="glyphicon glyphicon-phone"></span>
										</span>
                                        </label>
                                    </div>
                                    <div class="section colm colm6">
                                        <label class="field prepend-icon">
                                            <input type="tel" name="home_phone" id="home_phone" class="gui-input" placeholder="Home Phone">
                                            <span class="field-icon">
											<span class="glyphicon glyphicon-phone-alt"></span>
										</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="align-center">
                                <div class="frm-row">
                                    <div class="section colm colm8">
                                        <label class="field prepend-icon">
                                            <input type="text" name="driver_address" id="driver_address" class="gui-input" placeholder="Street Address">
                                            <span class="field-icon">
											<span class="glyphicon glyphicon-road"></span>
										</span>
                                        </label>
                                    </div>
                                    <div class="section colm colm4">
                                        <label class="field prepend-icon">
                                            <input type="text" name="driver_zip" id="driver_zip" class="gui-input" placeholder="Zip">
                                            <span class="field-icon">
											<span class="glyphicon glyphicon-map-marker"></span>
										</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="align-center">
                                <div class="frm-row">
                                    <div class="section colm colm6">
                                        <label class="field prepend-icon">
                                            <input type="text" name="driver_city" id="driver_city" class="gui-input" placeholder="City">
                                            <span class="field-icon">
											<span class="glyphicon glyphicon-home"></span>
										</span>
                                        </label>
                                    </div>
                                    <div class="section colm colm6">
                                        <label for="driver_state" class="field select prepend-icon">
                                            <select id="driver_state" name="driver_state">
                                                <option value="">Choose State</option>
                                                <option value="AL">Alabama</option>
                                                <option value="AK">Alaska</option>
                                                <option value="AZ">Arizona</option>
                                                <option value="AR">Arkansas</option>
                                                <option value="CA">California</option>
                                                <option value="CO">Colorado</option>
                                                <option value="CT">Connecticut</option>
                                                <option value="DE">Delaware</option>
                                                <option value="DC">District Of Columbia</option>
                                                <option value="FL">Florida</option>
                                                <option value="GA">Georgia</option>
                                                <option value="HI">Hawaii</option>
                                                <option value="ID">Idaho</option>
                                                <option value="IL">Illinois</option>
                                                <option value="IN">Indiana</option>
                                                <option value="IA">Iowa</option>
                                                <option value="KS">Kansas</option>
                                                <option value="KY">Kentucky</option>
                                                <option value="LA">Louisiana</option>
                                                <option value="ME">Maine</option>
                                                <option value="MD">Maryland</option>
                                                <option value="MA">Massachusetts</option>
                                                <option value="MI">Michigan</option>
                                                <option value="MN">Minnesota</option>
                                                <option value="MS">Mississippi</option>
                                                <option value="MO">Missouri</option>
                                                <option value="MT">Montana</option>
                                                <option value="NE">Nebraska</option>
                                                <option value="NV">Nevada</option>
                                                <option value="NH">New Hampshire</option>
                                                <option value="NJ">New Jersey</option>
                                                <option value="NM">New Mexico</option>
                                                <option value="NY">New York</option>
                                                <option value="NC">North Carolina</option>
                                                <option value="ND">North Dakota</option>
                                                <option value="OH">Ohio</option>
                                                <option value="OK">Oklahoma</option>
                                                <option value="OR">Oregon</option>
                                                <option value="PA">Pennsylvania</option>
                                                <option value="RI">Rhode Island</option>
                                                <option value="SC">South Carolina</option>
                                                <option value="SD">South Dakota</option>
                                                <option value="TN">Tennessee</option>
                                                <option value="TX">Texas</option>
                                                <option value="UT">Utah</option>
                                                <option value="VT">Vermont</option>
                                                <option value="VA">Virginia</option>
                                                <option value="WA">Washington</option>
                                                <option value="WV">West Virginia</option>
                                                <option value="WI">Wisconsin</option>
                                                <option value="WY">Wyoming</option>
                                            </select>
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="section align-center">Do you have a CDL-A?
                                <div id="cdl_holder" class="option-group field">
                                    <label class="option">
                                        <input type="radio" id="cdl_holder_y" name="cdl_holder" class="smartfm-ctrl" value="Y"  data-show-id="app_type_box">
                                        <span class="radio"></span> Yes
                                    </label>
                                    <label class="option">
                                        <input type="radio" id="cdl_holder_n" name="cdl_holder" class="smartfm-ctrl" value="N"  data-hide-id="app_type_box">
                                        <span class="radio"></span> No
                                    </label>
                                </div>

                                <div class="spacer-b30"></div>


                                <div id="app_type_box" class="hiddenbox">
                                    <div class="section">
                                        <label class="field select">
                                            <select id="app_type" name="app_type">
                                            </select>
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="section align-center">Are you a Veteran?
                                <div id="veteran_status" class="option-group field">
                                    <label class="option">
                                        <input type="radio" id="veteran_status_y" name="veteran_status" class="smartfm-ctrl" value="Y"  data-show-id="honorable_discharge_box">
                                        <span class="radio"></span> Yes
                                    </label>
                                    <label class="option">
                                        <input type="radio" id="veteran_status_n" name="veteran_status" class="smartfm-ctrl" value="N"  data-show-id="">
                                        <span class="radio"></span> No
                                    </label>
                                </div>

                                <div class="spacer-b30"></div>

                                <div id="honorable_discharge_box" class="hiddenbox">
                                    <div class="section">
                                        <label class="field select">
                                            <select id="honorable_discharge" name="honorable_discharge">
                                                <option value="">Veteran Status:</option>
                                                <option value="Y">I received an honorable discharge</option>
                                                <option value="N">I did not receive an honorable discharge</option>
                                            </select>
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="align-center">
                                <div class="frm-row">
                                    <div class="section colm colm12 align-center">Are you able to pass a drug test?
                                        <div id="pass_drug_test" class="option-group field">
                                            <label class="option">
                                                <input type="radio" name="pass_drug_test" class="smartfm-ctrl" value="Y">
                                                <span class="radio"></span> Yes
                                            </label>
                                            <label class="option">
                                                <input type="radio" name="pass_drug_test" class="smartfm-ctrl" value="N">
                                                <span class="radio"></span> No
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="frm-row align-center">In the last 5 years, how many of the following have you had?
	                            <div class="spacer-b10"></div>
                                <div class="section colm colm6">

	                                <label for="stepper1" class="field-label">Accidents:</label>
	                                <label class="field">
										<input type="text" name="stepper1" id="stepper1" placeholder="0" class="gui-input">
									</label>

                                </div>
                                <div class="section colm colm6">

	                                <label for="stepper2" class="field-label">Moving Violations:</label>
	                                <label class="field">
										<input type="text" name="stepper2" id="stepper2" placeholder="0" class="gui-input">
									</label>

                                </div>
                            </div>
                            <div class="frm-row align-center">
                                <div class="section colm colm12">

	                                <label for="stepper3" class="field-label">Total DUIs / DWIs In Your Lifetime:</label>
	                                <label class="field">
										<input type="text" name="stepper3" id="stepper3" placeholder="0" class="gui-input">
									</label>

                                </div>
                            </div>
                            <div class="align-center">
                                <div class="frm-row">
                                    <div class="option-group field">
                                        <div class="section colm colm12">
                                            <label class="option block">
                                                <input type="checkbox" name="agree_to_terms" id="agree_to_terms" value="Yes">
                                                <span class="checkbox"></span> I agree to the
                                                <a href = "javascript:void(0)" style="color: #999; text-decoration:underline;" onclick = "document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'">terms and conditions.</a>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="light" class="white_content">Consent to Sign Electronically:<br>The parties agree that these agreements may be electronically signed. The parties agree that the electronic signatures appearing on such agreements are the same as handwritten signatures for the purposes of validity, enforceability and admissibility.<br><br>Consent to be contacted:<br>By providing the information above, I am providing an electronic signature expressly authorizing C.R. England, its affiliates, or third parties to contact me at any address, telephone number or email address I have provided to communicate with me regarding my application for employment with C.R. England or other employment opportunities. I agree that C.R. England, its affiliates, or third parties may use SMS (text) messages, automatic telephone dialing systems or pre-recorded messages in connection with calls made to any telephone number I entered, even if the telephone number is assigned to a cellular telephone service or other service for which the called party is charged.  I understand that C.R. England may record telephone conversations I have with its representatives for quality control and training purposes and I consent to the recording of those calls.
                                <a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'">Close</a>
                            </div>

                            <div id="fade" class="black_overlay"></div>

                            <div class="spacer-b20"></div>
                        </fieldset>