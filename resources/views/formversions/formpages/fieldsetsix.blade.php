<h2 id="disclaimer"> Disclaimer </h2>
                        <fieldset  id="last">



                            <div class="frm-row">

                                <div class="section colm colm12">
                                    <label class="field prepend-icon">
                                        <input type="password" name="disclosure_dlNumber" id="disclosure_dlNumber" class="gui-input" placeholder="Drivers License Number">
                                        <span class="field-icon"><span class="glyphicon glyphicon-certificate"></span></span>

                                    </label>
                                </div>

                            </div>

                            <div class="frm-row">

                                <div class="section colm colm6">
                                    <label for="driverlicense_state" class="field select">
                                        <select id="driverlicense_state" name="driverlicense_state">
                                            <option value="">Drivers License State</option>
                                            <option value="AL">Alabama</option>
                                            <option value="AK">Alaska</option>
                                            <option value="AZ">Arizona</option>
                                            <option value="AR">Arkansas</option>
                                            <option value="CA">California</option>
                                            <option value="CO">Colorado</option>
                                            <option value="CT">Connecticut</option>
                                            <option value="DE">Delaware</option>
                                            <option value="DC">District Of Columbia</option>
                                            <option value="FL">Florida</option>
                                            <option value="GA">Georgia</option>
                                            <option value="HI">Hawaii</option>
                                            <option value="ID">Idaho</option>
                                            <option value="IL">Illinois</option>
                                            <option value="IN">Indiana</option>
                                            <option value="IA">Iowa</option>
                                            <option value="KS">Kansas</option>
                                            <option value="KY">Kentucky</option>
                                            <option value="LA">Louisiana</option>
                                            <option value="ME">Maine</option>
                                            <option value="MD">Maryland</option>
                                            <option value="MA">Massachusetts</option>
                                            <option value="MI">Michigan</option>
                                            <option value="MN">Minnesota</option>
                                            <option value="MS">Mississippi</option>
                                            <option value="MO">Missouri</option>
                                            <option value="MT">Montana</option>
                                            <option value="NE">Nebraska</option>
                                            <option value="NV">Nevada</option>
                                            <option value="NH">New Hampshire</option>
                                            <option value="NJ">New Jersey</option>
                                            <option value="NM">New Mexico</option>
                                            <option value="NY">New York</option>
                                            <option value="NC">North Carolina</option>
                                            <option value="ND">North Dakota</option>
                                            <option value="OH">Ohio</option>
                                            <option value="OK">Oklahoma</option>
                                            <option value="OR">Oregon</option>
                                            <option value="PA">Pennsylvania</option>
                                            <option value="RI">Rhode Island</option>
                                            <option value="SC">South Carolina</option>
                                            <option value="SD">South Dakota</option>
                                            <option value="TN">Tennessee</option>
                                            <option value="TX">Texas</option>
                                            <option value="UT">Utah</option>
                                            <option value="VT">Vermont</option>
                                            <option value="VA">Virginia</option>
                                            <option value="WA">Washington</option>
                                            <option value="WV">West Virginia</option>
                                            <option value="WI">Wisconsin</option>
                                            <option value="WY">Wyoming</option>
                                        </select>
                                        <i class="arrow double"></i>
                                    </label>
                                </div>

                                <div class="section colm colm6">
                                    <label class="field prepend-icon">
                                        <input type="text" name="disclosure_dlexp" id="disclosure_dlexp" class="gui-input" placeholder="Drivers License Expiration" readonly="readonly">
                                        <span class="field-icon"><span class="glyphicon glyphicon-calendar"></span></span>

                                    </label>
                                </div>

                            </div>

                            <div class="frm-row">



                                <div class="section colm colm12">
                                    <label class="field prepend-icon">
                                        <input type="text" name="driver_birthdate" id="driver_birthdate" class="gui-input" placeholder="Date Of Birth" readonly="readonly">
                                        <span class="field-icon"><span class="glyphicon glyphicon-calendar"></span></span>

                                    </label>
                                </div>

                            </div>

                            <div class="frm-row">

                                <div class="section colm colm4">
                                    <label class="field prepend-icon">
                                        <input type="text" name="driver_birthcity" id="driver_birthcity" class="gui-input" placeholder="City Of Birth">
                                        <span class="field-icon"><span class="glyphicon glyphicon-certificate"></span></span>

                                    </label>
                                </div>

                                <div class="section colm colm4">
                                    <label for="driver_birthstate" class="field select">
                                        <select id="driver_birthstate" name="driver_birthstate">
                                            <option value="">State Of Birth</option>
                                            <option value="AL">Alabama</option>
                                            <option value="AK">Alaska</option>
                                            <option value="AZ">Arizona</option>
                                            <option value="AR">Arkansas</option>
                                            <option value="CA">California</option>
                                            <option value="CO">Colorado</option>
                                            <option value="CT">Connecticut</option>
                                            <option value="DE">Delaware</option>
                                            <option value="DC">District Of Columbia</option>
                                            <option value="FL">Florida</option>
                                            <option value="GA">Georgia</option>
                                            <option value="HI">Hawaii</option>
                                            <option value="ID">Idaho</option>
                                            <option value="IL">Illinois</option>
                                            <option value="IN">Indiana</option>
                                            <option value="IA">Iowa</option>
                                            <option value="KS">Kansas</option>
                                            <option value="KY">Kentucky</option>
                                            <option value="LA">Louisiana</option>
                                            <option value="ME">Maine</option>
                                            <option value="MD">Maryland</option>
                                            <option value="MA">Massachusetts</option>
                                            <option value="MI">Michigan</option>
                                            <option value="MN">Minnesota</option>
                                            <option value="MS">Mississippi</option>
                                            <option value="MO">Missouri</option>
                                            <option value="MT">Montana</option>
                                            <option value="NE">Nebraska</option>
                                            <option value="NV">Nevada</option>
                                            <option value="NH">New Hampshire</option>
                                            <option value="NJ">New Jersey</option>
                                            <option value="NM">New Mexico</option>
                                            <option value="NY">New York</option>
                                            <option value="NC">North Carolina</option>
                                            <option value="ND">North Dakota</option>
                                            <option value="OH">Ohio</option>
                                            <option value="OK">Oklahoma</option>
                                            <option value="OR">Oregon</option>
                                            <option value="PA">Pennsylvania</option>
                                            <option value="RI">Rhode Island</option>
                                            <option value="SC">South Carolina</option>
                                            <option value="SD">South Dakota</option>
                                            <option value="TN">Tennessee</option>
                                            <option value="TX">Texas</option>
                                            <option value="UT">Utah</option>
                                            <option value="VT">Vermont</option>
                                            <option value="VA">Virginia</option>
                                            <option value="WA">Washington</option>
                                            <option value="WV">West Virginia</option>
                                            <option value="WI">Wisconsin</option>
                                            <option value="WY">Wyoming</option>
                                        </select>
                                        <i class="arrow double"></i>
                                    </label>
                                </div>

                                <div class="section colm colm4">
                                    <label class="field prepend-icon">
                                        <input type="text" name="driver_birthcountry" id="driver_birthcountry" class="gui-input" placeholder="Country Of Birth">
                                        <span class="field-icon"><span class="glyphicon glyphicon-certificate"></span></span>

                                    </label>
                                </div>

                            </div>



                            <div class="frm-row">
                                <div class="section colm colm12">
                                    <p class="small-text fine-grey align-center">By entering your name and identifying information below, you are signing this Disclosure, Consent, and Application electronically. You understand and agree that your electronic signature is the legal equivalent of your manual signature which may be upheld and used for identification and legal purposes. Your signature will be appended to the Past Employment Verification and the DOT D/A Disclosure and Authorization forms. You affirm that all the information contained in this application is correct and accurate to the best of your knowledge.<br></p>
                                </div>


                            </div>


                            <div class="frm-row">
                                <div class="section colm colm12">
                                    <label class="field prepend-icon">
                                        <input type="text" name="disclosure_name" id="disclosure_name" class="gui-input" placeholder="Legal Full Name">
                                        <span class="field-icon"><span class="glyphicon glyphicon-pencil"></span></span>

                                    </label>
                                </div>
                            </div>


                            <div class="frm-row">

                                <div class="section colm colm12">
                                    <label class="field prepend-icon">
                                        <input type="password" name="disclosure_ssn" id="disclosure_ssn" maxlength="9" class="gui-input" placeholder="Social Security Number">
                                        <span class="field-icon"><span class="glyphicon glyphicon-certificate"></span></span>

                                    </label>
                                </div>

                            </div>


                            <div class="spacer-b30">
                                <div class="tagline"><span style="color: #888888;"> IP ADDRESS </span></div>
                                <h5 style="text-align: center;"><?php echo $_SERVER["REMOTE_ADDR"]; ?></h5>
                            </div>

                            <div class="spacer-b30">
                                <div class="tagline"><span style="color: #888888;"> TIMESTAMP </span></div>
                                <h5 style="text-align: center;"><?php $todaySignature = date("Y-m-d H:i:s") . " GMT";
	                                echo $todaySignature; ?></h5>
                            </div>




                           <!--  <div class="frm-row">

                                <div class="section colm colm6">
                                    <label class="field prepend-icon">
                                        <input type="password" name="disclosure_dlNumber" id="disclosure_dlNumber" class="gui-input" placeholder="Drivers License Number">
                                        <span class="field-icon"><span class="glyphicon glyphicon-certificate"></span></span>

                                    </label>
                                </div>

                                <div class="section colm colm6">
                                    <label class="field prepend-icon">
                                        <input type="text" name="driver_birthdate" id="driver_birthdate" class="gui-input" placeholder="Date Of Birth" readonly="readonly">
                                        <span class="field-icon"><span class="glyphicon glyphicon-calendar"></span></span>

                                    </label>
                                </div>

                            </div>

                            <div class="frm-row">

                                <div class="section colm colm6">
                                    <label class="field prepend-icon">
                                        <input type="text" name="driver_birthcity" id="driver_birthcity" class="gui-input" placeholder="City Of Birth">
                                        <span class="field-icon"><span class="glyphicon glyphicon-certificate"></span></span>

                                    </label>
                                </div>

                                <div class="section colm colm6">
                                    <label for="driver_birthstate" class="field select">
                                        <select id="driver_birthstate" name="driver_birthstate">
                                            <option value="">State Of Birth</option>
                                            <option value="AL">Alabama</option>
                                            <option value="AK">Alaska</option>
                                            <option value="AZ">Arizona</option>
                                            <option value="AR">Arkansas</option>
                                            <option value="CA">California</option>
                                            <option value="CO">Colorado</option>
                                            <option value="CT">Connecticut</option>
                                            <option value="DE">Delaware</option>
                                            <option value="DC">District Of Columbia</option>
                                            <option value="FL">Florida</option>
                                            <option value="GA">Georgia</option>
                                            <option value="HI">Hawaii</option>
                                            <option value="ID">Idaho</option>
                                            <option value="IL">Illinois</option>
                                            <option value="IN">Indiana</option>
                                            <option value="IA">Iowa</option>
                                            <option value="KS">Kansas</option>
                                            <option value="KY">Kentucky</option>
                                            <option value="LA">Louisiana</option>
                                            <option value="ME">Maine</option>
                                            <option value="MD">Maryland</option>
                                            <option value="MA">Massachusetts</option>
                                            <option value="MI">Michigan</option>
                                            <option value="MN">Minnesota</option>
                                            <option value="MS">Mississippi</option>
                                            <option value="MO">Missouri</option>
                                            <option value="MT">Montana</option>
                                            <option value="NE">Nebraska</option>
                                            <option value="NV">Nevada</option>
                                            <option value="NH">New Hampshire</option>
                                            <option value="NJ">New Jersey</option>
                                            <option value="NM">New Mexico</option>
                                            <option value="NY">New York</option>
                                            <option value="NC">North Carolina</option>
                                            <option value="ND">North Dakota</option>
                                            <option value="OH">Ohio</option>
                                            <option value="OK">Oklahoma</option>
                                            <option value="OR">Oregon</option>
                                            <option value="PA">Pennsylvania</option>
                                            <option value="RI">Rhode Island</option>
                                            <option value="SC">South Carolina</option>
                                            <option value="SD">South Dakota</option>
                                            <option value="TN">Tennessee</option>
                                            <option value="TX">Texas</option>
                                            <option value="UT">Utah</option>
                                            <option value="VT">Vermont</option>
                                            <option value="VA">Virginia</option>
                                            <option value="WA">Washington</option>
                                            <option value="WV">West Virginia</option>
                                            <option value="WI">Wisconsin</option>
                                            <option value="WY">Wyoming</option>
                                        </select>
                                        <i class="arrow double"></i>
                                    </label>
                                </div>

                            </div>

                            <div class="frm-row">

                                <div class="section colm colm12">
                                    <label class="field prepend-icon">
                                        <input type="text" name="driver_birthcountry" id="driver_birthcountry" class="gui-input" placeholder="Country Of Birth">
                                        <span class="field-icon"><span class="glyphicon glyphicon-certificate"></span></span>

                                    </label>
                                </div>

                            </div>

                            <div class="frm-row">

                                <div class="section colm colm6">
                                    <label for="driverlicense_state" class="field select">
                                        <select id="driverlicense_state" name="driverlicense_state">
                                            <option value="">Drivers License State</option>
                                            <option value="AL">Alabama</option>
                                            <option value="AK">Alaska</option>
                                            <option value="AZ">Arizona</option>
                                            <option value="AR">Arkansas</option>
                                            <option value="CA">California</option>
                                            <option value="CO">Colorado</option>
                                            <option value="CT">Connecticut</option>
                                            <option value="DE">Delaware</option>
                                            <option value="DC">District Of Columbia</option>
                                            <option value="FL">Florida</option>
                                            <option value="GA">Georgia</option>
                                            <option value="HI">Hawaii</option>
                                            <option value="ID">Idaho</option>
                                            <option value="IL">Illinois</option>
                                            <option value="IN">Indiana</option>
                                            <option value="IA">Iowa</option>
                                            <option value="KS">Kansas</option>
                                            <option value="KY">Kentucky</option>
                                            <option value="LA">Louisiana</option>
                                            <option value="ME">Maine</option>
                                            <option value="MD">Maryland</option>
                                            <option value="MA">Massachusetts</option>
                                            <option value="MI">Michigan</option>
                                            <option value="MN">Minnesota</option>
                                            <option value="MS">Mississippi</option>
                                            <option value="MO">Missouri</option>
                                            <option value="MT">Montana</option>
                                            <option value="NE">Nebraska</option>
                                            <option value="NV">Nevada</option>
                                            <option value="NH">New Hampshire</option>
                                            <option value="NJ">New Jersey</option>
                                            <option value="NM">New Mexico</option>
                                            <option value="NY">New York</option>
                                            <option value="NC">North Carolina</option>
                                            <option value="ND">North Dakota</option>
                                            <option value="OH">Ohio</option>
                                            <option value="OK">Oklahoma</option>
                                            <option value="OR">Oregon</option>
                                            <option value="PA">Pennsylvania</option>
                                            <option value="RI">Rhode Island</option>
                                            <option value="SC">South Carolina</option>
                                            <option value="SD">South Dakota</option>
                                            <option value="TN">Tennessee</option>
                                            <option value="TX">Texas</option>
                                            <option value="UT">Utah</option>
                                            <option value="VT">Vermont</option>
                                            <option value="VA">Virginia</option>
                                            <option value="WA">Washington</option>
                                            <option value="WV">West Virginia</option>
                                            <option value="WI">Wisconsin</option>
                                            <option value="WY">Wyoming</option>
                                        </select>
                                        <i class="arrow double"></i>
                                    </label>
                                </div>

                                <div class="section colm colm6">
                                    <label class="field prepend-icon">
                                        <input type="text" name="disclosure_dlexp" id="disclosure_dlexp" class="gui-input" placeholder="Drivers License Expiration" readonly="readonly">
                                        <span class="field-icon"><span class="glyphicon glyphicon-calendar"></span></span>

                                    </label>
                                </div>

                            </div> -->
                            <div id="result" class="result spacer-b10"></div>
                        </fieldset>