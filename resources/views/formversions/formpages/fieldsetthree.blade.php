<h2></h2>
                        <fieldset>






                            <div class="section align-center">Have you had any moving violations in the last five years?
                                <div id="movingViolations" class="option-group field">
                                    <label class="option">
                                        <input type="radio" id="moving_violations_y" name="moving_violations" class="smartfm-ctrl" value="Y"  data-show-id="moving_violations_info">
                                        <span class="radio"></span> Yes
                                    </label>
                                    <label class="option">
                                        <input type="radio" id="moving_violations_n" name="moving_violations" class="smartfm-ctrl" value="N"  data-show-id="">
                                        <span class="radio"></span> No
                                    </label>
                                </div>
                            </div>
                            <div class="section align-center hiddenbox formShowHide_reset" id="moving_violations_info">
                                <div id="clone-animate">
                                    <div class="toclone">
                                        <div class="frm-row">
                                            <div class="section colm colm6">
                                                <label class="field prepend-icon">
                                                    <input type="text" name="moving_violations_date[0]" id="vdate" class="gui-input moving_violations_date" placeholder="Citation Date" readonly="readonly" />
                                                    <span class="field-icon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                </label>
                                            </div>
                                            <div class="section colm colm6">
                                                <label class="field select">
                                                    <select name="violation_type[0]" id="vtype">
                                                        <option value="">Violation Type:</option>
                                                        <option value="Speeding Less Than 15 Over">Speeding: Less than 15 MPH over the posted speed limit</option>
                                                        <option value="Speeding Greater Than 15 Over">Speeding: Greater than 15 MPH over the posted speed limit</option>
                                                        <option value="Reckless">Reckless Driving</option>
                                                        <option value="Careless">Careless Driving</option>
                                                        <option value="Other">Other. Describe Below</option>
                                                    </select>
                                                    <i class="arrow double"></i>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="frm-row align-center">
                                            <div class="section align-center colm colm4">
                                                <label class="field prepend-icon">
                                                    <input type="text" name="violation_city[0]" id="vcity" class="gui-input" placeholder="City">
                                                    <span class="field-icon"><span class="glyphicon glyphicon-globe"></span></span>
                                                </label>
                                            </div>
                                            <div class="section colm colm4">
                                                <label class="field prepend-icon">
                                                    <input type="text" name="violation_county[0]" id="vcounty" class="gui-input" placeholder="County">
                                                    <span class="field-icon"><span class="glyphicon glyphicon-map-marker"></span></span>
                                                </label>
                                            </div>
                                            <div class="section colm colm4">
                                                <label class="field select prepend-icon">
                                                    <select name="violation_state[0]" id="vstate">
                                                        <option value="">State:</option>
                                                        <option value="AL">Alabama</option>
                                                        <option value="AK">Alaska</option>
                                                        <option value="AZ">Arizona</option>
                                                        <option value="AR">Arkansas</option>
                                                        <option value="CA">California</option>
                                                        <option value="CO">Colorado</option>
                                                        <option value="CT">Connecticut</option>
                                                        <option value="DE">Delaware</option>
                                                        <option value="DC">District Of Columbia</option>
                                                        <option value="FL">Florida</option>
                                                        <option value="GA">Georgia</option>
                                                        <option value="HI">Hawaii</option>
                                                        <option value="ID">Idaho</option>
                                                        <option value="IL">Illinois</option>
                                                        <option value="IN">Indiana</option>
                                                        <option value="IA">Iowa</option>
                                                        <option value="KS">Kansas</option>
                                                        <option value="KY">Kentucky</option>
                                                        <option value="LA">Louisiana</option>
                                                        <option value="ME">Maine</option>
                                                        <option value="MD">Maryland</option>
                                                        <option value="MA">Massachusetts</option>
                                                        <option value="MI">Michigan</option>
                                                        <option value="MN">Minnesota</option>
                                                        <option value="MS">Mississippi</option>
                                                        <option value="MO">Missouri</option>
                                                        <option value="MT">Montana</option>
                                                        <option value="NE">Nebraska</option>
                                                        <option value="NV">Nevada</option>
                                                        <option value="NH">New Hampshire</option>
                                                        <option value="NJ">New Jersey</option>
                                                        <option value="NM">New Mexico</option>
                                                        <option value="NY">New York</option>
                                                        <option value="NC">North Carolina</option>
                                                        <option value="ND">North Dakota</option>
                                                        <option value="OH">Ohio</option>
                                                        <option value="OK">Oklahoma</option>
                                                        <option value="OR">Oregon</option>
                                                        <option value="PA">Pennsylvania</option>
                                                        <option value="RI">Rhode Island</option>
                                                        <option value="SC">South Carolina</option>
                                                        <option value="SD">South Dakota</option>
                                                        <option value="TN">Tennessee</option>
                                                        <option value="TX">Texas</option>
                                                        <option value="UT">Utah</option>
                                                        <option value="VT">Vermont</option>
                                                        <option value="VA">Virginia</option>
                                                        <option value="WA">Washington</option>
                                                        <option value="WV">West Virginia</option>
                                                        <option value="WI">Wisconsin</option>
                                                        <option value="WY">Wyoming</option>
                                                    </select>
                                                    <i class="arrow double"></i>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="section colm colm12">
                                            <label class="field prepend-icon">
                                                <input type="text" class="gui-input" name="violation_details[0]" id="vdetails" placeholder="In your own words, describe the citation...">
                                                <span class="field-icon"><span class="glyphicon glyphicon-pencil"></span></span>
                                            </label>
                                        </div>
                                        <div class="section align-center">Are all fines and fees associated with this citation paid?
                                            <div class="option-group field">
                                                <label class="option">
                                                    <input type="radio" id="fines_and_fees_y" name="fines_and_fees[0]" class="smartfm-ctrl" value="Y">
                                                    <span class="radio"></span>Yes
                                                </label>
                                                <label class="option">
                                                    <input type="radio" id="fines_and_fees_n" name="fines_and_fees[0]" class="smartfm-ctrl" value="N">
                                                    <span class="radio"></span> No
                                                </label>
                                            </div>
                                        </div>
                                        <a href="#" class="clone button btn-primary"><i class="glyphicon glyphicon-plus"></i></a>
                                        <a href="#" class="delete button"><i class="glyphicon glyphicon-minus"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="section align-center">Have you had any traffic accidents in the last five years?
                                <div id="carAccidents" class="option-group field">
                                    <label class="option">
                                        <input type="radio" id="accidents_y" name="accidents" class="smartfm-ctrl" value="Y"  data-show-id="accidents_info">
                                        <span class="radio"></span> Yes
                                    </label>
                                    <label class="option">
                                        <input type="radio" id="accidents_n" name="accidents" class="smartfm-ctrl" value="N"  data-show-id="">
                                        <span class="radio"></span> No
                                    </label>
                                </div>
                                <div class="spacer-b30"></div>
                            </div>
                            <div class="section align-center hiddenbox formShowHide_reset" id="accidents_info">
                                <div id="clone-animate2">
                                    <div class="toclone">
                                        <div class="frm-row">
                                            <div class="section colm colm12">
                                                <label class="field prepend-icon">
                                                    <input type="text" name="accident_date[0]" id="accdate" class="gui-input accidents_date" placeholder="Accident Date" readonly="readonly">
                                                    <span class="field-icon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="frm-row">
                                            <div class="section align-center colm colm6">Were you issued a ticket?
                                                <div class="option-group field">
                                                    <label class="option">
                                                        <input type="radio" id="accticy" name="accident_ticket[0]" class="smartfm-ctrl" value="Y">
                                                        <span class="radio"></span> Yes
                                                    </label>
                                                    <label class="option">
                                                        <input type="radio" id="accticn" name="accident_ticket[0]" class="smartfm-ctrl" value="N">
                                                        <span class="radio"></span> No
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="section align-center colm colm6">Were there any injuries?
                                                <div class="option-group field">
                                                    <label class="option">
                                                        <input type="radio" id="accidents_injuries_y" name="accident_injuries[0]" class="smartfm-ctrl" value="Y">
                                                        <span class="radio"></span> Yes
                                                    </label>
                                                    <label class="option">
                                                        <input type="radio" id="accidents_injuries_n" name="accident_injuries[0]" class="smartfm-ctrl" value="N">
                                                        <span class="radio"></span> No
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="frm-row">
                                            <div class="section align-center colm colm6">Were there any fatalities?
                                                <div class="option-group field">
                                                    <label class="option">
                                                        <input type="radio" id="accidents_fatalities_y" name="accident_fatalities[0]" class="smartfm-ctrl" value="Y">
                                                        <span class="radio"></span> Yes
                                                    </label>
                                                    <label class="option">
                                                        <input type="radio" id="accidents_fatalities_n" name="accident_fatalities[0]" class="smartfm-ctrl" value="N">
                                                        <span class="radio"></span> No
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="section align-center colm colm6">Was the accident preventable?
                                                <div class="option-group field">
                                                    <label class="option">
                                                        <input type="radio" id="accidents_preventable_y" name="accident_preventable[0]" class="smartfm-ctrl" value="Y">
                                                        <span class="radio"></span> Yes
                                                    </label>
                                                    <label class="option">
                                                        <input type="radio" id="accidents_preventable_n" name="accident_preventable[0]" class="smartfm-ctrl" value="N">
                                                        <span class="radio"></span> No
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="frm-row">
                                            <div class="section align-center colm colm12">Who was listed as being at fault in the accident?
                                                <div class="option-group field">
                                                    <label class="option">
                                                        <input type="radio" id="accidents_fault_self" name="accident_fault[0]" class="smartfm-ctrl" value="S">
                                                        <span class="radio"></span> Me
                                                    </label>
                                                    <label class="option">
                                                        <input type="radio" id="accidents_fault_other" name="accident_fault[0]" class="smartfm-ctrl" value="O">
                                                        <span class="radio"></span> The other driver
                                                    </label>
                                                    <label class="option">
                                                        <input type="radio" id="accidents_fault_both" name="accident_fault[0]" class="smartfm-ctrl" value="B">
                                                        <span class="radio"></span> Both Of Us
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="frm-row">
                                            <div class="section colm colm4">
                                                <label class="field prepend-icon">
                                                    <input type="text" name="accident_city[0]" id="acccity" class="gui-input" placeholder="City">
                                                    <span class="field-icon"><span class="glyphicon glyphicon-globe"></span></span>
                                                </label>
                                            </div>
                                            <div class="section colm colm4">
                                                <label class="field prepend-icon">
                                                    <input type="text" name="accident_county[0]" id="acccounty" class="gui-input" placeholder="County">
                                                    <span class="field-icon"><span class="glyphicon glyphicon-map-marker"></span></span>
                                                </label>
                                            </div>
                                            <div class="section colm colm4">
                                                <label class="field select prepend-icon">
                                                    <select id="accstate" name="accident_state[0]">
                                                        <option value="">State:</option>
                                                        <option value="AL">Alabama</option>
                                                        <option value="AK">Alaska</option>
                                                        <option value="AZ">Arizona</option>
                                                        <option value="AR">Arkansas</option>
                                                        <option value="CA">California</option>
                                                        <option value="CO">Colorado</option>
                                                        <option value="CT">Connecticut</option>
                                                        <option value="DE">Delaware</option>
                                                        <option value="DC">District Of Columbia</option>
                                                        <option value="FL">Florida</option>
                                                        <option value="GA">Georgia</option>
                                                        <option value="HI">Hawaii</option>
                                                        <option value="ID">Idaho</option>
                                                        <option value="IL">Illinois</option>
                                                        <option value="IN">Indiana</option>
                                                        <option value="IA">Iowa</option>
                                                        <option value="KS">Kansas</option>
                                                        <option value="KY">Kentucky</option>
                                                        <option value="LA">Louisiana</option>
                                                        <option value="ME">Maine</option>
                                                        <option value="MD">Maryland</option>
                                                        <option value="MA">Massachusetts</option>
                                                        <option value="MI">Michigan</option>
                                                        <option value="MN">Minnesota</option>
                                                        <option value="MS">Mississippi</option>
                                                        <option value="MO">Missouri</option>
                                                        <option value="MT">Montana</option>
                                                        <option value="NE">Nebraska</option>
                                                        <option value="NV">Nevada</option>
                                                        <option value="NH">New Hampshire</option>
                                                        <option value="NJ">New Jersey</option>
                                                        <option value="NM">New Mexico</option>
                                                        <option value="NY">New York</option>
                                                        <option value="NC">North Carolina</option>
                                                        <option value="ND">North Dakota</option>
                                                        <option value="OH">Ohio</option>
                                                        <option value="OK">Oklahoma</option>
                                                        <option value="OR">Oregon</option>
                                                        <option value="PA">Pennsylvania</option>
                                                        <option value="RI">Rhode Island</option>
                                                        <option value="SC">South Carolina</option>
                                                        <option value="SD">South Dakota</option>
                                                        <option value="TN">Tennessee</option>
                                                        <option value="TX">Texas</option>
                                                        <option value="UT">Utah</option>
                                                        <option value="VT">Vermont</option>
                                                        <option value="VA">Virginia</option>
                                                        <option value="WA">Washington</option>
                                                        <option value="WV">West Virginia</option>
                                                        <option value="WI">Wisconsin</option>
                                                        <option value="WY">Wyoming</option>
                                                    </select>
                                                    <i class="arrow double"></i>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="section colm colm12">
                                            <label class="field prepend-icon">
                                                <input type="text" name="accident_damage_amount[0]" id="accdamageamount" class="gui-input accdamageamount" placeholder="Total Damage Amount:">
                                                <span class="field-icon"><span class="glyphicon glyphicon-usd"></span></span>
                                            </label>
                                        </div>
                                        <div class="section colm colm12">
                                            <label class="field prepend-icon">
                                                <input type="text" class="gui-input" id="accdetails" name="accident_details[0]" placeholder="In your own words, describe the accident...">
                                                <span class="field-icon"><span class="glyphicon glyphicon-pencil"></span></span>
                                            </label>
                                        </div>
                                        <a href="#" class="clone button btn-primary"><i class="glyphicon glyphicon-plus"></i></a>
                                        <a href="#" class="delete button"><i class="glyphicon glyphicon-minus"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="section align-center">Have you ever been cited with DUI or DWI?
                                <div id="duiDwi" class="option-group field">
                                    <label class="option">
                                        <input type="radio" id="dui_dwi_y" name="dui_dwi" class="smartfm-ctrl" value="Y"  data-show-id="dui">
                                        <span class="radio"></span> Yes
                                    </label>
                                    <label class="option">
                                        <input type="radio" id="dui_dwi_n" name="dui_dwi" class="smartfm-ctrl" value="N"  data-show-id="">
                                        <span class="radio"></span> No
                                    </label>
                                </div>
                                <div class="spacer-b30"></div>
                            </div>
                            <div class="section align-center hiddenbox formShowHide_reset" id="dui">
                                <div id="clone-animate3">
                                    <div class="toclone">
                                        <div class="frm-row">
                                            <div class="section colm colm12">
                                                <label class="field prepend-icon">
                                                    <input type="text" name="dui_dwi_date[0]" id="duidate" class="gui-input dui_dwi_date" placeholder="Date Of DUI/DWI" readonly="readonly">
                                                    <span class="field-icon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="frm-row">
                                            <div class="section colm colm4">
                                                <label class="field prepend-icon">
                                                    <input type="text" name="dui_dwi_city[0]" id="duicity" class="gui-input" placeholder="City">
                                                    <span class="field-icon"><i class="glyphicon glyphicon-globe"></i></span>
                                                </label>
                                            </div>
                                            <div class="section colm colm4">
                                                <label class="field prepend-icon">
                                                    <input type="text" name="dui_dwi_county[0]" id="duicounty" class="gui-input" placeholder="County">
                                                    <span class="field-icon"><i class="glyphicon glyphicon-map-marker"></i></span>
                                                </label>
                                            </div>
                                            <div class="section colm colm4">
                                                <label class="field select prepend-icon">
                                                    <select id="duistate" name="dui_dwi_state[0]">
                                                        <option value="">State:</option>
                                                        <option value="AL">Alabama</option>
                                                        <option value="AK">Alaska</option>
                                                        <option value="AZ">Arizona</option>
                                                        <option value="AR">Arkansas</option>
                                                        <option value="CA">California</option>
                                                        <option value="CO">Colorado</option>
                                                        <option value="CT">Connecticut</option>
                                                        <option value="DE">Delaware</option>
                                                        <option value="DC">District Of Columbia</option>
                                                        <option value="FL">Florida</option>
                                                        <option value="GA">Georgia</option>
                                                        <option value="HI">Hawaii</option>
                                                        <option value="ID">Idaho</option>
                                                        <option value="IL">Illinois</option>
                                                        <option value="IN">Indiana</option>
                                                        <option value="IA">Iowa</option>
                                                        <option value="KS">Kansas</option>
                                                        <option value="KY">Kentucky</option>
                                                        <option value="LA">Louisiana</option>
                                                        <option value="ME">Maine</option>
                                                        <option value="MD">Maryland</option>
                                                        <option value="MA">Massachusetts</option>
                                                        <option value="MI">Michigan</option>
                                                        <option value="MN">Minnesota</option>
                                                        <option value="MS">Mississippi</option>
                                                        <option value="MO">Missouri</option>
                                                        <option value="MT">Montana</option>
                                                        <option value="NE">Nebraska</option>
                                                        <option value="NV">Nevada</option>
                                                        <option value="NH">New Hampshire</option>
                                                        <option value="NJ">New Jersey</option>
                                                        <option value="NM">New Mexico</option>
                                                        <option value="NY">New York</option>
                                                        <option value="NC">North Carolina</option>
                                                        <option value="ND">North Dakota</option>
                                                        <option value="OH">Ohio</option>
                                                        <option value="OK">Oklahoma</option>
                                                        <option value="OR">Oregon</option>
                                                        <option value="PA">Pennsylvania</option>
                                                        <option value="RI">Rhode Island</option>
                                                        <option value="SC">South Carolina</option>
                                                        <option value="SD">South Dakota</option>
                                                        <option value="TN">Tennessee</option>
                                                        <option value="TX">Texas</option>
                                                        <option value="UT">Utah</option>
                                                        <option value="VT">Vermont</option>
                                                        <option value="VA">Virginia</option>
                                                        <option value="WA">Washington</option>
                                                        <option value="WV">West Virginia</option>
                                                        <option value="WI">Wisconsin</option>
                                                        <option value="WY">Wyoming</option>
                                                    </select>
                                                    <i class="arrow double"></i>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="section colm colm12">
                                            <label class="field prepend-icon">
                                                <input type="text" class="gui-input" id="duidetails" name="dui_dwi_details[0]" placeholder="In your own words, describe the DUI / DWI...">
                                                <span class="field-icon"><i class="glyphicon glyphicon-pencil"></i></span>
                                            </label>
                                        </div>
                                        <a href="#" class="clone button btn-primary"><i class="glyphicon glyphicon-plus"></i></a>
                                        <a href="#" class="delete button"><i class="glyphicon glyphicon-minus"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="section align-center">Have you ever had your license suspended or revoked?
                                <div id="suspendedRevoked" class="option-group field">
                                    <label class="option">
                                        <input type="radio" id="suspendedrevoked_y" name="suspendedrevoked" class="smartfm-ctrl" value="Y"  data-show-id="suspended_revoked">
                                        <span class="radio"></span> Yes
                                    </label>
                                    <label class="option">
                                        <input type="radio" id="suspendedrevoked_n" name="suspendedrevoked" class="smartfm-ctrl" value="N"  data-show-id="">
                                        <span class="radio"></span> No
                                    </label>
                                </div>
                                <div class="spacer-b30"></div>
                            </div>
                            <div class="section align-center hiddenbox" id="suspended_revoked">
                                <div id="clone-animate4">
                                    <div class="toclone">
                                        <div class="frm-row">
                                            <div class="section colm colm6">
                                                <label class="field prepend-icon">
                                                    <input type="text" name="suspendedrevoked_start_date[0]" id="suspendedstartdate" class="gui-input suspendedrevoked_start_date" placeholder="Start Date" readonly="readonly">
                                                    <span class="field-icon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                </label>
                                            </div>
                                            <div class="section colm colm6">
                                                <label class="field prepend-icon">
                                                    <input type="text" name="suspendedrevoked_end_date[0]" id="suspendedenddate" class="gui-input suspendedrevoked_end_date" placeholder="End Date" readonly="readonly">
                                                    <span class="field-icon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="frm-row">
                                            <div class="section colm colm4">
                                                <label class="field prepend-icon">
                                                    <input type="text" name="suspendedrevoked_city[0]" id="suspendedcity" class="gui-input" placeholder="City">
                                                    <span class="field-icon"><i class="glyphicon glyphicon-globe"></i></span>
                                                </label>
                                            </div>
                                            <div class="section colm colm4">
                                                <label class="field prepend-icon">
                                                    <input type="text" name="suspendedrevoked_county[0]" id="suspendedcounty" class="gui-input" placeholder="County">
                                                    <span class="field-icon"><i class="glyphicon glyphicon-map-marker"></i></span>
                                                </label>
                                            </div>
                                            <div class="section colm colm4">
                                                <label class="field select prepend-icon">
                                                    <select id="suspendedstate" name="suspendedrevoked_state[0]">
                                                        <option value="">State:</option>
                                                        <option value="AL">Alabama</option>
                                                        <option value="AK">Alaska</option>
                                                        <option value="AZ">Arizona</option>
                                                        <option value="AR">Arkansas</option>
                                                        <option value="CA">California</option>
                                                        <option value="CO">Colorado</option>
                                                        <option value="CT">Connecticut</option>
                                                        <option value="DE">Delaware</option>
                                                        <option value="DC">District Of Columbia</option>
                                                        <option value="FL">Florida</option>
                                                        <option value="GA">Georgia</option>
                                                        <option value="HI">Hawaii</option>
                                                        <option value="ID">Idaho</option>
                                                        <option value="IL">Illinois</option>
                                                        <option value="IN">Indiana</option>
                                                        <option value="IA">Iowa</option>
                                                        <option value="KS">Kansas</option>
                                                        <option value="KY">Kentucky</option>
                                                        <option value="LA">Louisiana</option>
                                                        <option value="ME">Maine</option>
                                                        <option value="MD">Maryland</option>
                                                        <option value="MA">Massachusetts</option>
                                                        <option value="MI">Michigan</option>
                                                        <option value="MN">Minnesota</option>
                                                        <option value="MS">Mississippi</option>
                                                        <option value="MO">Missouri</option>
                                                        <option value="MT">Montana</option>
                                                        <option value="NE">Nebraska</option>
                                                        <option value="NV">Nevada</option>
                                                        <option value="NH">New Hampshire</option>
                                                        <option value="NJ">New Jersey</option>
                                                        <option value="NM">New Mexico</option>
                                                        <option value="NY">New York</option>
                                                        <option value="NC">North Carolina</option>
                                                        <option value="ND">North Dakota</option>
                                                        <option value="OH">Ohio</option>
                                                        <option value="OK">Oklahoma</option>
                                                        <option value="OR">Oregon</option>
                                                        <option value="PA">Pennsylvania</option>
                                                        <option value="RI">Rhode Island</option>
                                                        <option value="SC">South Carolina</option>
                                                        <option value="SD">South Dakota</option>
                                                        <option value="TN">Tennessee</option>
                                                        <option value="TX">Texas</option>
                                                        <option value="UT">Utah</option>
                                                        <option value="VT">Vermont</option>
                                                        <option value="VA">Virginia</option>
                                                        <option value="WA">Washington</option>
                                                        <option value="WV">West Virginia</option>
                                                        <option value="WI">Wisconsin</option>
                                                        <option value="WY">Wyoming</option>
                                                    </select>
                                                    <i class="arrow double"></i>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="frm-row">
                                            <div class="section align-center colm colm12">Has Your License Been Reinstated?
                                                <div class="option-group field">
                                                    <label class="option">
                                                        <input type="radio" id="license_reinstated_y" name="license_reinstated[0]" class="smartfm-ctrl" value="Y">
                                                        <span class="radio"></span> Yes
                                                    </label>
                                                    <label class="option">
                                                        <input type="radio" id="license_reinstated_n" name="license_reinstated[0]" class="smartfm-ctrl" value="N">
                                                        <span class="radio"></span> No
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="#" class="clone button btn-primary">
                                            <i class="glyphicon glyphicon-plus"></i>
                                        </a>
                                        <a href="#" class="delete button">
                                            <i class="glyphicon glyphicon-minus"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </fieldset>