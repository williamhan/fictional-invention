<h2>Offer</h2>
                        <fieldset>
                            <div id="conditional_offer" class="section align-center">
                                <strong class="offer1">Congratulations!</strong>
                                <br>
                                <br>
                                <h7 class="offer2">
                                    We are pleased to make you a conditional offer of employment with C.R. England, Inc. conditioned upon successful completion of:
                                </h7>
                                <ol class="offer3">
                                    <li>Drug Screen</li>
                                    <li>Criminal Background Check</li>
									<li>CDL School & Licensing</li>
                                    <li>CRE Road Test</li>
                                    <li>MVR & DAC Review</li>
                                    <li>Reference Checks</li>
                                    <li>Pre-Hire Interview</li>
                                    <li>New Hire Orientation</li>
                                    <li>Signed Driver Employment Contract</li>
                                </ol>

                            </div>
                        </fieldset>