<?php
$value = explode('?',$_SERVER["REQUEST_URI"]);

$pageurl = array_shift($value);


?>
<div class="col-sm-5" id="join-us-form">
    <div id="header-right">
        <!-- Form Title -->
        <h2 class="form-title">Apply Now
            <span class="arrow-down"></span></h2>
        <!--- /End Form Title -->
        <div id="join-us-results"></div>
        <!-- Form -->

        <div class="smart-wrap">
            <div class="smart-forms smart-container wrap-0">

                <div class="form-body smart-steps stp-two">
                    <form id="smart-form" name="smart-form" method="post" action="//drivecre.com/Scripts/newfilemakercopy/pdftesting.php" enctype="application/x-www-form-urlencoded">

                        <h2 id="contactInfo">Contact Info</h2>

                        <fieldset id="first">

                            <div class="spacer-t20 spacer-b20"></div>

                            <div class="section">
                                <label class="field prepend-icon">
                                    <input type="hidden" name="start_time" id="start_time" class="gui-input" value="<?php echo htmlspecialchars($start_time); ?>">
                                    <input type="hidden" name="driver_guid" id="driver_guid" class="gui-input" value=<?php echo $driver_guid; ?>>
                                    <input type="email" name="driver_email" id="driver_email" class="gui-input" placeholder="Email Address">
                                    <span class="field-icon">
										<span class="glyphicon glyphicon-envelope"></span>
									</span>
                                </label>
                            </div>
                            <div class="frm-row">
                                <div class="section colm colm6">
                                    <label class="field prepend-icon">
                                        <input type="text" name="driver_fname" id="driver_fname" class="gui-input" placeholder="First Name">
                                        <input type="hidden" name="record_source" id="record_source" class="gui-input" value=<?php echo $rs; ?>>
                                        <input type="hidden" name="how_heard" id="how_heard" class="gui-input" value=<?php echo $id; ?>>
                                        <input type="hidden" name="app_version" id="app_version" class="gui-input" value="Long">
                                        <input type="hidden" name="var_track" id="var_track" class="gui-input" value="<?php echo $pageurl; ?>">
                                        <span class="field-icon">
										<span class="glyphicon glyphicon-user"></span>
									</span>
                                    </label>
                                </div>
                                <div class="section colm colm6">
                                    <label class="field prepend-icon">
                                        <input type="text" name="driver_lname" id="driver_lname" class="gui-input" placeholder="Last Name">
                                        <span class="field-icon">
										<span class="glyphicon glyphicon-user"></span>
									</span>
                                    </label>
                                </div>
                            </div>
                            <div class="align-center">
                                <div class="frm-row">
                                    <div class="section colm colm6">
                                        <label class="field prepend-icon">
                                            <input type="tel" name="work_phone" id="work_phone" class="gui-input" placeholder="Cell Phone">
                                            <span class="field-icon">
											<span class="glyphicon glyphicon-phone"></span>
										</span>
                                        </label>
                                    </div>
                                    <div class="section colm colm6">
                                        <label class="field prepend-icon">
                                            <input type="tel" name="home_phone" id="home_phone" class="gui-input" placeholder="Home Phone">
                                            <span class="field-icon">
											<span class="glyphicon glyphicon-phone-alt"></span>
										</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="align-center">
                                <div class="frm-row">
                                    <div class="section colm colm8">
                                        <label class="field prepend-icon">
                                            <input type="text" name="driver_address" id="driver_address" class="gui-input" placeholder="Street Address">
                                            <span class="field-icon">
											<span class="glyphicon glyphicon-road"></span>
										</span>
                                        </label>
                                    </div>
                                    <div class="section colm colm4">
                                        <label class="field prepend-icon">
                                            <input type="text" name="driver_zip" id="driver_zip" class="gui-input" placeholder="Zip">
                                            <span class="field-icon">
											<span class="glyphicon glyphicon-map-marker"></span>
										</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="align-center">
                                <div class="frm-row">
                                    <div class="section colm colm6">
                                        <label class="field prepend-icon">
                                            <input type="text" name="driver_city" id="driver_city" class="gui-input" placeholder="City">
                                            <span class="field-icon">
											<span class="glyphicon glyphicon-home"></span>
										</span>
                                        </label>
                                    </div>
                                    <div class="section colm colm6">
                                        <label for="driver_state" class="field select prepend-icon">
                                            <select id="driver_state" name="driver_state">
                                                <option value="">Choose State</option>
                                                <option value="AL">Alabama</option>
                                                <option value="AK">Alaska</option>
                                                <option value="AZ">Arizona</option>
                                                <option value="AR">Arkansas</option>
                                                <option value="CA">California</option>
                                                <option value="CO">Colorado</option>
                                                <option value="CT">Connecticut</option>
                                                <option value="DE">Delaware</option>
                                                <option value="DC">District Of Columbia</option>
                                                <option value="FL">Florida</option>
                                                <option value="GA">Georgia</option>
                                                <option value="HI">Hawaii</option>
                                                <option value="ID">Idaho</option>
                                                <option value="IL">Illinois</option>
                                                <option value="IN">Indiana</option>
                                                <option value="IA">Iowa</option>
                                                <option value="KS">Kansas</option>
                                                <option value="KY">Kentucky</option>
                                                <option value="LA">Louisiana</option>
                                                <option value="ME">Maine</option>
                                                <option value="MD">Maryland</option>
                                                <option value="MA">Massachusetts</option>
                                                <option value="MI">Michigan</option>
                                                <option value="MN">Minnesota</option>
                                                <option value="MS">Mississippi</option>
                                                <option value="MO">Missouri</option>
                                                <option value="MT">Montana</option>
                                                <option value="NE">Nebraska</option>
                                                <option value="NV">Nevada</option>
                                                <option value="NH">New Hampshire</option>
                                                <option value="NJ">New Jersey</option>
                                                <option value="NM">New Mexico</option>
                                                <option value="NY">New York</option>
                                                <option value="NC">North Carolina</option>
                                                <option value="ND">North Dakota</option>
                                                <option value="OH">Ohio</option>
                                                <option value="OK">Oklahoma</option>
                                                <option value="OR">Oregon</option>
                                                <option value="PA">Pennsylvania</option>
                                                <option value="RI">Rhode Island</option>
                                                <option value="SC">South Carolina</option>
                                                <option value="SD">South Dakota</option>
                                                <option value="TN">Tennessee</option>
                                                <option value="TX">Texas</option>
                                                <option value="UT">Utah</option>
                                                <option value="VT">Vermont</option>
                                                <option value="VA">Virginia</option>
                                                <option value="WA">Washington</option>
                                                <option value="WV">West Virginia</option>
                                                <option value="WI">Wisconsin</option>
                                                <option value="WY">Wyoming</option>
                                            </select>
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="section align-center">Do you have a CDL-A?
                                <div id="cdl_holder" class="option-group field">
                                    <label class="option">
                                        <input type="radio" id="cdl_holder_y" name="cdl_holder" class="smartfm-ctrl" value="Y"  data-show-id="app_type_box">
                                        <span class="radio"></span> Yes
                                    </label>
                                    <label class="option">
                                        <input type="radio" id="cdl_holder_n" name="cdl_holder" class="smartfm-ctrl" value="N"  data-hide-id="app_type_box">
                                        <span class="radio"></span> No
                                    </label>
                                </div>

                                <div class="spacer-b30"></div>


                                <div id="app_type_box" class="hiddenbox">
                                    <div class="section">
                                        <label class="field select">
                                            <select id="app_type" name="app_type">
                                                <!--
                                                                                            <option value="SC">select your CDL driving experience</option>
                                                                                            <option value="SR">I have less than 3 months experience in the last 12 months</option>
                                                                                            <option value="T">I have more than 3 months but less than 6 months experience</option>
                                                                                            <option value="D">I have 6 months experience in the last 12 months</option>
                                                                                            <option value="D">I have 19 months experience in the last 36 months</option>
                                                -->
                                            </select>
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="section align-center">Are you a Veteran?
                                <div id="veteran_status" class="option-group field">
                                    <label class="option">
                                        <input type="radio" id="veteran_status_y" name="veteran_status" class="smartfm-ctrl" value="Y"  data-show-id="honorable_discharge_box">
                                        <span class="radio"></span> Yes
                                    </label>
                                    <label class="option">
                                        <input type="radio" id="veteran_status_n" name="veteran_status" class="smartfm-ctrl" value="N"  data-show-id="">
                                        <span class="radio"></span> No
                                    </label>
                                </div><!-- end .option-group section -->

                                <div class="spacer-b30"></div>

                                <div id="honorable_discharge_box" class="hiddenbox">
                                    <div class="section">
                                        <label class="field select">
                                            <select id="honorable_discharge" name="honorable_discharge">
                                                <option value="">Veteran Status:</option>
                                                <option value="Y">I received an honorable discharge</option>
                                                <option value="N">I did not receive an honorable discharge</option>
                                            </select>
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="align-center">
                                <div class="frm-row">
                                    <div class="section colm colm12 align-center">Are you able to pass a drug test?
                                        <div id="pass_drug_test" class="option-group field">
                                            <label class="option">
                                                <input type="radio" name="pass_drug_test" class="smartfm-ctrl" value="Y">
                                                <span class="radio"></span> Yes
                                            </label>
                                            <label class="option">
                                                <input type="radio" name="pass_drug_test" class="smartfm-ctrl" value="N">
                                                <span class="radio"></span> No
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="frm-row align-center">In the last five years, how many of the following have you had?
                                <div class="section colm colm6">
                                    <label class="field prepend-icon">
                                        <input type="text" name="number_accidents_last_five" id="number_accidents_last_five" class="gui-input" placeholder="Accidents:">
                                        <span class="field-icon">
																		<span class="glyphicon glyphicon-exclamation-sign"></span>
																	</span>
                                    </label>
                                </div>
                                <div class="section colm colm6">
                                    <label class="field prepend-icon">
                                        <input type="text" name="number_tickets" id="number_tickets" class="gui-input" placeholder="Moving Violations:">
                                        <span class="field-icon">
																		<span class="glyphicon glyphicon-warning-sign"></span>
																	</span>
                                    </label>
                                </div>
                            </div>
                            <div class="frm-row align-center">
                                <div class="section colm colm12">
                                    <label class="field prepend-icon">
                                        <input type="text" name="number_duis" id="number_duis" class="gui-input" placeholder="Total DUIs / DWIs In Your Lifetime:">
                                        <span class="field-icon">
																		<span class="glyphicon glyphicon-glass"></span>
																	</span>
                                    </label>
                                </div>
                            </div>
                            <div class="align-center">
                                <div class="frm-row">
                                    <div class="option-group field">
                                        <div class="section colm colm12">
                                            <label class="option block">
                                                <input type="checkbox" name="agree_to_terms" id="agree_to_terms" value="Yes">
                                                <span class="checkbox"></span> I agree to the
                                                <a href = "javascript:void(0)" style="color: #999; text-decoration:underline;" onclick = "document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'">terms and conditions.</a>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="light" class="white_content">Consent to Sign Electronically:<br>The parties agree that these agreements may be electronically signed. The parties agree that the electronic signatures appearing on such agreements are the same as handwritten signatures for the purposes of validity, enforceability and admissibility.<br><br>Consent to be contacted:<br>By providing the information above, I am providing an electronic signature expressly authorizing C.R. England, its affiliates, or third parties to contact me at any address, telephone number or email address I have provided to communicate with me regarding my application for employment with C.R. England or other employment opportunities. I agree that C.R. England, its affiliates, or third parties may use SMS (text) messages, automatic telephone dialing systems or pre-recorded messages in connection with calls made to any telephone number I entered, even if the telephone number is assigned to a cellular telephone service or other service for which the called party is charged.  I understand that C.R. England may record telephone conversations I have with its representatives for quality control and training purposes and I consent to the recording of those calls.
                                <a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'">Close</a>
                            </div>

                            <div id="fade" class="black_overlay"></div>

                            <div class="spacer-b20"></div>
                        </fieldset>

                        <!--                     <div class="varform" id="varform"></div> -->


                        <h2>Offer</h2>
                        <fieldset>
                            <div id="conditional_offer" class="section align-center">
                                <strong class="offer1">Congratulations!</strong>
                                <br>
                                <br>
                                <h7 class="offer2">
                                    We are pleased to make you a conditional offer of employment with C.R. England, Inc. conditioned upon successful completion of:
                                </h7>
                                <ol class="offer3">
                                    <li>Drug Screen</li>
                                    <li>Criminal Background Check</li>
									<li>CDL School & Licensing</li>
                                    <li>CRE Road Test</li>
                                    <li>MVR & DAC Review</li>
                                    <li>Reference Checks</li>
                                    <li>Pre-Hire Interview</li>
                                    <li>New Hire Orientation</li>
                                    <li>Signed Driver Employment Contract</li>
                                </ol>

                                <!-- 														<a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'">Close</a> -->
                            </div>
                        </fieldset>



                        <h2></h2>
                        <fieldset>
                            <div class="section align-center">Have you ever been employed as a company driver <br>or independent contractor for C.R. England?
                                <div id="companyDriver" class="option-group field">
                                    <label class="option">
                                        <input type="radio" id="company_driver_y" name="company_driver" class="smartfm-ctrl" value="Y"  data-show-id="company_driver_dates">
                                        <span class="radio"></span> Yes
                                    </label>
                                    <label class="option">
                                        <input type="radio" id="company_driver_n" name="company_driver" class="smartfm-ctrl" value="N"  data-show-id="">
                                        <span class="radio"></span> No
                                    </label>
                                </div>
                                <div class="spacer-b30"></div>
                            </div>
                            <div class="section align-center hiddenbox formShowHide_reset" id="company_driver_dates">
                                <div class="frm-row">Please Enter Your Start And Ending Dates Of Employment
                                    <div class="section colm colm6">
                                        <label class="field prepend-icon">
                                            <input type="text" name="company_driver_start" id="company_driver_start" class="gui-input" placeholder="Start Date" readonly="readonly">
                                            <span class="field-icon"><i class="glyphicon glyphicon-calendar"></i></span>
                                        </label>
                                    </div>
                                    <div class="section colm colm6">
                                        <label class="field prepend-icon">
                                            <input type="text" name="company_driver_end" id="company_driver_end" class="gui-input" placeholder="End Date" readonly="readonly">
                                            <span class="field-icon"><i class="glyphicon glyphicon-calendar"></i></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="section align-center">Have you had any moving violations in the last five years?
                                <div id="movingViolations" class="option-group field">
                                    <label class="option">
                                        <input type="radio" id="moving_violations_y" name="moving_violations" class="smartfm-ctrl" value="Y"  data-show-id="moving_violations_info">
                                        <span class="radio"></span> Yes
                                    </label>
                                    <label class="option">
                                        <input type="radio" id="moving_violations_n" name="moving_violations" class="smartfm-ctrl" value="N"  data-show-id="">
                                        <span class="radio"></span> No
                                    </label>
                                </div>
                            </div>
                            <div class="section align-center hiddenbox formShowHide_reset" id="moving_violations_info">
                                <div id="clone-animate">
                                    <div class="toclone">
                                        <div class="frm-row">
                                            <div class="section colm colm6">
                                                <label class="field prepend-icon">
                                                    <input type="text" name="moving_violations_date[0]" id="vdate" class="gui-input moving_violations_date" placeholder="Citation Date" readonly="readonly" />
                                                    <span class="field-icon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                </label>
                                            </div>
                                            <div class="section colm colm6">
                                                <label class="field select">
                                                    <select name="violation_type[0]" id="vtype">
                                                        <option value="">Violation Type:</option>
                                                        <option value="Speeding Less Than 15 Over">Speeding: Less than 15 MPH over the posted speed limit</option>
                                                        <option value="Speeding Greater Than 15 Over">Speeding: Greater than 15 MPH over the posted speed limit</option>
                                                        <option value="Reckless">Reckless Driving</option>
                                                        <option value="Careless">Careless Driving</option>
                                                        <option value="Other">Other. Describe Below</option>
                                                    </select>
                                                    <i class="arrow double"></i>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="frm-row align-center">
                                            <div class="section align-center colm colm4">
                                                <label class="field prepend-icon">
                                                    <input type="text" name="violation_city[0]" id="vcity" class="gui-input" placeholder="City">
                                                    <span class="field-icon"><span class="glyphicon glyphicon-globe"></span></span>
                                                </label>
                                            </div>
                                            <div class="section colm colm4">
                                                <label class="field prepend-icon">
                                                    <input type="text" name="violation_county[0]" id="vcounty" class="gui-input" placeholder="County">
                                                    <span class="field-icon"><span class="glyphicon glyphicon-map-marker"></span></span>
                                                </label>
                                            </div>
                                            <div class="section colm colm4">
                                                <label class="field select prepend-icon">
                                                    <select name="violation_state[0]" id="vstate">
                                                        <option value="">State:</option>
                                                        <option value="AL">Alabama</option>
                                                        <option value="AK">Alaska</option>
                                                        <option value="AZ">Arizona</option>
                                                        <option value="AR">Arkansas</option>
                                                        <option value="CA">California</option>
                                                        <option value="CO">Colorado</option>
                                                        <option value="CT">Connecticut</option>
                                                        <option value="DE">Delaware</option>
                                                        <option value="DC">District Of Columbia</option>
                                                        <option value="FL">Florida</option>
                                                        <option value="GA">Georgia</option>
                                                        <option value="HI">Hawaii</option>
                                                        <option value="ID">Idaho</option>
                                                        <option value="IL">Illinois</option>
                                                        <option value="IN">Indiana</option>
                                                        <option value="IA">Iowa</option>
                                                        <option value="KS">Kansas</option>
                                                        <option value="KY">Kentucky</option>
                                                        <option value="LA">Louisiana</option>
                                                        <option value="ME">Maine</option>
                                                        <option value="MD">Maryland</option>
                                                        <option value="MA">Massachusetts</option>
                                                        <option value="MI">Michigan</option>
                                                        <option value="MN">Minnesota</option>
                                                        <option value="MS">Mississippi</option>
                                                        <option value="MO">Missouri</option>
                                                        <option value="MT">Montana</option>
                                                        <option value="NE">Nebraska</option>
                                                        <option value="NV">Nevada</option>
                                                        <option value="NH">New Hampshire</option>
                                                        <option value="NJ">New Jersey</option>
                                                        <option value="NM">New Mexico</option>
                                                        <option value="NY">New York</option>
                                                        <option value="NC">North Carolina</option>
                                                        <option value="ND">North Dakota</option>
                                                        <option value="OH">Ohio</option>
                                                        <option value="OK">Oklahoma</option>
                                                        <option value="OR">Oregon</option>
                                                        <option value="PA">Pennsylvania</option>
                                                        <option value="RI">Rhode Island</option>
                                                        <option value="SC">South Carolina</option>
                                                        <option value="SD">South Dakota</option>
                                                        <option value="TN">Tennessee</option>
                                                        <option value="TX">Texas</option>
                                                        <option value="UT">Utah</option>
                                                        <option value="VT">Vermont</option>
                                                        <option value="VA">Virginia</option>
                                                        <option value="WA">Washington</option>
                                                        <option value="WV">West Virginia</option>
                                                        <option value="WI">Wisconsin</option>
                                                        <option value="WY">Wyoming</option>
                                                    </select>
                                                    <i class="arrow double"></i>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="section colm colm12">
                                            <label class="field prepend-icon">
                                                <input type="text" class="gui-input" name="violation_details[0]" id="vdetails" placeholder="In your own words, describe the citation...">
                                                <span class="field-icon"><span class="glyphicon glyphicon-pencil"></span></span>
                                            </label>
                                        </div>
                                        <div class="section align-center">Are all fines and fees associated with this citation paid?
                                            <div class="option-group field">
                                                <label class="option">
                                                    <input type="radio" id="fines_and_fees_y" name="fines_and_fees[0]" class="smartfm-ctrl" value="Y">
                                                    <span class="radio"></span>Yes
                                                </label>
                                                <label class="option">
                                                    <input type="radio" id="fines_and_fees_n" name="fines_and_fees[0]" class="smartfm-ctrl" value="N">
                                                    <span class="radio"></span> No
                                                </label>
                                            </div>
                                        </div>
                                        <a href="#" class="clone button btn-primary"><i class="glyphicon glyphicon-plus"></i></a>
                                        <a href="#" class="delete button"><i class="glyphicon glyphicon-minus"></i></a>
                                    </div> <!-- end .toclone -->
                                </div> <!-- end #clone-group-fields -->
                            </div>
                            <div class="section align-center">Have you had any traffic accidents in the last five years?
                                <div id="carAccidents" class="option-group field">
                                    <label class="option">
                                        <input type="radio" id="accidents_y" name="accidents" class="smartfm-ctrl" value="Y"  data-show-id="accidents_info">
                                        <span class="radio"></span> Yes
                                    </label>
                                    <label class="option">
                                        <input type="radio" id="accidents_n" name="accidents" class="smartfm-ctrl" value="N"  data-show-id="">
                                        <span class="radio"></span> No
                                    </label>
                                </div>
                                <div class="spacer-b30"></div>
                            </div>
                            <div class="section align-center hiddenbox formShowHide_reset" id="accidents_info">
                                <div id="clone-animate2">
                                    <div class="toclone">
                                        <div class="frm-row">
                                            <div class="section colm colm12">
                                                <label class="field prepend-icon">
                                                    <input type="text" name="accident_date[0]" id="accdate" class="gui-input accidents_date" placeholder="Accident Date" readonly="readonly">
                                                    <span class="field-icon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="frm-row">
                                            <div class="section align-center colm colm6">Were you issued a ticket?
                                                <div class="option-group field">
                                                    <label class="option">
                                                        <input type="radio" id="accticy" name="accident_ticket[0]" class="smartfm-ctrl" value="Y">
                                                        <span class="radio"></span> Yes
                                                    </label>
                                                    <label class="option">
                                                        <input type="radio" id="accticn" name="accident_ticket[0]" class="smartfm-ctrl" value="N">
                                                        <span class="radio"></span> No
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="section align-center colm colm6">Were there any injuries?
                                                <div class="option-group field">
                                                    <label class="option">
                                                        <input type="radio" id="accidents_injuries_y" name="accident_injuries[0]" class="smartfm-ctrl" value="Y">
                                                        <span class="radio"></span> Yes
                                                    </label>
                                                    <label class="option">
                                                        <input type="radio" id="accidents_injuries_n" name="accident_injuries[0]" class="smartfm-ctrl" value="N">
                                                        <span class="radio"></span> No
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="frm-row">
                                            <div class="section align-center colm colm6">Were there any fatalities?
                                                <div class="option-group field">
                                                    <label class="option">
                                                        <input type="radio" id="accidents_fatalities_y" name="accident_fatalities[0]" class="smartfm-ctrl" value="Y">
                                                        <span class="radio"></span> Yes
                                                    </label>
                                                    <label class="option">
                                                        <input type="radio" id="accidents_fatalities_n" name="accident_fatalities[0]" class="smartfm-ctrl" value="N">
                                                        <span class="radio"></span> No
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="section align-center colm colm6">Was the accident preventable?
                                                <div class="option-group field">
                                                    <label class="option">
                                                        <input type="radio" id="accidents_preventable_y" name="accident_preventable[0]" class="smartfm-ctrl" value="Y">
                                                        <span class="radio"></span> Yes
                                                    </label>
                                                    <label class="option">
                                                        <input type="radio" id="accidents_preventable_n" name="accident_preventable[0]" class="smartfm-ctrl" value="N">
                                                        <span class="radio"></span> No
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="frm-row">
                                            <div class="section align-center colm colm12">Who was listed as being at fault in the accident?
                                                <div class="option-group field">
                                                    <label class="option">
                                                        <input type="radio" id="accidents_fault_self" name="accident_fault[0]" class="smartfm-ctrl" value="S">
                                                        <span class="radio"></span> Me
                                                    </label>
                                                    <label class="option">
                                                        <input type="radio" id="accidents_fault_other" name="accident_fault[0]" class="smartfm-ctrl" value="O">
                                                        <span class="radio"></span> The other driver
                                                    </label>
                                                    <label class="option">
                                                        <input type="radio" id="accidents_fault_both" name="accident_fault[0]" class="smartfm-ctrl" value="B">
                                                        <span class="radio"></span> Both Of Us
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="frm-row">
                                            <div class="section colm colm4">
                                                <label class="field prepend-icon">
                                                    <input type="text" name="accident_city[0]" id="acccity" class="gui-input" placeholder="City">
                                                    <span class="field-icon"><span class="glyphicon glyphicon-globe"></span></span>
                                                </label>
                                            </div>
                                            <div class="section colm colm4">
                                                <label class="field prepend-icon">
                                                    <input type="text" name="accident_county[0]" id="acccounty" class="gui-input" placeholder="County">
                                                    <span class="field-icon"><span class="glyphicon glyphicon-map-marker"></span></span>
                                                </label>
                                            </div>
                                            <div class="section colm colm4">
                                                <label class="field select prepend-icon">
                                                    <select id="accstate" name="accident_state[0]">
                                                        <option value="">State:</option>
                                                        <option value="AL">Alabama</option>
                                                        <option value="AK">Alaska</option>
                                                        <option value="AZ">Arizona</option>
                                                        <option value="AR">Arkansas</option>
                                                        <option value="CA">California</option>
                                                        <option value="CO">Colorado</option>
                                                        <option value="CT">Connecticut</option>
                                                        <option value="DE">Delaware</option>
                                                        <option value="DC">District Of Columbia</option>
                                                        <option value="FL">Florida</option>
                                                        <option value="GA">Georgia</option>
                                                        <option value="HI">Hawaii</option>
                                                        <option value="ID">Idaho</option>
                                                        <option value="IL">Illinois</option>
                                                        <option value="IN">Indiana</option>
                                                        <option value="IA">Iowa</option>
                                                        <option value="KS">Kansas</option>
                                                        <option value="KY">Kentucky</option>
                                                        <option value="LA">Louisiana</option>
                                                        <option value="ME">Maine</option>
                                                        <option value="MD">Maryland</option>
                                                        <option value="MA">Massachusetts</option>
                                                        <option value="MI">Michigan</option>
                                                        <option value="MN">Minnesota</option>
                                                        <option value="MS">Mississippi</option>
                                                        <option value="MO">Missouri</option>
                                                        <option value="MT">Montana</option>
                                                        <option value="NE">Nebraska</option>
                                                        <option value="NV">Nevada</option>
                                                        <option value="NH">New Hampshire</option>
                                                        <option value="NJ">New Jersey</option>
                                                        <option value="NM">New Mexico</option>
                                                        <option value="NY">New York</option>
                                                        <option value="NC">North Carolina</option>
                                                        <option value="ND">North Dakota</option>
                                                        <option value="OH">Ohio</option>
                                                        <option value="OK">Oklahoma</option>
                                                        <option value="OR">Oregon</option>
                                                        <option value="PA">Pennsylvania</option>
                                                        <option value="RI">Rhode Island</option>
                                                        <option value="SC">South Carolina</option>
                                                        <option value="SD">South Dakota</option>
                                                        <option value="TN">Tennessee</option>
                                                        <option value="TX">Texas</option>
                                                        <option value="UT">Utah</option>
                                                        <option value="VT">Vermont</option>
                                                        <option value="VA">Virginia</option>
                                                        <option value="WA">Washington</option>
                                                        <option value="WV">West Virginia</option>
                                                        <option value="WI">Wisconsin</option>
                                                        <option value="WY">Wyoming</option>
                                                    </select>
                                                    <i class="arrow double"></i>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="section colm colm12">
                                            <label class="field prepend-icon">
                                                <input type="text" name="accident_damage_amount[0]" id="accdamageamount" class="gui-input accdamageamount" placeholder="Total Damage Amount:">
                                                <span class="field-icon"><span class="glyphicon glyphicon-usd"></span></span>
                                            </label>
                                        </div>
                                        <div class="section colm colm12">
                                            <label class="field prepend-icon">
                                                <input type="text" class="gui-input" id="accdetails" name="accident_details[0]" placeholder="In your own words, describe the accident...">
                                                <span class="field-icon"><span class="glyphicon glyphicon-pencil"></span></span>
                                            </label>
                                        </div>
                                        <a href="#" class="clone button btn-primary"><i class="glyphicon glyphicon-plus"></i></a>
                                        <a href="#" class="delete button"><i class="glyphicon glyphicon-minus"></i></a>
                                    </div>
                                </div><!-- end #clone-group-fields2 -->
                            </div>
                            <div class="section align-center">Have you ever been cited with DUI or DWI?
                                <div id="duiDwi" class="option-group field">
                                    <label class="option">
                                        <input type="radio" id="dui_dwi_y" name="dui_dwi" class="smartfm-ctrl" value="Y"  data-show-id="dui">
                                        <span class="radio"></span> Yes
                                    </label>
                                    <label class="option">
                                        <input type="radio" id="dui_dwi_n" name="dui_dwi" class="smartfm-ctrl" value="N"  data-show-id="">
                                        <span class="radio"></span> No
                                    </label>
                                </div>
                                <div class="spacer-b30"></div>
                            </div>
                            <div class="section align-center hiddenbox formShowHide_reset" id="dui">
                                <div id="clone-animate3">
                                    <div class="toclone">
                                        <div class="frm-row">
                                            <div class="section colm colm12">
                                                <label class="field prepend-icon">
                                                    <input type="text" name="dui_dwi_date[0]" id="duidate" class="gui-input dui_dwi_date" placeholder="Date Of DUI/DWI" readonly="readonly">
                                                    <span class="field-icon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="frm-row">
                                            <div class="section colm colm4">
                                                <label class="field prepend-icon">
                                                    <input type="text" name="dui_dwi_city[0]" id="duicity" class="gui-input" placeholder="City">
                                                    <span class="field-icon"><i class="glyphicon glyphicon-globe"></i></span>
                                                </label>
                                            </div>
                                            <div class="section colm colm4">
                                                <label class="field prepend-icon">
                                                    <input type="text" name="dui_dwi_county[0]" id="duicounty" class="gui-input" placeholder="County">
                                                    <span class="field-icon"><i class="glyphicon glyphicon-map-marker"></i></span>
                                                </label>
                                            </div>
                                            <div class="section colm colm4">
                                                <label class="field select prepend-icon">
                                                    <select id="duistate" name="dui_dwi_state[0]">
                                                        <option value="">State:</option>
                                                        <option value="AL">Alabama</option>
                                                        <option value="AK">Alaska</option>
                                                        <option value="AZ">Arizona</option>
                                                        <option value="AR">Arkansas</option>
                                                        <option value="CA">California</option>
                                                        <option value="CO">Colorado</option>
                                                        <option value="CT">Connecticut</option>
                                                        <option value="DE">Delaware</option>
                                                        <option value="DC">District Of Columbia</option>
                                                        <option value="FL">Florida</option>
                                                        <option value="GA">Georgia</option>
                                                        <option value="HI">Hawaii</option>
                                                        <option value="ID">Idaho</option>
                                                        <option value="IL">Illinois</option>
                                                        <option value="IN">Indiana</option>
                                                        <option value="IA">Iowa</option>
                                                        <option value="KS">Kansas</option>
                                                        <option value="KY">Kentucky</option>
                                                        <option value="LA">Louisiana</option>
                                                        <option value="ME">Maine</option>
                                                        <option value="MD">Maryland</option>
                                                        <option value="MA">Massachusetts</option>
                                                        <option value="MI">Michigan</option>
                                                        <option value="MN">Minnesota</option>
                                                        <option value="MS">Mississippi</option>
                                                        <option value="MO">Missouri</option>
                                                        <option value="MT">Montana</option>
                                                        <option value="NE">Nebraska</option>
                                                        <option value="NV">Nevada</option>
                                                        <option value="NH">New Hampshire</option>
                                                        <option value="NJ">New Jersey</option>
                                                        <option value="NM">New Mexico</option>
                                                        <option value="NY">New York</option>
                                                        <option value="NC">North Carolina</option>
                                                        <option value="ND">North Dakota</option>
                                                        <option value="OH">Ohio</option>
                                                        <option value="OK">Oklahoma</option>
                                                        <option value="OR">Oregon</option>
                                                        <option value="PA">Pennsylvania</option>
                                                        <option value="RI">Rhode Island</option>
                                                        <option value="SC">South Carolina</option>
                                                        <option value="SD">South Dakota</option>
                                                        <option value="TN">Tennessee</option>
                                                        <option value="TX">Texas</option>
                                                        <option value="UT">Utah</option>
                                                        <option value="VT">Vermont</option>
                                                        <option value="VA">Virginia</option>
                                                        <option value="WA">Washington</option>
                                                        <option value="WV">West Virginia</option>
                                                        <option value="WI">Wisconsin</option>
                                                        <option value="WY">Wyoming</option>
                                                    </select>
                                                    <i class="arrow double"></i>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="section colm colm12">
                                            <label class="field prepend-icon">
                                                <input type="text" class="gui-input" id="duidetails" name="dui_dwi_details[0]" placeholder="In your own words, describe the DUI / DWI...">
                                                <span class="field-icon"><i class="glyphicon glyphicon-pencil"></i></span>
                                            </label>
                                        </div>
                                        <a href="#" class="clone button btn-primary"><i class="glyphicon glyphicon-plus"></i></a>
                                        <a href="#" class="delete button"><i class="glyphicon glyphicon-minus"></i></a>
                                    </div>
                                </div> <!-- end #clone-group-fields3 -->
                            </div>
                            <div class="section align-center">Have you ever had your license suspended or revoked?
                                <div id="suspendedRevoked" class="option-group field">
                                    <label class="option">
                                        <input type="radio" id="suspendedrevoked_y" name="suspendedrevoked" class="smartfm-ctrl" value="Y"  data-show-id="suspended_revoked">
                                        <span class="radio"></span> Yes
                                    </label>
                                    <label class="option">
                                        <input type="radio" id="suspendedrevoked_n" name="suspendedrevoked" class="smartfm-ctrl" value="N"  data-show-id="">
                                        <span class="radio"></span> No
                                    </label>
                                </div>
                                <div class="spacer-b30"></div>
                            </div>
                            <div class="section align-center hiddenbox" id="suspended_revoked">
                                <div id="clone-animate4"> <!--  -->
                                    <div class="toclone">
                                        <div class="frm-row">
                                            <div class="section colm colm6">
                                                <label class="field prepend-icon">
                                                    <input type="text" name="suspendedrevoked_start_date[0]" id="suspendedstartdate" class="gui-input suspendedrevoked_start_date" placeholder="Start Date" readonly="readonly">
                                                    <span class="field-icon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                </label>
                                            </div>
                                            <div class="section colm colm6">
                                                <label class="field prepend-icon">
                                                    <input type="text" name="suspendedrevoked_end_date[0]" id="suspendedenddate" class="gui-input suspendedrevoked_end_date" placeholder="End Date" readonly="readonly">
                                                    <span class="field-icon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="frm-row">
                                            <div class="section colm colm4">
                                                <label class="field prepend-icon">
                                                    <input type="text" name="suspendedrevoked_city[0]" id="suspendedcity" class="gui-input" placeholder="City">
                                                    <span class="field-icon"><i class="glyphicon glyphicon-globe"></i></span>
                                                </label>
                                            </div>
                                            <div class="section colm colm4">
                                                <label class="field prepend-icon">
                                                    <input type="text" name="suspendedrevoked_county[0]" id="suspendedcounty" class="gui-input" placeholder="County">
                                                    <span class="field-icon"><i class="glyphicon glyphicon-map-marker"></i></span>
                                                </label>
                                            </div>
                                            <div class="section colm colm4">
                                                <label class="field select prepend-icon">
                                                    <select id="suspendedstate" name="suspendedrevoked_state[0]">
                                                        <option value="">State:</option>
                                                        <option value="AL">Alabama</option>
                                                        <option value="AK">Alaska</option>
                                                        <option value="AZ">Arizona</option>
                                                        <option value="AR">Arkansas</option>
                                                        <option value="CA">California</option>
                                                        <option value="CO">Colorado</option>
                                                        <option value="CT">Connecticut</option>
                                                        <option value="DE">Delaware</option>
                                                        <option value="DC">District Of Columbia</option>
                                                        <option value="FL">Florida</option>
                                                        <option value="GA">Georgia</option>
                                                        <option value="HI">Hawaii</option>
                                                        <option value="ID">Idaho</option>
                                                        <option value="IL">Illinois</option>
                                                        <option value="IN">Indiana</option>
                                                        <option value="IA">Iowa</option>
                                                        <option value="KS">Kansas</option>
                                                        <option value="KY">Kentucky</option>
                                                        <option value="LA">Louisiana</option>
                                                        <option value="ME">Maine</option>
                                                        <option value="MD">Maryland</option>
                                                        <option value="MA">Massachusetts</option>
                                                        <option value="MI">Michigan</option>
                                                        <option value="MN">Minnesota</option>
                                                        <option value="MS">Mississippi</option>
                                                        <option value="MO">Missouri</option>
                                                        <option value="MT">Montana</option>
                                                        <option value="NE">Nebraska</option>
                                                        <option value="NV">Nevada</option>
                                                        <option value="NH">New Hampshire</option>
                                                        <option value="NJ">New Jersey</option>
                                                        <option value="NM">New Mexico</option>
                                                        <option value="NY">New York</option>
                                                        <option value="NC">North Carolina</option>
                                                        <option value="ND">North Dakota</option>
                                                        <option value="OH">Ohio</option>
                                                        <option value="OK">Oklahoma</option>
                                                        <option value="OR">Oregon</option>
                                                        <option value="PA">Pennsylvania</option>
                                                        <option value="RI">Rhode Island</option>
                                                        <option value="SC">South Carolina</option>
                                                        <option value="SD">South Dakota</option>
                                                        <option value="TN">Tennessee</option>
                                                        <option value="TX">Texas</option>
                                                        <option value="UT">Utah</option>
                                                        <option value="VT">Vermont</option>
                                                        <option value="VA">Virginia</option>
                                                        <option value="WA">Washington</option>
                                                        <option value="WV">West Virginia</option>
                                                        <option value="WI">Wisconsin</option>
                                                        <option value="WY">Wyoming</option>
                                                    </select>
                                                    <i class="arrow double"></i>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="frm-row">
                                            <div class="section align-center colm colm12">Has Your License Been Reinstated?
                                                <div class="option-group field">
                                                    <label class="option">
                                                        <input type="radio" id="license_reinstated_y" name="license_reinstated[0]" class="smartfm-ctrl" value="Y">
                                                        <span class="radio"></span> Yes
                                                    </label>
                                                    <label class="option">
                                                        <input type="radio" id="license_reinstated_n" name="license_reinstated[0]" class="smartfm-ctrl" value="N">
                                                        <span class="radio"></span> No
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="#" class="clone button btn-primary">
                                            <i class="glyphicon glyphicon-plus"></i>
                                        </a>
                                        <a href="#" class="delete button">
                                            <i class="glyphicon glyphicon-minus"></i>
                                        </a>
                                    </div>
                                </div> <!-- end #clone-group-fields4 -->
                            </div>
                        </fieldset>


                        <h2></h2>
                        <fieldset>
                            <!--
                                                    <div class="frm-row">
                                                        <div class="section colm colm12 align-center">Have you used illegal drugs or prescription marijuana in the past 12 months?
                                                            <br>
                                                            <a href = "javascript:void(0)" style="color: #999; text-decoration:underline;" onclick = "document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block'">California Applicants Click Here</a>

                                                            <div id="illegal_drugs" class="option-group field">
                                                                <label class="option">
                                                                    <input type="radio" id="illegal_drugs_y" name="illegal_drugs" class="smartfm-ctrl" value="Y"  data-show-id="illegal_drugs1">
                                                                    <span class="radio"></span> Yes
                                                                </label>
                                                                <label class="option">
                                                                    <input type="radio" id="illegal_drugs_n" name="illegal_drugs" class="smartfm-ctrl" value="N"  data-show-id="">
                                                                    <span class="radio"></span> No
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="light2" class="white_content">CALIFORNIA APPLICANTS ONLY: Applicant may omit any convictions for the possession of marijuana (except for convictions for the possession of marijuana on school grounds or possession of concentrated cannabis) that are more than two (2) years old, and any information concerning a referral to, and participation in, any pre-trial or post-trial diversion program for such convictions.
                                                        <a href = "javascript:void(0)" onclick = "document.getElementById('light2').style.display='none';document.getElementById('fade').style.display='none'">Close</a>
                                                    </div>
                                                    <div class="section align-center hiddenbox" id="illegal_drugs1">
                                                        <div class="frm-row">
                                                            <div class="section colm colm12">
                                                                <label class="field prepend-icon">
                                                                    <input type="text" name="illegal_drugs_date" id="illegal_drugs_date" class="gui-input" placeholder="When?" readonly="readonly">
                                                                    <span class="field-icon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                            -->
                            <div class="section align-center">Have you ever failed or refused a pre-employment drug screen?
                                <div id="drug_test" class="option-group field">
                                    <label class="option">
                                        <input type="radio" id="drugtest_y" name="drugtest" class="smartfm-ctrl" value="Y"  data-show-id="pre_emp_drugtest">
                                        <span class="radio"></span> Yes
                                    </label>
                                    <label class="option">
                                        <input type="radio" id="drugtest_n" name="drugtest" class="smartfm-ctrl" value="N"  data-show-id="">
                                        <span class="radio"></span> No
                                    </label>
                                </div>
                                <div class="spacer-b30"></div>
                            </div>
                            <div class="section align-center hiddenbox" id="pre_emp_drugtest">
                                <div class="frm-row">
                                    <div class="section colm colm6">
                                        <label class="field prepend-icon">
                                            <input type="text" name="drugtest_date" id="drugtest_date" class="gui-input" placeholder="When?" readonly="readonly">
                                            <span class="field-icon"><i class="glyphicon glyphicon-calendar"></i></span>
                                        </label>
                                    </div>
                                    <div class="section colm colm6">
                                        <label class="field prepend-icon">
                                            <input type="text" class="gui-input" id="drugtest_details" name="drugtest_details" placeholder="Explain. (optional)">
                                            <span class="field-icon"><i class="glyphicon glyphicon-pencil"></i></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="section align-center">Have you ever been charged with a felony?
                                <div id="felony_charged" class="option-group field">
                                    <label class="option">
                                        <input type="radio" id="felony_y" name="felony" class="smartfm-ctrl" value="Y"  data-show-id="felony">
                                        <span class="radio"></span> Yes
                                    </label>
                                    <label class="option">
                                        <input type="radio" id="felony_n" name="felony" class="smartfm-ctrl" value="N"  data-show-id="">
                                        <span class="radio"></span> No
                                    </label>
                                </div>
                                <div class="spacer-b30"></div>
                            </div>
                            <div class="section align-center hiddenbox" id="felony">
                                <div id="clone-animate5">
                                    <div class="toclone" id="felonyclone">
                                        <div class="section colm colm12">
                                            <label class="field prepend-icon">
                                                <input type="text" name="felony_date[0]" id="feldate" class="gui-input felony_date" placeholder="Date Charged" readonly="readonly">
                                                <span class="field-icon"><i class="glyphicon glyphicon-calendar"></i></span>
                                            </label>
                                        </div>
                                        <div class="frm-row">
                                            <div class="section colm colm6">Were You Convicted?
                                                <div class="option-group field">
                                                    <label class="option">
                                                        <input type="radio" id="felconvicted_y" name="felony_convicted[0]" class="smartfm-ctrl" value="Y">
                                                        <span class="radio"></span> Yes
                                                    </label>
                                                    <label class="option">
                                                        <input type="radio" id="felconvicted_n" name="felony_convicted[0]" class="smartfm-ctrl" value="N">
                                                        <span class="radio"></span> No
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="section colm colm6">Were You Incarcerated?
                                                <div class="option-group field">
                                                    <label class="option">
                                                        <input type="radio" id="felincarcerated_y" name="felony_incarcerated[0]" class="smartfm-ctrl" value="Y" >
                                                        <span class="radio"></span> Yes
                                                    </label>
                                                    <label class="option">
                                                        <input type="radio" id="felincarcerated_n" name="felony_incarcerated[0]" class="smartfm-ctrl" value="N" >
                                                        <span class="radio"></span> No
                                                    </label>
                                                </div>


                                                <!--

     -->
                                                <!--
                                                                                            <script>
                                                                                                function expandField(showid){
                                                                                                        $(showid).show();
                                                                                                    };
                                                                                            </script>
                                                -->
                                            </div>
                                        </div>
                                        <!-- 								                	<div class="section align-center hiddenbox" id="incarceration_date">
                                                                            <div class="frm-row">
                                                                                <div class="section colm colm12">
                                                                                    <label class="field prepend-icon">
                                                                                        <input type="text" name="incarceration_release_date[0]" id="incarreldate" class="gui-input incarceration_release_date" placeholder="Date Released From Incarceration" readonly="readonly">
                                                                                        <span class="field-icon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                            </div> -->
                                        <div class="frm-row">
                                            <div class="section colm colm4">
                                                <label class="field prepend-icon">
                                                    <input type="text" name="felony_city[0]" id="felcity" class="gui-input" placeholder="City">
                                                    <span class="field-icon"><i class="glyphicon glyphicon-globe"></i></span>
                                                </label>
                                            </div>
                                            <div class="section colm colm4">
                                                <label class="field prepend-icon">
                                                    <input type="text" name="felony_county[0]" id="felcounty" class="gui-input" placeholder="County">
                                                    <span class="field-icon"><i class="glyphicon glyphicon-map-marker"></i></span>
                                                </label>
                                            </div>
                                            <div class="section colm colm4">
                                                <label class="field select prepend-icon">
                                                    <select id="felstate" name="felony_state[0]">
                                                        <option value="">State:</option>
                                                        <option value="AL">Alabama</option>
                                                        <option value="AK">Alaska</option>
                                                        <option value="AZ">Arizona</option>
                                                        <option value="AR">Arkansas</option>
                                                        <option value="CA">California</option>
                                                        <option value="CO">Colorado</option>
                                                        <option value="CT">Connecticut</option>
                                                        <option value="DE">Delaware</option>
                                                        <option value="DC">District Of Columbia</option>
                                                        <option value="FL">Florida</option>
                                                        <option value="GA">Georgia</option>
                                                        <option value="HI">Hawaii</option>
                                                        <option value="ID">Idaho</option>
                                                        <option value="IL">Illinois</option>
                                                        <option value="IN">Indiana</option>
                                                        <option value="IA">Iowa</option>
                                                        <option value="KS">Kansas</option>
                                                        <option value="KY">Kentucky</option>
                                                        <option value="LA">Louisiana</option>
                                                        <option value="ME">Maine</option>
                                                        <option value="MD">Maryland</option>
                                                        <option value="MA">Massachusetts</option>
                                                        <option value="MI">Michigan</option>
                                                        <option value="MN">Minnesota</option>
                                                        <option value="MS">Mississippi</option>
                                                        <option value="MO">Missouri</option>
                                                        <option value="MT">Montana</option>
                                                        <option value="NE">Nebraska</option>
                                                        <option value="NV">Nevada</option>
                                                        <option value="NH">New Hampshire</option>
                                                        <option value="NJ">New Jersey</option>
                                                        <option value="NM">New Mexico</option>
                                                        <option value="NY">New York</option>
                                                        <option value="NC">North Carolina</option>
                                                        <option value="ND">North Dakota</option>
                                                        <option value="OH">Ohio</option>
                                                        <option value="OK">Oklahoma</option>
                                                        <option value="OR">Oregon</option>
                                                        <option value="PA">Pennsylvania</option>
                                                        <option value="RI">Rhode Island</option>
                                                        <option value="SC">South Carolina</option>
                                                        <option value="SD">South Dakota</option>
                                                        <option value="TN">Tennessee</option>
                                                        <option value="TX">Texas</option>
                                                        <option value="UT">Utah</option>
                                                        <option value="VT">Vermont</option>
                                                        <option value="VA">Virginia</option>
                                                        <option value="WA">Washington</option>
                                                        <option value="WV">West Virginia</option>
                                                        <option value="WI">Wisconsin</option>
                                                        <option value="WY">Wyoming</option>
                                                    </select>
                                                    <i class="arrow double"></i>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="section colm colm12">
                                            <label class="field prepend-icon">
                                                <input type="text" class="gui-input" id="feldetails" name="felony_details[0]" placeholder="In your own words, describe the felony...">
                                                <span class="field-icon"><i class="glyphicon glyphicon-pencil"></i></span>
                                            </label>
                                        </div>
                                        <a href="#" class="clone button btn-primary"><i class="glyphicon glyphicon-plus"></i></a>
                                        <a href="#" class="delete button"><i class="glyphicon glyphicon-minus"></i></a>
                                    </div>
                                </div> <!-- end #clone-group-fields5 -->
                            </div>
                            <div class="section align-center">Have you ever been charged with a misdemeanor?
                                <div id="misdemeanor_charged" class="option-group field">
                                    <label class="option">
                                        <input type="radio" id="misdemeanor_y" name="misdemeanor" class="smartfm-ctrl" value="Y"  data-show-id="misdemeanor">
                                        <span class="radio"></span> Yes
                                    </label>
                                    <label class="option">
                                        <input type="radio" id="misdemeanor_n" name="misdemeanor" class="smartfm-ctrl" value="N"  data-show-id="">
                                        <span class="radio"></span> No
                                    </label>
                                </div>
                                <div class="spacer-b30"></div>
                            </div>
                            <div class="section align-center hiddenbox" id="misdemeanor">
                                <div id="clone-animate6">
                                    <div class="toclone">
                                        <!-- 													<div class="frm-row"> -->
                                        <div class="section colm colm12">
                                            <label class="field prepend-icon">
                                                <input type="text" name="misdemeanor_date[0]" id="misdate" class="gui-input misdemeanor_date" placeholder="Date Charged" readonly="readonly">
                                                <span class="field-icon"><i class="glyphicon glyphicon-calendar"></i></span>
                                            </label>
                                        </div>
                                        <!-- 							                    	</div> -->
                                        <!-- 							                    	<div class="frm-row"> -->
                                        <div class="section colm colm12">Were You Convicted?
                                            <div class="option-group field">
                                                <label class="option">
                                                    <input type="radio" id="misconvicted_y" name="misdemeanor_convicted[0]" class="smartfm-ctrl" value="Y">
                                                    <span class="radio"></span> Yes
                                                </label>
                                                <label class="option">
                                                    <input type="radio" id="misconvicted_n" name="misdemeanor_convicted[0]" class="smartfm-ctrl" value="N">
                                                    <span class="radio"></span> No
                                                </label>
                                            </div>
                                        </div>
                                        <!-- 							                		</div> -->
                                        <div class="frm-row">
                                            <div class="section colm colm4">
                                                <label class="field prepend-icon">
                                                    <input type="text" name="misdemeanor_city[0]" id="miscity" class="gui-input" placeholder="City">
                                                    <span class="field-icon"><i class="glyphicon glyphicon-globe"></i></span>
                                                </label>
                                            </div>
                                            <div class="section colm colm4">
                                                <label class="field prepend-icon">
                                                    <input type="text" name="misdemeanor_county[0]" id="miscounty" class="gui-input" placeholder="County">
                                                    <span class="field-icon"><i class="glyphicon glyphicon-map-marker"></i></span>
                                                </label>
                                            </div>
                                            <div class="section colm colm4">
                                                <label class="field select prepend-icon">
                                                    <select id="misstate" name="misdemeanor_state[0]">
                                                        <option value="">State:</option>
                                                        <option value="AL">Alabama</option>
                                                        <option value="AK">Alaska</option>
                                                        <option value="AZ">Arizona</option>
                                                        <option value="AR">Arkansas</option>
                                                        <option value="CA">California</option>
                                                        <option value="CO">Colorado</option>
                                                        <option value="CT">Connecticut</option>
                                                        <option value="DE">Delaware</option>
                                                        <option value="DC">District Of Columbia</option>
                                                        <option value="FL">Florida</option>
                                                        <option value="GA">Georgia</option>
                                                        <option value="HI">Hawaii</option>
                                                        <option value="ID">Idaho</option>
                                                        <option value="IL">Illinois</option>
                                                        <option value="IN">Indiana</option>
                                                        <option value="IA">Iowa</option>
                                                        <option value="KS">Kansas</option>
                                                        <option value="KY">Kentucky</option>
                                                        <option value="LA">Louisiana</option>
                                                        <option value="ME">Maine</option>
                                                        <option value="MD">Maryland</option>
                                                        <option value="MA">Massachusetts</option>
                                                        <option value="MI">Michigan</option>
                                                        <option value="MN">Minnesota</option>
                                                        <option value="MS">Mississippi</option>
                                                        <option value="MO">Missouri</option>
                                                        <option value="MT">Montana</option>
                                                        <option value="NE">Nebraska</option>
                                                        <option value="NV">Nevada</option>
                                                        <option value="NH">New Hampshire</option>
                                                        <option value="NJ">New Jersey</option>
                                                        <option value="NM">New Mexico</option>
                                                        <option value="NY">New York</option>
                                                        <option value="NC">North Carolina</option>
                                                        <option value="ND">North Dakota</option>
                                                        <option value="OH">Ohio</option>
                                                        <option value="OK">Oklahoma</option>
                                                        <option value="OR">Oregon</option>
                                                        <option value="PA">Pennsylvania</option>
                                                        <option value="RI">Rhode Island</option>
                                                        <option value="SC">South Carolina</option>
                                                        <option value="SD">South Dakota</option>
                                                        <option value="TN">Tennessee</option>
                                                        <option value="TX">Texas</option>
                                                        <option value="UT">Utah</option>
                                                        <option value="VT">Vermont</option>
                                                        <option value="VA">Virginia</option>
                                                        <option value="WA">Washington</option>
                                                        <option value="WV">West Virginia</option>
                                                        <option value="WI">Wisconsin</option>
                                                        <option value="WY">Wyoming</option>
                                                    </select>
                                                    <i class="arrow double"></i>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="section colm colm12">
                                            <label class="field prepend-icon">
                                                <input type="text" class="gui-input" id="misdetails" name="misdemeanor_details[0]" placeholder="In your own words, describe the misdemeanor...">
                                                <span class="field-icon"><i class="glyphicon glyphicon-pencil"></i></span>
                                            </label>
                                        </div>
                                        <a href="#" class="clone button btn-primary"><i class="glyphicon glyphicon-plus"></i></a>
                                        <a href="#" class="delete button"><i class="glyphicon glyphicon-minus"></i></a>
                                    </div>
                                </div> <!-- end #clone-group-fields6 -->
                            </div>
                            <div class="section align-center">Have you been to any other truck driving school in the last five years?
                                <div id="school_grad" class="option-group field">
                                    <label class="option">
                                        <input type="radio" id="school_y" name="schoolgrad" class="smartfm-ctrl" value="Y"  data-show-id="other_school1">
                                        <span class="radio"></span> Yes
                                    </label>
                                    <label class="option">
                                        <input type="radio" id="school_n" name="schoolgrad" class="smartfm-ctrl" value="N"  data-show-id="">
                                        <span class="radio"></span> No
                                    </label>
                                </div>
                                <div class="spacer-b30"></div>
                            </div>
                            <div class="section align-center hiddenbox" id="other_school1">
                                <div class="frm-row">
                                    <div class="section colm colm6">
                                        <label class="field prepend-icon">
                                            <input type="text" name="school_grad_name" id="school_grad_name" class="gui-input" placeholder="School Name">
                                            <span class="field-icon"><i class="glyphicon glyphicon-book"></i></span>
                                        </label>
                                    </div>
                                    <div class="section colm colm6">
                                        <label class="field prepend-icon">
                                            <input type="text" name="school_grad_date" id="school_grad_date" class="gui-input" placeholder="Graduation Date" readonly="readonly">
                                            <span class="field-icon"><i class="glyphicon glyphicon-calendar"></i></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="frm-row">
                                    <div class="section colm colm4">
                                        <label class="field prepend-icon">
                                            <input type="text" name="school_grad_city" id="school_grad_city" class="gui-input" placeholder="City">
                                            <span class="field-icon"><i class="glyphicon glyphicon-globe"></i></span>
                                        </label>
                                    </div>
                                    <div class="section colm colm4">
                                        <label for="school_grad_state" class="field select prepend-icon">
                                            <select id="school_grad_state" name="school_grad_state">
                                                <option value="">State:</option>
                                                <option value="AL">Alabama</option>
                                                <option value="AK">Alaska</option>
                                                <option value="AZ">Arizona</option>
                                                <option value="AR">Arkansas</option>
                                                <option value="CA">California</option>
                                                <option value="CO">Colorado</option>
                                                <option value="CT">Connecticut</option>
                                                <option value="DE">Delaware</option>
                                                <option value="DC">District Of Columbia</option>
                                                <option value="FL">Florida</option>
                                                <option value="GA">Georgia</option>
                                                <option value="HI">Hawaii</option>
                                                <option value="ID">Idaho</option>
                                                <option value="IL">Illinois</option>
                                                <option value="IN">Indiana</option>
                                                <option value="IA">Iowa</option>
                                                <option value="KS">Kansas</option>
                                                <option value="KY">Kentucky</option>
                                                <option value="LA">Louisiana</option>
                                                <option value="ME">Maine</option>
                                                <option value="MD">Maryland</option>
                                                <option value="MA">Massachusetts</option>
                                                <option value="MI">Michigan</option>
                                                <option value="MN">Minnesota</option>
                                                <option value="MS">Mississippi</option>
                                                <option value="MO">Missouri</option>
                                                <option value="MT">Montana</option>
                                                <option value="NE">Nebraska</option>
                                                <option value="NV">Nevada</option>
                                                <option value="NH">New Hampshire</option>
                                                <option value="NJ">New Jersey</option>
                                                <option value="NM">New Mexico</option>
                                                <option value="NY">New York</option>
                                                <option value="NC">North Carolina</option>
                                                <option value="ND">North Dakota</option>
                                                <option value="OH">Ohio</option>
                                                <option value="OK">Oklahoma</option>
                                                <option value="OR">Oregon</option>
                                                <option value="PA">Pennsylvania</option>
                                                <option value="RI">Rhode Island</option>
                                                <option value="SC">South Carolina</option>
                                                <option value="SD">South Dakota</option>
                                                <option value="TN">Tennessee</option>
                                                <option value="TX">Texas</option>
                                                <option value="UT">Utah</option>
                                                <option value="VT">Vermont</option>
                                                <option value="VA">Virginia</option>
                                                <option value="WA">Washington</option>
                                                <option value="WV">West Virginia</option>
                                                <option value="WI">Wisconsin</option>
                                                <option value="WY">Wyoming</option>
                                            </select>
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                    <div class="section colm colm4">
                                        <label class="field prepend-icon">
                                            <input type="text" name="school_grad_zip" id="school_grad_zip" class="gui-input" placeholder="Zip">
                                            <span class="field-icon"><i class="glyphicon glyphicon-map-marker"></i></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>


                        <h2></h2>
                        <fieldset>
                            <div class="section align-center"> Please provide the last three years of work history.
                                <div class="section align-center hiddenbox" id="work_history">
                                    <div id="clone-animate7">
                                        <div class="toclone">
                                            <!--
                                                                                <div class="inner-wrap">
                                                                                    <div class="clone-inner">
                                            -->
                                            <div class="frm-row">
                                                <div class="section colm colm6">
                                                    <label class="field prepend-icon">
                                                        <input type="text" name="work_history_company[0]" id="workhiscompany" class="gui-input" placeholder="Company Name">
                                                        <span class="field-icon"><i class="glyphicon glyphicon-briefcase"></i></span>
                                                    </label>
                                                </div>
                                                <div class="section colm colm6">
                                                    <label class="field prepend-icon">
                                                        <input type="text" name="work_history_title[0]" id="workhistitle" class="gui-input" placeholder="Job Title">
                                                        <span class="field-icon"><i class="glyphicon glyphicon-bookmark"></i></span>
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="frm-row">
                                                <div class="section colm colm12">
                                                    <label class="field prepend-icon">
                                                        <input type="text" name="work_history_start[0]" id="workhisstart" class="gui-input work_history_start" placeholder="Start Date" readonly="readonly">
                                                        <span class="field-icon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="frm-row">
                                                <div class="section colm colm6">
                                                    <label class="field prepend-icon">
                                                        <input type="text" name="work_history_end[0]" id="workhisend" class="gui-input work_history_end" placeholder="End Date" readonly="readonly">
                                                        <span class="field-icon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                    </label>
                                                </div>

                                                <!--
<div class="section colm colm6">Is This Your Current Employer?
                                                    <div class="option-group field">
                                                        <label class="option">
                                                            <input type="radio" id="workhiscurrent_y" name="work_history_current[0]" class="smartfm-ctrl" value="Y" data-show-id="">
                                                            <span class="radio"></span> Yes
                                                        </label>
                                                        <label class="option">
                                                            <input type="radio" id="workhiscurrent_n" name="work_history_current[0]" class="smartfm-ctrl" value="N" data-show-id="noncurrent_work_history">
                                                            <span class="radio"></span> No
                                                        </label>
                                                    </div>
                                                </div>
-->
                                                <!-- </div> -->

                                                <!-- <div class="frm-row"> -->
                                                <div class="section colm colm6">
                                                    <div class="option-group field">

                                                        <label class="option block">
                                                            <input type="checkbox" name="work_history_current[0]" id="workhiscurrent_y" value="Y">
                                                            <span class="checkbox"></span>
                                                            <span class="small-text fine-grey align-center">Current Employer.</span>
                                                        </label>

                                                        <!--
<label class="option">
                                                            <input type="radio" id="workhiscurrent_y" name="work_history_current[0]" class="smartfm-ctrl" value="Y" data-show-id="">
                                                            <span class="radio"></span> Yes
                                                        </label>
                                                        <label class="option">
                                                            <input type="radio" id="workhiscurrent_n" name="work_history_current[0]" class="smartfm-ctrl" value="N" data-show-id="noncurrent_work_history">
                                                            <span class="radio"></span> No
                                                        </label>
-->
                                                    </div>
                                                </div>
                                            </div>




                                            <div class="frm-row">
                                                <div class="section colm colm12">
                                                    <label class="field prepend-icon">
                                                        <input type="text" name="work_history_address[0]" id="workhisaddress" class="gui-input" placeholder="Address">
                                                        <span class="field-icon"><i class="glyphicon glyphicon-road"></i></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="frm-row">
                                                <div class="section colm colm4">
                                                    <label class="field prepend-icon">
                                                        <input type="text" name="work_history_city[0]" id="workhiscity" class="gui-input" placeholder="City">
                                                        <span class="field-icon"><i class="glyphicon glyphicon-globe"></i></span>
                                                    </label>
                                                </div>
                                                <div class="section colm colm4">
                                                    <label class="field select prepend-icon">
                                                        <select id="workhisstate" name="work_history_state[0]">
                                                            <option value="">State:</option>
                                                            <option value="AL">Alabama</option>
                                                            <option value="AK">Alaska</option>
                                                            <option value="AZ">Arizona</option>
                                                            <option value="AR">Arkansas</option>
                                                            <option value="CA">California</option>
                                                            <option value="CO">Colorado</option>
                                                            <option value="CT">Connecticut</option>
                                                            <option value="DE">Delaware</option>
                                                            <option value="DC">District Of Columbia</option>
                                                            <option value="FL">Florida</option>
                                                            <option value="GA">Georgia</option>
                                                            <option value="HI">Hawaii</option>
                                                            <option value="ID">Idaho</option>
                                                            <option value="IL">Illinois</option>
                                                            <option value="IN">Indiana</option>
                                                            <option value="IA">Iowa</option>
                                                            <option value="KS">Kansas</option>
                                                            <option value="KY">Kentucky</option>
                                                            <option value="LA">Louisiana</option>
                                                            <option value="ME">Maine</option>
                                                            <option value="MD">Maryland</option>
                                                            <option value="MA">Massachusetts</option>
                                                            <option value="MI">Michigan</option>
                                                            <option value="MN">Minnesota</option>
                                                            <option value="MS">Mississippi</option>
                                                            <option value="MO">Missouri</option>
                                                            <option value="MT">Montana</option>
                                                            <option value="NE">Nebraska</option>
                                                            <option value="NV">Nevada</option>
                                                            <option value="NH">New Hampshire</option>
                                                            <option value="NJ">New Jersey</option>
                                                            <option value="NM">New Mexico</option>
                                                            <option value="NY">New York</option>
                                                            <option value="NC">North Carolina</option>
                                                            <option value="ND">North Dakota</option>
                                                            <option value="OH">Ohio</option>
                                                            <option value="OK">Oklahoma</option>
                                                            <option value="OR">Oregon</option>
                                                            <option value="PA">Pennsylvania</option>
                                                            <option value="RI">Rhode Island</option>
                                                            <option value="SC">South Carolina</option>
                                                            <option value="SD">South Dakota</option>
                                                            <option value="TN">Tennessee</option>
                                                            <option value="TX">Texas</option>
                                                            <option value="UT">Utah</option>
                                                            <option value="VT">Vermont</option>
                                                            <option value="VA">Virginia</option>
                                                            <option value="WA">Washington</option>
                                                            <option value="WV">West Virginia</option>
                                                            <option value="WI">Wisconsin</option>
                                                            <option value="WY">Wyoming</option>
                                                        </select>
                                                        <i class="arrow double"></i>
                                                    </label>
                                                </div>
                                                <div class="section colm colm4">
                                                    <label class="field prepend-icon">
                                                        <input type="text" name="work_history_zip[0]" id="workhiszip" class="gui-input" placeholder="Zip">
                                                        <span class="field-icon"><i class="glyphicon glyphicon-map-marker"></i></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="frm-row">
                                                <div class="section colm colm12">
                                                    <label class="field prepend-icon">
                                                        <input type="text" name="work_history_phone[0]" id="workhisphone" class="gui-input" placeholder="Phone Number">
                                                        <span class="field-icon"><i class="glyphicon glyphicon-phone-alt"></i></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="frm-row hiddenbox" id="leave_reason">
                                                <div class="section colm colm12">
                                                    <label class="field prepend-icon">
                                                        <input type="text" name="work_history_reason[0]" id="workhisreason" class="gui-input" placeholder="Reason For Leaving?">
                                                        <span class="field-icon"><i class="glyphicon glyphicon-plane"></i></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="frm-row">

                                                <div class="section colm colm12">May We Contact This Employer?
                                                    <div class="option-group field">
                                                        <label class="option">
                                                            <input type="radio" id="workhiscontact_y" name="work_history_contact[0]" class="smartfm-ctrl" value="Y">
                                                            <span class="radio"></span> Yes
                                                        </label>
                                                        <label class="option">
                                                            <input type="radio" id="workhiscontact_n" name="work_history_contact[0]" class="smartfm-ctrl" value="N">
                                                            <span class="radio"></span> No
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--
                                                                                    </div>
                                                                                </div>
                                            -->
                                            <a href="#" class="clone button btn-primary"><i class="glyphicon glyphicon-plus"></i></a>
                                            <a href="#" class="delete button"><i class="glyphicon glyphicon-minus"></i></a>
                                        </div>
                                    </div> <!-- end #clone-group-fields7 -->
                                </div>
                            </div>
                        </fieldset>



                        <h2 id="disclaimer"> Disclaimer </h2>
                        <fieldset  id="last">
                            <div id="EVERIFY">
                                <div class="section">
                                    <label class="field prepend-icon">
                                    <span class="input-hint small-text fine-grey align-center">
                                        <strong>DISCLOSURE:</strong> By submitting this application I certify that I personally have completed this application and that all of the information is true and correct. I hereby authorize C.R. England, Inc. and its agents or contractors that receive this application to cause to be conducted, at any time, an investigation of my background for employment purposes, which may include, but is not limited to, any information relating to my character, general reputation, personal characteristics, mode of living, criminal history, driving record, credit history, past work experience, educational background, alcohol or drug test results, or failure to submit to an alcohol or drug test, or any other information about me which may reflect upon my potential for employment gathered from any individual, organization, entity, agency, or other source which may have knowledge concerning any such items of information. If hired or contracted this authorization for the above reports shall remain on file and shall serve as ongoing authorization for C.R. England, Inc to procure consumer reports at any time during my employment or contract period. I have completed this application of my own free will and hold C.R. England, Inc., its agents and contractors harmless for all liability for providing this application for my use. I consent to receiving text (SMS) messages from CR England and/or its affiliates regarding or relating to my application for employment. I have read and understand the  job description  and certify that I am able to meet the requirements of this position and may be contacted by, which may include, but is not limited to, email contact, phone contact, direct mail contact.
											<br>
											<br>
										<strong>EMPLOYMENT VERIFICATION:</strong> I hereby authorize C.R. England, Inc to do a complete background investigation, which includes contacting my past employers regarding my service, character and drug and alcohol test results as per Federal Motor Vehicle § 391.23 and 382.413. My past employers are released from any and all liability, which may result from furnishing such information. I authorize C.R. England to run a consumer report from USIS (DAC) services. These reports may include information concerning driving record, work experience, and motor vehicle reports.
                                    </span>
                                    </label>
                                </div>
                                <div class="align-center">
                                    <div class="frm-row">
                                        <div class="option-group field">
                                            <div class="section colm colm12">
                                                <label class="option block">
                                                    <input type="checkbox" name="Disclosure_and_Everify_terms" id="Disclosure_and_Everify_terms" value="Y">
                                                    <span class="checkbox"></span>
                                                    <span class="small-text fine-grey align-center">I have read and agree to the Disclosure and the Employment Verification release above.</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="spacer-b20"></div>
                            </div>
                            <div id="FMCSA">
                                <div class="section">
                                    <label class="field prepend-icon">
                                    <span class="input-hint small-text fine-grey align-center">
                                        <strong>DRIVER RIGHTS NOTIFICATION
                                        	<br>FMCSA NOTIFICATION OF DRIVER RIGHTS
                                        </strong> In compliance with 49 CFR Part 40 § 391.23 you have certain	rights regarding the performance history information that will be provided to prospective employers. I) You have the right to review	information provided by previous employers. II) You have the right to	have errors in the information corrected by the previous employer and	for that previous employer to re-send the corrected information to	prospective employers. III) You have the right to have a rebuttal	statement attached to the alleged erroneous information, if the	previous employer and the driver cannot agree on the accuracy of the	information. Drivers who have previous DOT regulated employment	history in the preceding three years and wish to review previous	employer-provided investigative information must submit a written	request to prospective employers. This may be done at any time,	including when applying, or as late as 30 days after being employed or	being notified of denial of employment. Prospective employers must	provide this information within five business days of receiving the	written request. If prospective employers have not yet received the requested information from the previous employer, then the five day	deadline will begin when the requested safety performance history	information is received. If you have not arranged to pick up or	receive the requested records within 30 days of prospective employers	making them available. Prospective employers may consider you to have	waived your request to review the record.
                                    </span>
                                    </label>
                                </div>
                                <div class="align-center">
                                    <div class="frm-row">
                                        <div class="option-group field">
                                            <div class="section colm colm12">
                                                <label class="option block">
                                                    <input type="checkbox" name="FMCSA_terms" id="FMCSA_terms" value="Y">
                                                    <span class="checkbox"></span>
                                                    <span class="small-text fine-grey align-center">I have read and agree to the FMCSA Notification of Driver Rights.</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="PSP">
                                <div class="section">
                                    <label class="field prepend-icon">
                                    <span class="input-hint small-text fine-grey align-center">
                                        <strong>MANDATORY USE FOR ALL ACCOUNT HOLDERS - IMPORTANT NOTICE - REGARDING BACKGROUND REPORTS FROM THE PSP ONLINE SERVICE</strong> 1. In connection with your application for employment with __C.R. England__ ("Prospective Employer"), Prospective Employer, its employees, agents or contractors may obtain one or more reports regarding your driving, and safety inspection history from the Federal Motor Carrier Safety Administration (FMCSA).
When the application for employment is submitted in person, if the Prospective Employer uses any information it obtains from FMCSA in a decision to not hire you or to make any other adverse employment decision regarding you, the Prospective Employer will provide you with a copy of the report upon which its decision was based and a written summary of your rights under the Fair Credit Reporting Act before taking any final adverse action. If any final adverse action is taken against you based upon your driving history or safety report, the Prospective Employer will notify you that the action has been taken and that the action was based in part or in whole on this report.
When the application for employment is submitted by mail, telephone, computer, or other similar means, if the Prospective Employer uses any information it obtains from FMCSA in a decision to not hire you or to make any other adverse employment decision regarding you, the Prospective Employer must provide you within three business days of taking adverse action oral, written or electronic notification: that adverse action has been taken based in whole or in part on information obtained from FMCSA; the name, address, and the toll free telephone number of FMCSA; that the FMCSA did not make the decision to take the adverse action and is unable to provide you the specific reasons why the adverse action was taken; and that you may, upon providing proper identification, request a free copy of the report and may dispute with the FMCSA the accuracy or completeness of any information or report. If you request a copy of a driver record from the Prospective Employer who procured the report, then, within 3 business days of receiving your request, together with proper identification, the Prospective Employer must send or provide to you a copy of your report and a summary of your rights under the Fair Credit Reporting Act.
The Prospective Employer cannot obtain background reports from FMCSA unless you consent in writing.
If you agree that the Prospective Employer may obtain such background reports, please read the following and sign below:
2. I authorize __C.R. England__ ("Prospective Employer") to access the FMCSA Pre-Employment Screening Program (PSP) system to seek information regarding my commercial driving safety record and information regarding my safety inspection history. I understand that I am consenting to the release of safety performance information including crash data from the previous five (5) years and inspection history from the previous three (3) years. I understand and acknowledge that this release of information may assist the Prospective Employer to make a determination regarding my suitability as an employee.
3. I further understand that neither the Prospective Employer nor the FMCSA contractor supplying the crash and safety information has the capability to correct any safety data that appears to be incorrect. I understand I may challenge the accuracy of the data by submitting a request to https://dataqs.fmcsa.dot.gov. If I am challenging crash or inspection information reported by a State, FMCSA cannot change or correct this data. I understand my request will be forwarded by the DataQs system to the appropriate State for adjudication.
4. Please note: Any crash or inspection in which you were involved will display on your PSP report. Since the PSP report does not report, or assign, or imply fault, it will include all Commercial Motor Vehicle (CMV) crashes where you were a driver or co-driver and where those crashes were reported to FMCSA, regardless of fault. Similarly, all inspections, with or without violations, appear on the PSP report. State citations associated with FMCSR violations that have been adjudicated by a court of law will also appear, and remain, on a PSP report.
I have read the above Notice Regarding Background Reports provided to me by Prospective Employer and I understand that if I sign this consent form, Prospective Employer may obtain a report of my crash and inspection history. I hereby authorize Prospective Employer and its employees, authorized agents, and/or affiliates to obtain the information authorized above.
                                    </span>
                                    </label>
                                </div>
                                <div class="align-center">
                                    <div class="frm-row">
                                        <div class="option-group field">
                                            <div class="section colm colm12">
                                                <label class="option block">
                                                    <input type="checkbox" name="PSP_terms" id="PSP_terms" class="gui-input" value="Y">
                                                    <span class="checkbox"></span>
                                                    <span class="small-text fine-grey align-center">I have read the above Notice Regarding Background Reports provided to me by Prospective Employer and I understand that if I sign this consent form, Prospective Employer may obtain a report of my crash and inspection history. I hereby authorize Prospective Employer and its employees, authorized agents, and/or affiliates to obtain the information authorized above.</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="frm-row">
                                <div class="section colm colm12">
                                    <p class="small-text fine-grey align-center">By entering your name and identifying information below, you are signing this Disclosure, Consent, and Application electronically. You understand and agree that your electronic signature is the legal equivalent of your manual signature which may be upheld and used for identification and legal purposes. Your signature will be appended to the Past Employment Verification and the DOT D/A Disclosure and Authorization forms. You affirm that all the information contained in this application is correct and accurate to the best of your knowledge.<br></p>
                                </div>

                                <!-- 							<div class="spacer-b20"></div> -->

                            </div>


                            <div></div>Please Sign Below:
                            <div id="signatureparent">
                                <!-- 														<div class="field"> -->
                                <div class="wrap-0" id="signature"></div>
                                <input type="hidden" id="signaturedata" name="signature_data" class="gui-input">
                            </div>

                            <div class="frm-row">
                                <div class="section align-center">
                                    <p id="signature_data_error" class="sig-error"></p>
                                </div>


                            </div>


                            <div class="frm-row">
                                <div class="section colm colm6">
                                    <label class="field prepend-icon">
                                        <input type="text" name="disclosure_name" id="disclosure_name" class="gui-input" placeholder="Legal Full Name">
                                        <span class="field-icon"><span class="glyphicon glyphicon-pencil"></span></span>

                                    </label>
                                </div>

                                <div class="section colm colm6">
                                    <label class="field prepend-icon">
                                        <input type="password" name="disclosure_ssn" id="disclosure_ssn" maxlength="9" class="gui-input" placeholder="Social Security Number">
                                        <span class="field-icon"><span class="glyphicon glyphicon-certificate"></span></span>

                                    </label>
                                </div>

                            </div>

                            <div class="frm-row">

                                <div class="section colm colm6">
                                    <label class="field prepend-icon">
                                        <input type="password" name="disclosure_dlNumber" id="disclosure_dlNumber" class="gui-input" placeholder="Drivers License Number">
                                        <span class="field-icon"><span class="glyphicon glyphicon-certificate"></span></span>

                                    </label>
                                </div>

                                <div class="section colm colm6">
                                    <label class="field prepend-icon">
                                        <input type="text" name="driver_birthdate" id="driver_birthdate" class="gui-input" placeholder="Date Of Birth" readonly="readonly">
                                        <span class="field-icon"><span class="glyphicon glyphicon-calendar"></span></span>

                                    </label>
                                </div>

                            </div>

                            <div class="frm-row">

                                <div class="section colm colm6">
                                    <label for="driverlicense_state" class="field select">
                                        <select id="driverlicense_state" name="driverlicense_state">
                                            <option value="">Drivers License State</option>
                                            <option value="AL">Alabama</option>
                                            <option value="AK">Alaska</option>
                                            <option value="AZ">Arizona</option>
                                            <option value="AR">Arkansas</option>
                                            <option value="CA">California</option>
                                            <option value="CO">Colorado</option>
                                            <option value="CT">Connecticut</option>
                                            <option value="DE">Delaware</option>
                                            <option value="DC">District Of Columbia</option>
                                            <option value="FL">Florida</option>
                                            <option value="GA">Georgia</option>
                                            <option value="HI">Hawaii</option>
                                            <option value="ID">Idaho</option>
                                            <option value="IL">Illinois</option>
                                            <option value="IN">Indiana</option>
                                            <option value="IA">Iowa</option>
                                            <option value="KS">Kansas</option>
                                            <option value="KY">Kentucky</option>
                                            <option value="LA">Louisiana</option>
                                            <option value="ME">Maine</option>
                                            <option value="MD">Maryland</option>
                                            <option value="MA">Massachusetts</option>
                                            <option value="MI">Michigan</option>
                                            <option value="MN">Minnesota</option>
                                            <option value="MS">Mississippi</option>
                                            <option value="MO">Missouri</option>
                                            <option value="MT">Montana</option>
                                            <option value="NE">Nebraska</option>
                                            <option value="NV">Nevada</option>
                                            <option value="NH">New Hampshire</option>
                                            <option value="NJ">New Jersey</option>
                                            <option value="NM">New Mexico</option>
                                            <option value="NY">New York</option>
                                            <option value="NC">North Carolina</option>
                                            <option value="ND">North Dakota</option>
                                            <option value="OH">Ohio</option>
                                            <option value="OK">Oklahoma</option>
                                            <option value="OR">Oregon</option>
                                            <option value="PA">Pennsylvania</option>
                                            <option value="RI">Rhode Island</option>
                                            <option value="SC">South Carolina</option>
                                            <option value="SD">South Dakota</option>
                                            <option value="TN">Tennessee</option>
                                            <option value="TX">Texas</option>
                                            <option value="UT">Utah</option>
                                            <option value="VT">Vermont</option>
                                            <option value="VA">Virginia</option>
                                            <option value="WA">Washington</option>
                                            <option value="WV">West Virginia</option>
                                            <option value="WI">Wisconsin</option>
                                            <option value="WY">Wyoming</option>
                                        </select>
                                        <i class="arrow double"></i>
                                    </label>
                                </div>

                                <div class="section colm colm6">
                                    <label class="field prepend-icon">
                                        <input type="text" name="disclosure_dlexp" id="disclosure_dlexp" class="gui-input" placeholder="Drivers License Expiration" readonly="readonly">
                                        <span class="field-icon"><span class="glyphicon glyphicon-calendar"></span></span>

                                    </label>
                                </div>

                            </div>
                            <div id="result" class="result spacer-b10"></div>
                        </fieldset>







                    </form>





                </div><!-- end .form-body section -->


            </div><!-- end .smart-forms section -->
        </div><!-- end .smart-wrap section -->
        <!--           </div> -->
        <!-- /End Form -->
    </div>
    <!-- /End Header Form -->
</div>
