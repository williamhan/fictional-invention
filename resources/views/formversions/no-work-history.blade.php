<div class="smart-wrap">
            <div class="smart-forms smart-container wrap-0">
                <div class="form-body smart-steps stp-two">
                    <form id="smart-form" name="smart-form" method="post" action="/posts" enctype="application/x-www-form-urlencoded">

                  {{ csrf_field() }}

                   @include('formversions.formpages.fieldsetone')
                   @include('formversions.formpages.fieldsettwo')
                   @include('formversions.formpages.fieldsetthree')
                   @include('formversions.formpages.fieldsetfour')
                   {{-- @include('formversions.formpages.fieldsetfive') --}}
                   @include('formversions.formpages.fieldsetsix')
                    </form>
                </div><!-- end .form-body section -->
            </div><!-- end .smart-forms section -->
        </div>