<!DOCTYPE html>
<html lang="en">
    <head>
        <?php
        header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
        error_reporting(E_ALL & ~E_NOTICE);
        date_default_timezone_set('America/Denver');
        ?>
        <title>C.R. England</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
        @include('sections.scripts')
        @include('tags.bing')
        @include('tags.facebook')
        @include('tags.bayard-pixels')
        @include('tags.quantcast')
    </head>
    <body style="overflow:auto; margin:0">
        @include('sections.preloader')
        <div class="jumbotron">
            <div class="container" id="container1">
                <div class="row">
                    <div class="col-sm-7" id="header-left">
                        @include('sections.logo')
                        <div class="verticle-line">
                            <h1>@yield('leftheader')</h1>
                        </div>
                        <h2>
                            @yield('leftsubheader')
                        </h2>
                        @yield('textcontent')
                    </div>
                    <div class="col-sm-5" id="join-us-form">
                        <div id="header-right">
                            <h2 class="form-title">@yield('formtitle')
                                <span class="arrow-down"></span>
                            </h2>
                            <div id="join-us-results"></div>
                            @yield('formbody')
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('sections.footer')
    </body>
</html>
@include('sections.autosavejs')