<!DOCTYPE html>
<html lang="en">
<head>
    <?php include '../prod/guid_and_vars.php'; ?>
    <title>C.R. England</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php include '../prod/styles.php';
    include '../prod/scriptsmaster.php'; ?>



<script type="text/javascript" src="../prod/build/scripts/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="../prod/build/scripts/jquery.placeholder.min.js"></script>
<script type="text/javascript" src="../prod/build/scripts/jquery-ui-touch-punch.min.js"></script>
<script type="text/javascript" src="../prod/build/scripts/jquery-ui.min.js"></script>

<script type="text/javascript" src="../prod/build/scripts/smartforms-modal.min.js"></script>
<script type="text/javascript" src="../prod/build/scripts/steps.bill.js"></script>
<script type="text/javascript" src="../prod/build/scripts/jquery-ui-custom.min.js"></script>
<script type="text/javascript" src="../prod/build/scripts/jquery.validatebillletters.min.js"></script>
<script type="text/javascript" src="../prod/build/scripts/additional-methods.min.js"></script>
<script type="text/javascript" src="../prod/build/scripts/jquery.maskedinput.js"></script>
<script type="text/javascript" src="../prod/build/scripts/jquery.form.min.js"></script>
<script type="text/javascript" src="../prod/build/scripts/jquery.formShowHide.min.js"></script>
<script type="text/javascript" src="../prod/build/scripts/smart-form-master-long.js"></script>
<script type="text/javascript" src="../prod/build/scripts/bootstrap.min.js"></script>
<script type="text/javascript" src="../prod/build/scripts/custom.js"></script>
<script type="text/javascript" src="../prod/build/scripts/jquery-cloneya-bill.js"></script>
<script type="text/javascript" src="../prod/build/scripts/jquery.maskMoney.js"></script>


<noscript>
    <img src="//bat.bing.com/action/0?ti=5014094&Ver=2" height="0" width="0" style="display:none; visibility: hidden;" />
</noscript>

<noscript>
    <img height="1" width="1" src="https://www.facebook.com/tr?id=1207376995959219&ev=PageView&noscript=1"/>
</noscript>




    <?php include '../prod/quantcast.php'; ?>
</head>
<body style="overflow:auto; margin:0">
<?php include '../prod/preloader.php'; ?>
<div class="jumbotron">
    <div class="container" id="container1">
        <div class="row">
            <div class="col-sm-7" id="header-left">
                <?php include '../prod/logo.php'; ?>
                <div class="verticle-line">
                    <h1>Truck Driver Jobs</h1>
                </div>
                <title>App Name - @yield('title')</title>
                <h2>CDL or No CDL? Doesn’t Matter!<br>
                    We Train, We Hire—Guaranteed.</h2>
                <h3>Whether you need a CDL, Job, or both; we have you covered. At C.R. England, we train new drivers and hire experienced drivers. We are seeking truck drivers that have a safe and clean record and can handle 12 Gears, 18 Wheels, 40 Tons, 80 Feet, and 400 Horsepower! If that sounds anything like you—Apply Now.<br><br>
                    At C.R. England, You Choose Your Lane, You Choose Your Position, and You Get ALL the Benefits!<br><br>
                    <h5><b>You Choose Your Lane</b></h5>
                    <ul>
                        <li>Dedicated: customer relationships, regular routes, and great pay</li>
                        <li>Regional: balanced home and road life, regular routes, and great miles</li>
                        <li>National: explore the country, competitive pay, and great miles</li>
                    </ul>
                    <h5><b>You Choose Your Position</b></h5>
                    <ul>
                        <li>Solo</li>
                        <li>Team</li>
                        <li>Trainer</li>
                        <li>Instructor</li>
                    </ul>
                    <h5><b>You Get ALL the Benefits</b></h5>
                    <ul>
                        <li>Weekly Pay & Consistent Home Time</li>
                        <li>Health Benefits & 401k Participation</li>
                        <li>Paid Vacation & Bonus Incentives</li>
                        <li>Unlimited Cash Referral Program</li>
                    </ul>
                </h3>
                <h2>
                    Better Pay, Home Time, and Miles &mdash; Apply Now.
                </h2>
                <h4>DISCLAIMER: Eligible applicants receive zero out-of-pocket CDL training (a zero tuition expense with a completed work contract)<br> to work for C.R. England. Training is provided through Premier Truck Driving Schools.</h4>
            </div>
            <?php include '../prod/longform.php'; ?>
        </div>
    </div>
</div>
<?php include '../prod/footer.php'; ?>
</body>
</html>
<?php include '../prod/autosavejs.php'; ?>