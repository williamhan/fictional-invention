@extends('layouts.app')

@section('leftheader', 'Truck Driver Jobs')

@section('leftsubheader')
	CDL or No CDL? Doesn’t Matter!
    <br>
    We Train, We Hire—Guaranteed.
@endsection

@section('textcontent')
    <h3>Whether you need a CDL, Job, or both; we have you covered. At C.R. England, we train new drivers and hire experienced drivers. We are seeking truck drivers that have a safe and clean record and can handle 12 Gears, 18 Wheels, 40 Tons, 80 Feet, and 400 Horsepower! If that sounds anything like you—Apply Now.<br><br>
                    At C.R. England, You Choose Your Lane, You Choose Your Position, and You Get ALL the Benefits!<br><br>
                    <h5><b>You Choose Your Lane</b></h5>
                    <ul>
                        <li>Dedicated: customer relationships, regular routes, and great pay</li>
                        <li>Regional: balanced home and road life, regular routes, and great miles</li>
                        <li>National: explore the country, competitive pay, and great miles</li>
                    </ul>
                    <h5><b>You Choose Your Position</b></h5>
                    <ul>
                        <li>Solo</li>
                        <li>Team</li>
                        <li>Trainer</li>
                        <li>Instructor</li>
                    </ul>
                    <h5><b>You Get ALL the Benefits</b></h5>
                    <ul>
                        <li>Weekly Pay & Consistent Home Time</li>
                        <li>Health Benefits & 401k Participation</li>
                        <li>Paid Vacation & Bonus Incentives</li>
                        <li>Unlimited Cash Referral Program</li>
                    </ul>
                </h3>
                <h2>
                    Better Pay, Home Time, and Miles &mdash; Apply Now.
                </h2>
                <h4>DISCLAIMER: Eligible applicants receive zero out-of-pocket CDL training (a zero tuition expense with a completed work contract)<br> to work for C.R. England. Training is provided through Premier Truck Driving Schools.</h4>
@endsection

@section('formtitle', 'Apply Now')

@section('formbody')
	@if (Request::get('id') == 'PERENGO')
        @include('formversions.no-work-history')
    @else
        @include('formversions.with-work-history')
    @endif
@endsection