<script type="text/javascript" src="scripts/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="scripts/jquery.placeholder.min.js"></script>
<script type="text/javascript" src="scripts/jquery-ui-touch-punch.min.js"></script>
<script type="text/javascript" src="scripts/jquery-ui.min.js"></script>

<script type="text/javascript" src="scripts/smartforms-modal.min.js"></script>
<script type="text/javascript" src="scripts/steps.bill.js"></script>
<script type="text/javascript" src="scripts/jquery-ui-custom.min.js"></script>
<script type="text/javascript" src="scripts/jquery.validatebillletters.min.js"></script>
<script type="text/javascript" src="scripts/additional-methods.min.js"></script>
<script type="text/javascript" src="scripts/jquery.maskedinput.js"></script>
<script type="text/javascript" src="scripts/jquery.form.min.js"></script>
<script type="text/javascript" src="scripts/jquery.formShowHide.min.js"></script>
<script type="text/javascript" src="scripts/smart-form-master-long.js"></script>
<script type="text/javascript" src="scripts/bootstrap.min.js"></script>
<script type="text/javascript" src="scripts/custom.js"></script>
<script type="text/javascript" src="scripts/jquery-cloneya-bill.js"></script>
<script type="text/javascript" src="scripts/jquery.maskMoney.js"></script>