<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/cdl-and-no-cdl', function () {
    return view('pages.cdl-and-no-cdl');
});

Route::get('/cdl-a', function () {
    return view('pages.cdl-a');
});

Route::get('/local', function () {
    return view('pages.local');
});

Route::get('/non-cdl', function () {
    return view('pages.non-cdl');
});

Route::get('/otr', function () {
    return view('pages.otr');
});

Route::get('/recent-grads', function () {
    return view('pages.recent-grads');
});

Route::get('/regional', function () {
    return view('pages.regional');
});

Route::get('/student', function () {
    return view('pages.student');
});

Route::get('/trainer', function () {
    return view('pages.trainer');
});

Route::get('/training', function () {
    return view('pages.training');
});

Route::get('/trucker-jobs', function () {
    return view('pages.trucker-jobs');
});

Route::post('/posts', 'PostsController@store');


