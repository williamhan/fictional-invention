let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */



mix.styles([
    'resources/assets/css/bootstrap.min.css',
    'resources/assets/css/font-awesome.min.css',
    'resources/assets/css/style.css',
    'resources/assets/css/smart-forms.css',
    'resources/assets/css/smart-addons.css',
    'resources/assets/css/Style4.css'
], 'public/css/app.css')

.copyDirectory('resources/assets/scripts', 'public/scripts')
.copyDirectory('resources/assets/img', 'public/img')
.copyDirectory('resources/assets/fonts', 'public/fonts')
.version();



